function iniciar_sesion()
{
    $('#error_inicio_sesion').hide();
    
    var usuario = $('#usuario').val();
    var password = $('#password').val();

    if(usuario === '' || password === '')
    {
        $('#error_inicio_sesion').html("Complete los datos solicitados.");
        $('#error_inicio_sesion').show();
    } else
    {
        $.post('_modulos/inicio_sesion/iniciar_sesion.php',
        {
            usuario: usuario,
            password: password,
            function: 'validar_login'
        },function(data){
            var datos = typeof data === 'object' ? data : JSON.parse(data);
            
            if(datos.tipo != 1)
            {
                $('#error_inicio_sesion').html(datos.mensaje);
                $('#error_inicio_sesion').show();
            }
            else
            {
                window.location.href = '_modulos/principal.php';
            }
        });
    }
}
