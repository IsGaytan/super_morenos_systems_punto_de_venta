//Cargar el combo de productos
$.post('../_general/general.php',
{
    function: 'obtiene_combo_productos'
},function(data)
{
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    
    if(datos.tipo === 1)
    {
         $('#marca_com').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select').chosen();
    }
    else
    {
         $('#marca_com').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select').chosen();
    }
});

//Cargar el combo de proveedores
$.post('../_general/general.php',
{
    function: 'obtiene_combo_proveedores'
},function(data)
{
    var datos = typeof data === 'object' ? data : JSON.parse(data);

    if(datos.tipo === 1)
    {
         $('#prov_compr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select-1').chosen();
         $('#prov_mod').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
    }
    else
    {
         $('#prov_compr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select-1').chosen();
         $('#prov_mod').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
    }
});

//Llamar función que permite sólo registrar dos decimales
$('#cantidad_pagar').numeric();
$('#cantidad_credito').numeric();

//Agregar scroll a la tabla de compras
$('#tabla-compra').ace_scroll();
    
//Al perder el foco el código de barras trae los datos del producto o si no existe, arroja aviso
$('#cod_bar_com').blur(function (){
    var codigo_barras = $('#cod_bar_com').val();

    if(codigo_barras != '')
    {
        $.post('../_general/general.php',
        {
            codigo_barras: codigo_barras,
            function: 'obten_datos_producto'
        },
        function(data){
            
            var datos = typeof data === 'object' ? data : JSON.parse(data);
            
            if(datos.tipo === 1)
            {
                $("#prec_com_com").val(datos.contenido.precio_compra);
                $('#prec_com_com').attr('disabled', true);
                $('#prec_ven_com').val(datos.contenido.precio_venta);
                $('#prec_ven_com').attr('disabled', true);
                $('#presentacion_com').val(datos.contenido.presentacion);
                $('#presentacion_com').attr('disabled', true);
                $('#marca_com').val(datos.contenido.id_producto).trigger("chosen:updated");
                $('#marca_com').prop('disabled', true).trigger("chosen:updated");
                $('#prov_compr').val(datos.contenido.id_proveedor).trigger("chosen:updated");
                $('#prov_compr').prop('disabled', true).trigger("chosen:updated");
                $('#subtotal_com').prop('disabled', true);
            }
            else
            {
                $("#prec_com_com").val('');
                $('#prec_com_com').attr('disabled', false);
                $('#prec_ven_com').val('');
                $('#prec_ven_com').attr('disabled', false);
                $('#presentacion_com').val('');
                $('#presentacion_com').attr('disabled', false);
                $('#marca_com').val(0).trigger("chosen:updated");
                $('#marca_com').prop('disabled', false).trigger("chosen:updated");
                $('#prov_compr').val(0).trigger("chosen:updated");
                $('#prov_compr').prop('disabled', false).trigger("chosen:updated");
                $('#subtotal_com').prop('disabled', false);
                
                $('#busca_prod_err').html(datos.mensaje);
                $('#busca_prod_err').show();
                setTimeout(function(){
                    $('#busca_prod_err').hide(1000);
                }, 3000);
                
            }
        });
    }
    else
    {
        $("#prec_com_com").val('0.00');
        $('#prec_com_com').attr('disabled', false);
        $('#prec_ven_com').val('0.00');
        $('#prec_ven_com').attr('disabled', false);
        $('#presentacion_com').val('');
        $('#presentacion_com').attr('disabled', false);
        $('#marca_com').val(0).trigger("chosen:updated");
        $('#marca_com').prop('disabled', false).trigger("chosen:updated");
        $('#prov_compr').val(0).trigger("chosen:updated");
        $('#prov_compr').prop('disabled', false).trigger("chosen:updated");
        $('#subtotal_com').prop('disabled', false);
        $('#subtotal_com').val('0.00');
        $('#cantidad_com').val('0.00');
    }
});

//Al cambiar la opción seleccionada; trae los datos del producto y los pinta en el formulario
//Cuando es 0 (no hay producto 0) limpia todos los campos
$('#marca_com').change(function(){
    var id_producto = $('#marca_com').val();
    
    if(id_producto != '0')
    {
        $.post('../_general/general.php',
        {
            id_producto: id_producto,
            function: 'obten_datos_producto'
        },
        function(data){
            
            var datos = typeof data === 'object' ? data : JSON.parse(data);
            
            if(datos.tipo === 1)
            {
                $("#prec_com_com").val(datos.contenido.precio_compra);
                $('#prec_com_com').attr('disabled', true);
                $('#prec_ven_com').val(datos.contenido.precio_venta);
                $('#prec_ven_com').attr('disabled', true);
                $('#presentacion_com').val(datos.contenido.presentacion);
                $('#presentacion_com').attr('disabled', true);
                $('#cod_bar_com').val(datos.contenido.codigo_barras);
                $('#cod_bar_com').attr('disabled', true);
                $('#prov_compr').val(datos.contenido.id_proveedor).trigger("chosen:updated");
                $('#prov_compr').prop('disabled', true).trigger("chosen:updated");
                $('#subtotal_com').val('0.00');
                $('#subtotal_com').prop('disabled', true);
                $('#cantidad_com').val('0.00');
            }
            else
            {
                $("#prec_com_com").val('');
                $('#prec_com_com').attr('disabled', false);
                $('#prec_ven_com').val('');
                $('#prec_ven_com').attr('disabled', false);
                $('#presentacion_com').val('');
                $('#presentacion_com').attr('disabled', false);
                $('#cod_bar_com').val('');
                $('#cod_bar_com').attr('disabled', false);
                $('#prov_compr').val(0).trigger("chosen:updated");
                $('#prov_compr').prop('disabled', false).trigger("chosen:updated");
                $('#subtotal_com').prop('disabled', false);
                
                $('#busca_prod_err').html(datos.mensaje);
                $('#busca_prod_err').show();
                setTimeout(function(){
                    $('#busca_prod_err').hide(1000);
                }, 3000);
                
            }
        });
    }
    else
    {
        $("#prec_com_com").val('');
        $('#prec_com_com').attr('disabled', false);
        $('#prec_ven_com').val('');
        $('#prec_ven_com').attr('disabled', false);
        $('#presentacion_com').val('');
        $('#presentacion_com').attr('disabled', false);
        $('#cod_bar_com').val('');
        $('#cod_bar_com').attr('disabled', false);
        $('#prov_compr').val(0).trigger("chosen:updated");
        $('#prov_compr').prop('disabled', false).trigger("chosen:updated");
    }
});

//Al perder el foco el input Cantidad, calcula el subtotal; resultado de multiplicar precio compra con cantidad
$('#cantidad_com').blur(function (){
    
    if($('#cantidad_com').val() != '' && $('#prec_com_com').val() != '')
    {
        var cantidad = parseFloat($('#cantidad_com').val());
        var precio_compra = parseFloat($('#prec_com_com').val());
        
        $('#subtotal_com').val(cantidad * precio_compra);
    }
    
});

function agregar_compra()
{
    var codigo_barras = $('#cod_bar_com').val();
    var id_producto = $('#marca_com').val();
    var producto = $( "#marca_com option:selected" ).text();
    var precio_compra = $('#prec_com_com').val();
    var cantidad = $('#cantidad_com').val();
    var subtotal = $('#subtotal_com').val();
    var id_proveedor = $('#prov_compr').val();
    var proveedor = $( "#prov_compr option:selected" ).text();
    
    $.post('comprar/comprar.php',
    {
        codigo_barras : codigo_barras,
        id_producto : id_producto,
        producto : producto,
        precio_compra: precio_compra,
        cantidad : cantidad,
        subtotal : subtotal,
        id_proveedor : id_proveedor,
        proveedor : proveedor,
        function : 'agregar_articulo_carrito_comprar'
    }, function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 1)
        {
            debugger;
            $('#body_tabla_compra').html(datos.contenido.html);
            $('#input_total_pagar').val(datos.contenido.precio_carrito);
            $('#input_cantidad_art_carrito').val(datos.contenido.cantidad_articulos_total);
            $('#btn_pagar').prop('disabled', false);
        }
        
    });
    
}

function eliminar_fila(id_tr)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea eliminar este producto de la lista de compras?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-times'></i>Eliminar",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                    $.post('comprar/comprar.php',
                    {
                        articulo_eliminar: id_tr,
                        function : 'eliminar_articulo_carrito_comprar'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $('#body_tabla_compra').html(datos.contenido.html);
                            $('#input_total_pagar').val(datos.contenido.precio_carrito);
                            $('#input_cantidad_art_carrito').val(datos.contenido.cantidad_articulos_total);
                        }
                        else
                        {
                            $('#body_tabla_compra').html(datos.mensaje);
                            $('#input_total_pagar').val('0.00');
                            $('#input_cantidad_art_carrito').val('0');
                            $('#btn_pagar').prop('disabled', true);
                        }

                    });
                }
           }
        }
    });
}

$('#cantidad_pagar').blur(function ()
{
    var cantidad_total_pagar = $('#cantidad_a_pagar').text();
    var cantidad_credito = $('#cantidad_credito').val();
    var cantidad_pagar = $('#cantidad_pagar').val();
    if(cantidad_pagar != '')
    {
        cantidad_pagar = parseFloat(cantidad_pagar);
        cantidad_credito = parseFloat(cantidad_credito);
        
        if(cantidad_total_pagar < cantidad_pagar && cantidad_pagar != 0)
        {
            $('#registrar_compra_parcial_btn').prop('disabled', true);
            $.gritter.add({
                title: 'Mensaje',
                text: 'La cantidad a pagar no puede ser mayor que la cantidad a crédito.',
                class_name: 'gritter-error'
            });
        }
        /*else if(cantidad_pagar === 0)
        {
            $('#registrar_compra_parcial_btn').prop('disabled', true);
            $.gritter.add({
                title: 'Mensaje',
                text: 'No puede realizar un pago parcial de $0.00.',
                class_name: 'gritter-error'
            });
        }*/
        else
        {
            cantidad_total_pagar = parseFloat(cantidad_total_pagar);
            $('#cantidad_credito').val(cantidad_total_pagar-cantidad_pagar);
        }
    }
});

function lanzar_modal_pagar_parcial()
{
    $.post('comprar/comprar.php',
    {
        function : 'validar_contenido_carrito'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'No tiene artículos en el carrito para realizar un pago parcial.',
                class_name: 'gritter-warning'
            });
        }
        else
        {
            var total_pagar = $('#input_total_pagar').val();

            $('#cantidad_a_pagar').html(total_pagar);
            $('#cantidad_credito').val(total_pagar);
            $('#modal_pagar_parcial').modal('show');
        }
    });
}

function lanzar_modal_pagar()
{
    $.post('comprar/comprar.php',
    {
        function : 'validar_contenido_carrito'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'No tiene artículos en el carrito para realizar un pago.',
                class_name: 'gritter-warning'
            });
        }
        else
        {
            var total_pagar = $('#input_total_pagar').val();
    
            $('#mensaje_pagar').html("¿Realizar pago por un total de $<strong>"+total_pagar+"</strong>?");
            $('#modal_pagar').modal('show');
        }
    });   
}

function realizar_pago_parcial()
{
    $.post('comprar/comprar.php',
    {
        cantidad_total : parseFloat($('#cantidad_a_pagar').text()),
        cantidad_pagar : parseFloat($('#cantidad_pagar').val()),
        observaciones : $('#observaciones').val(),
        id_proveedor : $('#prov_mod').val(),
        function : 'realizar_pago_parcial'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $('#mensaje_modal_error').html(datos.mensaje);
            $('#mensaje_modal_error').show();
            setTimeout(function(){
                $('#mensaje_modal_error').hide(1000);
            }, 3000);
        }
        else
        {
            $('#modal_pagar_parcial').modal('hide');
            setTimeout(function(){
                $('#comprar').click();
                $.gritter.add({
                        title: '¡Compra registrada correctamente!',
                        text: 'Se ha guardado la compra con una cuenta por pagar y se han actualizado las existencias de los productos comprados.',
                        class_name: 'gritter-info'
                });
            }, 1000);
            
        }
    });
}

function realizar_pago()
{
    var compra_rapida = "NO";
    
    if($('#check_compra_rap').prop('checked'))
    {
        compra_rapida = "SI";   
    }
    
    $.post('comprar/comprar.php',
    {
        compra_rapida : compra_rapida,
        function : 'realizar_pago'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        debugger;
        if(datos.tipo === 3)
        {
            $('#mensaje_modal_error_completo').html(datos.mensaje);
            $('#mensaje_modal_error_completo').show();
            setTimeout(function(){
                $('#mensaje_modal_error_completo').hide(1000);
            }, 3000);
        }
        else
        {
            $('#modal_pagar').modal('hide');
            setTimeout(function(){
                $('#comprar').click();
                $.gritter.add({
                        title: '¡Compra registrada correctamente!',
                        text: 'Se han actualizado las existencias de los productos comprados.',
                        class_name: 'gritter-info'
                });
            }, 1000);
            
        }
    });
}
