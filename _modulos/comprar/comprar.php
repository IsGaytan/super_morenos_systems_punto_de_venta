<?php
require_once '../../_conection/bd_connect.php';
require_once 'carrito_comprar/carrito_comprar.class.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'agregar_articulo_carrito_comprar':
            $datos = $_POST;
            agregar_articulo_carrito_comprar($datos);
            break;
        case 'eliminar_articulo_carrito_comprar';
            $id_articulo = $_POST['articulo_eliminar'];
            eliminar_articulo_carrito_comprar($id_articulo);
            break;
        case 'validar_contenido_carrito':
            validar_contenido_carrito();
            break;
        case 'realizar_pago_parcial':
            $datos = $_POST;
            realizar_pago_parcial($datos);
            break;
        case 'realizar_pago':
            realizar_pago($_POST['compra_rapida']);
            break;
    }
}

function agregar_articulo_carrito_comprar($datos)
{
    carrito_comprar::add_carrito_comprar($datos);
    
    $respuesta = obtener_html_tabla_comprar();
    
    echo json_encode($respuesta);
}

function eliminar_articulo_carrito_comprar($id_articulo)
{
     try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
    
        if(count($_SESSION["carrito_comprar"]['articulos']) == 1)
        {
            $carrito_eliminado = carrito_comprar::remove_carrito_comprar($id_articulo, $destruir = true);

            if($carrito_eliminado)
            {
                throw new Exception('<tr><td class="text-muted" style="padding-top: 13px; text-align: center" colspan="6">No tiene artículos en la lista</td></tr>');
            }
        }
        else
        {
            $carrito_eliminado = carrito_comprar::remove_carrito_comprar($id_articulo, $destruir = false);
            
            if(!$carrito_eliminado)
            {
                $respuesta = obtener_html_tabla_comprar();
            }
        }
        echo json_encode($respuesta);
    }
    catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        echo json_encode($respuesta);
    }
}

function validar_contenido_carrito()
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $tiene_contenido = carrito_comprar::check_carrito_content();

        if($tiene_contenido)
        {
            $respuesta = obtener_html_tabla_comprar();
            echo json_encode($respuesta);
        }
        else
        {
            throw new Exception('<tr><td class="text-muted" style="padding-top: 13px; text-align: center" colspan="6">No tiene artículos en la lista</td></tr>');
        }
        
    }catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        echo json_encode($respuesta);
    }
}

function obtener_html_tabla_comprar()
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $html_tabla = '';
        foreach($_SESSION['carrito_comprar']['articulos'] as $articulo)
        {
            $html_tabla .= '<tr>
                                <td class="text-muted" style="padding-top: 13px;">' . $articulo['codigo_barras'] . '</td>
                                <td class="orange" style="padding-top: 13px;">' . $articulo['producto'] . '</td>
                                <td style="padding-top: 13px; text-align: center;" class="text-primary">' . number_format($articulo['cantidad'], 2) . '</td>
                                <td style="padding-top: 13px; text-align: right;" class="green">$' . number_format($articulo['precio_compra'], 2) . '</td>
                                <td class="center" style="padding-top: 15px;">
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="red tooltip-error" id="'.$articulo['id_articulo'].'" onclick="eliminar_fila(this.id)" data-rel="tooltip" data-placement="top" data-original-title="Eliminar de la lista">
                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                    </div>
                                </td>
                                <td style="padding-top: 13px; text-align: right;" class="green">$' . number_format($articulo['precio_compra'] * $articulo['cantidad'], 2) . '</td>
                            </tr>';
        }
        
        $respuesta['contenido']['html'] = $html_tabla;
        $respuesta['contenido']['precio_carrito'] = $_SESSION['carrito_comprar']['precio_carrito'];
        $respuesta['contenido']['cantidad_articulos'] = $_SESSION['carrito_comprar']['cantidad_articulos'];
        $respuesta['contenido']['cantidad_articulos_total'] = $_SESSION['carrito_comprar']['cantidad_articulos_total'];
        
        return $respuesta;
    }
    catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        return $respuesta;
    }
}

function realizar_pago_parcial($datos)
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $respuesta = validar_campos_vacios($datos);
        
        if($respuesta['tipo'] == 3)
        {
            throw new Exception ($respuesta['mensaje']);
        }
        
        $conect = conectar_bd::realizar_conexion();
        
        $conect->begin_transaction();
        
        $total_compra = number_format($datos['cantidad_total']);
        $total_pagado = number_format($datos['cantidad_pagar']);
        $total_a_credito = number_format($total_compra-$total_pagado);
        $observaciones = $datos['observaciones'];
        $id_proveedor = $datos['id_proveedor'];
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        $fecha_hoy = date("Y-m-d H:i:s");
        
        $query = "  INSERT INTO compras_especiales (fecha_operacion, total_compra, total_pagado, total_a_credito, observaciones, id_proveedor, id_usuario, estatus)
                    VALUES ('$fecha_hoy', $total_compra, $total_pagado, $total_a_credito, '$observaciones', $id_proveedor, $id_usuario, 'PE')";
        
        $insertar_fila = $conect->query($query);
        
        if(!$insertar_fila)
        {
            throw new Exception("Error al guardar la compra en la base de datos.");
        }
        
        //Obtener el saldo del proveedor para actualizarlo con la cantidad a crédito de la venta
        $query = "SELECT saldo FROM proveedores WHERE id_proveedor = $id_proveedor";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            //Si obtuvo un saldo del proveedor, agregar la siguiente consulta para actualizarlo
            $saldo_nuevo = number_format($rows[0]['saldo'] + $total_a_credito);
            $query = "UPDATE proveedores SET saldo = $saldo_nuevo WHERE id_proveedor = $id_proveedor";
            $actualizar = $conect->query($query);
        
            if(!$actualizar)
            {
                throw new Exception("Error al actualizar el saldo al proveedor.");
            }
             
        } else 
        {
            throw new Exception("Error al obtener el saldo del proveedor.");
        }
        
        $respuesta = registrar_compra_carrito($conect);
         
        if($respuesta['tipo'] == 3)
        {
            Throw new Exception($respuesta['mensaje']);
        }
        else
        {
            carrito_comprar::destruir_carrito();
        }
        
        $conect->commit();
        $conect->close();
        
        echo json_encode($respuesta);
    }catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        echo json_encode($respuesta);
    }
}

function realizar_pago($compra_rapida)
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => ''
        );

        $conect = conectar_bd::realizar_conexion();

        $conect->begin_transaction();
        
        $respuesta = registrar_compra_carrito($conect);
         
        if($respuesta['tipo'] == 3)
        {
            Throw new Exception($respuesta['mensaje']);
        }
        else
        {
            if(isset($_SESSION['carrito_comprar']) && isset($_SESSION['carrito_comprar']['articulos']))
            {
                $stmt = $conect->prepare("  INSERT INTO compras (cantidad_tipos_articulos, precio_compra, cantidad_articulos, fecha_compra, id_usuario, compra_rapida)
                        VALUES (?, ?, ?, ?, ?, ?);");

                $stmt->bind_param('idisis', $cantidad_tipos_articulos, $precio_compra, $cantidad_articulos, $fecha_hoy, $id_usuario, $compra_rapida);

                $cantidad_tipos_articulos = $_SESSION['carrito_comprar']['cantidad_articulos'];
                $precio_compra = $_SESSION['carrito_comprar']['precio_carrito'];
                $cantidad_articulos = $_SESSION['carrito_comprar']['cantidad_articulos'];
                $fecha_hoy = date("Y-m-d H:i:s");
                $id_usuario = $_SESSION['usuario']['id_usuario'];

                $stmt->execute();

            }
            
            carrito_comprar::destruir_carrito();
        }
        
        $conect->commit();
        $conect->close();
        
        echo json_encode($respuesta);
    }catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        echo json_encode($respuesta);
    }
}

function validar_campos_vacios($datos)
{
    $respuesta = array(
        'tipo' => 1,
        'mensaje' => ''
    );
    
    if(trim($datos['observaciones']) == '')
    {
        $respuesta['mensaje'] .= 'Debe ingresar sus observaciones.';
        $respuesta['tipo'] = 3;
    }
    if($datos['id_proveedor'] == '0')
    {
        $respuesta['mensaje'] .= ' Debe seleccionar un proveedor para registrar la venta.';
        $respuesta['tipo'] = 3;
    }
    return $respuesta;
}

function registrar_compra_carrito($conect)
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => ''
        );
         
        if(isset($_SESSION['carrito_comprar']) && isset($_SESSION['carrito_comprar']['articulos']))
        {
            $productos_actualizar = $_SESSION['carrito_comprar']['articulos'];

            foreach($productos_actualizar as $producto)
            {
                $id_producto = $producto['id_producto'];
                $cantidad = $producto['cantidad'];
                
                //Consultamos la existencia del producto en cuestión
                $query = "SELECT existencia FROM productos WHERE id_producto = $id_producto";
                $filas = $conect->query($query);

                if (!empty($filas) && $filas->num_rows > 0) 
                {
                    $rows = $filas->fetch_all(MYSQLI_ASSOC);
                    $existencia_nueva = str_replace(',','',number_format(($rows[0]['existencia'] + $cantidad), 2));
                    $query = "UPDATE productos SET existencia = $existencia_nueva WHERE id_producto = $id_producto";
                    $actualizar = $conect->query($query);

                    if(!$actualizar)
                    {
                        throw new Exception("Error al actualizar la existencia del producto.");
                    }
                }
                else
                {
                    throw new Exception("Error al consultar la existencia del producto con id $id_producto");
                }
            }
        }
        else
        {
            Throw new Exception("No hay artículos en el carrito de compras.");
        }
        return $respuesta;
        
    }
    catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        return $respuesta;
    }
}