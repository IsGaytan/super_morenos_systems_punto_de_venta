<?php

/**
 * Description of carrito_comprar
 *
 * @author IsGaytán
 */
session_start();

class carrito_comprar {
    
    private $carrito_comprar = array();
    
    public function __construct()
    {
        if(!isset($_SESSION["carrito_comprar"]))
        {
            $_SESSION["carrito_comprar"] = null;
            $this->carrito_comprar["precio_total"] = 0;
            $this->carrito_comprar["articulos_total"] = 0;
        }
        $this->carrito_comprar = $_SESSION['carrito_comprar'];
    }
    
    public function add_carrito_comprar($articulo = array())
    {        
        if(!is_array($articulo) || empty($articulo))
        {
            throw new Exception("Error, el articulo no es un array!", 1);	
        }

        if(!$articulo["id_producto"] || !$articulo["producto"] || !$articulo["cantidad"] || !$articulo["precio_compra"])
        {
                throw new Exception("Error, el articulo debe tener un id, nombre, cantidad y precio.", 1);	
        }

        if(!is_numeric($articulo["id_producto"]) || !is_numeric($articulo["cantidad"]) || !is_numeric($articulo["precio_compra"]))
        {
                throw new Exception("Error, el id, cantidad y precio deben ser números!", 1);	
        }

        $hora_sistema = getdate(); //Obtengo la hora y fecha del sistema
        
        $id_fila = $hora_sistema['0']; //En esta posición están los segundos desde la época UNIX. Será el id
    
        $_SESSION["carrito_comprar"]['articulos'][$id_fila] = $articulo;
        
        $_SESSION["carrito_comprar"]['articulos'][$id_fila]['id_articulo'] = $id_fila;
        
        carrito_comprar::update_carrito_comprar();
    }
    
    public function remove_carrito_comprar($id_articulo, $destruir)
    {
        if($destruir)
        {
            unset($_SESSION["carrito_comprar"]);

            return true;
        }
        else
        {
            unset($_SESSION["carrito_comprar"]["articulos"][$id_articulo]);

            carrito_comprar::update_carrito_comprar();

            return false;
        }  
    }
    
    public function update_carrito_comprar()
    {
        $precio_carrito = 0;
        $cantidad_articulos = 0;
        
        if(count($_SESSION['carrito_comprar']['articulos']) > 0)
        {
            //Actualizar la cantidad de artículos en el carrito
            $_SESSION['carrito_comprar']['cantidad_articulos'] = count($_SESSION['carrito_comprar']['articulos']);
            
            //Obtener los precios unitarios y las cantidades de cada artículo para sacar el total del carrito
            foreach ($_SESSION['carrito_comprar']['articulos'] as $articulo)
            {
                $precio_carrito += $articulo['precio_compra'] * $articulo['cantidad'];
                $cantidad_articulos += $articulo['cantidad'];
            }
            
            $_SESSION['carrito_comprar']['precio_carrito'] = number_format($precio_carrito, 2);
            $_SESSION['carrito_comprar']['cantidad_articulos_total'] = number_format($cantidad_articulos, 2);
        }
    }
    
    public function check_carrito_content()
    {
        if(!isset($_SESSION['carrito_comprar']['articulos']))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function destruir_carrito()
    {
        if(isset($_SESSION['carrito_comprar']))
        {
            unset($_SESSION['carrito_comprar']);
            return true;
        }
        else
        {
            return false;
        }
    }
}
