<?php
session_start();
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'buscar_historial_ventas':
            buscar_historial_ventas($_POST);
        break;
    }
}

function buscar_historial_ventas($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar los datos del formulario
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos['fecha_inicial'], $datos['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje']);
        
        $conect = conectar_bd::realizar_conexion();
        
        //Where Fechas
        list($mes, $dia, $anio) = split('[/.-]', $datos['fecha_inicial']);
        
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $datos['fecha_final']);
        
        $where_fechas = "WHERE ve.fecha BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
        //Where tipo de venta
        $where_tipo = '';
        if($datos['tipo_venta'] != '0')
        {
            $tipo_cobro = $datos['tipo_venta'];
            $where_tipo = "AND ve.tipo_cobro = '$tipo_cobro'";
        }
        
        //
        $query = "  SELECT ve.id_venta, ve.fecha, ve.total, ve.tipo, us.nombre AS nombre_vendedor, 
                    us.apellido_pat AS apellido_vendedor, ve.tipo_cobro, '-' AS nombre_cliente, ' ' AS apellido_pat, '-' AS cantidad_credito, 
                    '-' AS cantidad_saldada, '-' AS referencia
                    FROM ventas AS ve
                    LEFT JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    $where_fechas
                    $where_tipo
                    
                    UNION

                    SELECT vc.id_venta, ve.fecha, ve.total, ve.tipo, us.nombre AS nombre_vendedor,
                    us.apellido_pat AS apellido_vendedor, ve.tipo_cobro, cl.nombre_cliente, cl.apellido_pat, vc.cantidad_credito, 
                    vc.cantidad_saldada, vt.referencia
                    FROM ventas_credito AS vc
                    LEFT JOIN ventas AS ve ON vc.id_venta = ve.id_venta
                    LEFT JOIN usuarios AS us ON us.id_usuario = vc.id_usuario_recibio
                    LEFT JOIN clientes AS cl ON cl.id_cliente = vc.id_cliente
                    LEFT JOIN ventas_tarjeta AS vt ON vt.id_venta = vc.id_venta
                    $where_fechas
                    $where_tipo
                
                    UNION

                    SELECT ve.id_venta, ve.fecha, ve.total, ve.tipo, us.nombre AS nombre_vendedor, 
                    us.apellido_pat AS apellido_vendedor, ve.tipo_cobro, '-' AS nombre_cliente, ' ' AS apellido_pat, '-' AS cantidad_credito, 
                    '-' AS cantidad_saldada, vt.referencia
                    FROM ventas AS ve
                    LEFT JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    LEFT JOIN ventas_tarjeta AS vt ON vt.id_venta = ve.id_venta
                    $where_fechas
                    $where_tipo";
        
        /*$query = "  SELECT ve.*, cl.nombre_cliente, cl.apellido_pat, vc.cantidad_credito, vc.cantidad_saldada, vt.referencia, 
                    us.nombre AS nombre_vendedor, us.apellido_pat AS apellido_vendedor
                    FROM ventas AS ve
                    LEFT JOIN ventas_credito AS vc ON ve.id_venta = vc.id_venta
                    LEFT JOIN ventas_tarjeta AS vt ON ve.id_venta = vt.id_venta
                    LEFT JOIN clientes AS cl ON vc.id_cliente = cl.id_cliente
                    LEFT JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    $where_fechas
                    $where_tipo";*/
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $resultado['contenido'] = armar_tabla_resultado($rows);
            
        } 
        else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($ventas)
{
    $respuesta = array(
        'html' => '',
        'total' => 0
    );
    
    foreach($ventas as $venta)
    {
        $id_venta = $venta['id_venta'];
        $fecha = $venta['fecha'];
        $total = $venta['total'];
        $tipo_venta = $venta['tipo'] == 1 ? 'Contado' : 'Crédito';
        $vendedor = $venta['nombre_vendedor'] . ' ' . $venta['apellido_vendedor'];
        $tipo_cobro = ucwords(strtolower($venta['tipo_cobro']));
        $cliente = $venta['nombre_cliente'] . ' ' . $venta['apellido_pat'];
        
        $cantidad_credito = $venta['cantidad_credito'];
        $cantidad_saldada = $venta['cantidad_saldada'];
        
        if(is_numeric($cantidad_credito))
            $cantidad_credito = number_format($cantidad_credito, 2);
        
        if(is_numeric($cantidad_saldada))
            $cantidad_saldada = number_format($cantidad_saldada, 2);
        
        $referencia_tarjeta = $venta['referencia'];
        
        $respuesta['html'] .= "   <tr>
                        <td align='center'>$id_venta</td>
                        <td align='center'>$fecha</td>
                        <td align='right'>$".number_format($total, 2)."</td>
                        <td align='center'>$tipo_venta</td>
                        <td align='center'>$vendedor</td>
                        <td align='center'>$tipo_cobro</td>
                        <td align='center'>$cliente</td>
                        <td align='right'>".$cantidad_credito."</td>
                        <td align='right'>".$cantidad_saldada."</td>
                        <td align='center'>$referencia_tarjeta</td>
                    </tr>";
        
        $respuesta['total'] += $total;
    }
    
    $respuesta['total'] = number_format($respuesta['total'], 2);
    
    return $respuesta;
}
