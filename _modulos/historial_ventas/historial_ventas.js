//Agregar DatePicker a campos de tipo Fecha
$('#fecha_inicial_historial').datepicker();
$('#fecha_final_historial').datepicker();

function buscar_historial_ventas()
{
    $.post('historial_ventas/historial_ventas.php',
    {
        fecha_inicial   : $('#fecha_inicial_historial').val(),
        fecha_final     : $('#fecha_final_historial').val(),
        tipo_venta      : $('#select_tipo_venta').val(),
        function        : 'buscar_historial_ventas'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ERROR!',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
        }
        else
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: "EN ESTE MOMENTO HAY DATOS PARA MOSTRAR.",
                class_name: 'gritter-success'
            });
            
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido.html);
            $('#total_ventas').val(datos.contenido.total);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
        }
    });
}