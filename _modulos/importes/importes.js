//Convertir esos input en un datepicker
$('#fecha_inicial_importes').datepicker();
$('#fecha_final_importes').datepicker();
$('#fecha_inicial_recargas').datepicker();
$('#fecha_final_recargas').datepicker();
$('#telefono_buscar_recargas').mask('(999)-999-9999');

function buscar_importes()
{
    $.post('importes/importes.php',
    {
        fecha_inicial : $('#fecha_inicial_importes').val(),
        fecha_final : $('#fecha_final_importes').val(),
        estatus : $('#estatus_importes').val(),
        function : 'buscar_importes'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
            
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'ERROR',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
            
            $('#section_tabla_resultados').hide();
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '15%' }
                ]  
            });
            $('#section_tabla_resultados').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
            $('.tooltip-error').tooltip();
        }
    });
}

function buscar_recargas()
{
    $.post('importes/importes.php',
    {
        fecha_inicial : $('#fecha_inicial_recargas').val(),
        fecha_final : $('#fecha_final_recargas').val(),
        telefono : $('#telefono_buscar_recargas').val(),
        function : 'buscar_recargas'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'ERROR',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
            
            $('#section_tabla_resultados_recargas').hide();
        }
        else
        {
            $('#tabla_resultados_recargas').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla_recargas').html(datos.contenido);
            $('#tabla_resultados_recargas').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados_recargas').show();
        }
    });
}

function entregar_importe(id_importe)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea entregar el importe por envase prestado?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Entregar",
                "className" : "btn-sm btn-success",
                "callback": function() {
                    $.post('importes/importes.php', 
                    {
                        id_importe : id_importe,
                        function : 'entregar_importe'
                    }, function(data)
                    {
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: '¡CORRECTO!',
                                text: 'El importe ha sido entregado correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#btn_buscar_importes').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}