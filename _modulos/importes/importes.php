<?php
session_start();
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'buscar_importes':
            buscar_importes($_POST);
        break;
        case 'buscar_recargas':
            buscar_recargas($_POST);
            break;
        case 'entregar_importe':
            entregar_importe($_POST['id_importe']);
            break;
    }
}

function buscar_importes($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar los datos del formulario
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos['fecha_inicial'], $datos['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje']);

        
        //Realizar la búsqueda mediante los filtros
        $conect = conectar_bd::realizar_conexion();
        
        $where_estatus = '';
        
        list($mes, $dia, $anio) = split('[/.-]', $datos['fecha_inicial']);
        
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $datos['fecha_final']);
        
        $estatus = $datos['estatus'];

        if($estatus != '0')
        {
            switch($estatus)
            {
                case 'PA':
                    $where_estatus = "AND im.estatus = 'PA'";
                break;
                case 'PE':
                    $where_estatus = "AND im.estatus = 'PE'";
                break;
            }
        }
        
        $query = "  SELECT im.*, us.nombre_usuario
                    FROM importes AS im
                    LEFT JOIN usuarios AS us ON us.id_usuario = im.id_usuario
                    WHERE im.fecha_operacion BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'
                    $where_estatus";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            
            $resultado['contenido'] = armar_tabla_resultado($rows);
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }

        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function buscar_recargas($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar los datos del formulario
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos['fecha_inicial'], $datos['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje']);
        
        //Realizar la búsqueda mediante los filtros
        $conect = conectar_bd::realizar_conexion();
        
        list($mes, $dia, $anio) = split('[/.-]', $datos['fecha_inicial']);
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $datos['fecha_final']);
        
        $where_telefono = '';
        $telefono = $datos['telefono'];
        
        if($telefono != '')
            $where_telefono = "AND re.telefono = '$telefono'";
        
        $query = "  SELECT re.*
                    FROM recargas AS re
                    WHERE re.fecha BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'
                    $where_telefono";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
           
            //Realizar el html para la tabla de recargas
            foreach($rows as $recarga)
            {
                $num_telefono = $recarga['telefono'];
                $monto = $recarga['monto'];
                $compania = $recarga['compania'];
                $fecha = $recarga['fecha'];
                $comision = $recarga['comision'];
                $folio = $recarga['folio'];
                $transaccion = $recarga['transaccion'];
                $numero_autorizacion = $recarga['numero_autorizacion'];
                
                $resultado['contenido'] .= "<tr>
                                                <td>$num_telefono</td>
                                                <td>$monto</td>
                                                <td>$compania</td>
                                                <td>$fecha</td>
                                                <td>$comision</td>
                                                <td>$folio</td>
                                                <td>$transaccion</td>
                                                <td>$numero_autorizacion</td>
                                            </tr>";
                
            }
             
        }
        else 
        {
            throw new Exception("No se encontraron datos");
        }
                
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function entregar_importe($id_importe)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare("  UPDATE importes SET estatus = 'PA', fecha_pago = ? WHERE id_importe = ?");
        $stmt->bind_param('si', $fecha_hoy, $id_importe);
        
        $fecha_hoy = date("Y-m-d H:i:s");

        $stmt->execute();
        
        $conect->close();
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($importes)
{
    $html = '';
    
    foreach($importes as $importe)
    {
        $id_importe = $importe['id_importe'];
        $referencia = $importe['referencia'];
        $fecha_operacion = $importe['fecha_operacion'];
        $fecha_pago = $importe['fecha_pago'];
        $cantidad = "$".number_format($importe['cantidad'],2);
        $tipo_envase = $importe['tipo_envase'];
        $estatus = $importe['estatus'] == 'PE' ? 'Pendiente' : 'Pagado';
        $usuario = $importe['nombre_usuario'];
        $opciones = '';
        
        switch($importe['estatus'])
        {
            case 'PE':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_importe' onclick='entregar_importe(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Entregar importe'>
                                        <i class='ace-icon fa fa-dollar bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'PA':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_importe' class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Entregado'>
                                        <i class='ace-icon fa fa-check bigger-130'></i>
                                    </a>
                                </div>";
                break;
        }
        
        $html .= "  <tr>
                        <td>$id_importe</td>
                        <td>$referencia</td>
                        <td>$fecha_operacion</td>
                        <td>$fecha_pago</td>
                        <td align='right'>$cantidad</td>
                        <td>$tipo_envase</td>
                        <td align='center'>$estatus</td>
                        <td>$usuario</td>
                        <td align='center'>$opciones</td>
                    </tr>";
    }
    
    return $html;
}
