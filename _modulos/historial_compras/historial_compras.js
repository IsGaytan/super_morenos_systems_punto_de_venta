//Agregar DatePicker a campos de tipo Fecha
$('#fecha_inicial_compras').datepicker();
$('#fecha_final_compras').datepicker();
//$('#fecha_inicial_compras_credito').datepicker();
//$('#fecha_final_compras_credito').datepicker();

/*$('#btn_buscar_compras_credito').click(function(){
    $.post('historial_compras/historial_compras.php',
    {
        fecha_inicial   : $('#fecha_inicial_compras_credito').val(),
        fecha_final     : $('#fecha_final_compras_credito').val(),
        function        : 'buscar_historial_compras_credito'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                $.gritter.add({
                    title: '¡CORRECTO!',
                    text: "EN ESTE MOMENTO HAY DATOS PARA MOSTRAR.",
                    class_name: 'gritter-success'
                });
            
            
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-warning'
                });
                break;
            case 3:
                $.gritter.add({
                    title: '¡ERROR!',
                    text: datos.mensaje,
                    class_name: 'gritter-error'
                });
                break;
        }
        
        
        
            
            
            /*$('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido.html);
            $('#total_ventas').val(datos.contenido.total);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
    });
});*/

$('#btn_buscar_compras').click(function(){
    $.post('historial_compras/historial_compras.php',
    {
        fecha_inicial   : $('#fecha_inicial_compras').val(),
        fecha_final     : $('#fecha_final_compras').val(),
        tipo_compra     : $('#select_tipo_compra').val(),
        function        : 'buscar_historial_compras'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                $.gritter.add({
                    title: '¡CORRECTO!',
                    text: "EN ESTE MOMENTO HAY DATOS PARA MOSTRAR.",
                    class_name: 'gritter-success'
                });
            
                $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
                $('#cuerpo_tabla').html(datos.contenido.html);
                $('#total_compras').val(datos.contenido.total);
                $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                    "language": {
                        "url":     "../assets/js/Spanish.json"
                    },
                    "bJQueryUI": true,
                    "bAutoWidth": false,
                    "aoColumns" : [
                        { 'sWidth': '20%' },
                        { 'sWidth': '15%' },
                        { 'sWidth': '20%' },
                        { 'sWidth': '15%' },
                        { 'sWidth': '15%' },
                        { 'sWidth': '15%' }
                    ]  
                });
                $('#section_tabla_resultados').show();
            
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-warning'
                });
                break;
            case 3:
                $.gritter.add({
                    title: '¡ERROR!',
                    text: datos.mensaje,
                    class_name: 'gritter-error'
                });
                break;
        }
    });
});

