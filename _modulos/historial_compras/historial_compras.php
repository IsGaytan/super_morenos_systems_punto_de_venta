<?php
session_start();
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'buscar_historial_compras_credito':
            buscar_historial_compras_credito($_POST);
        break;
        case 'buscar_historial_compras':
            buscar_historial_compras($_POST);
        break;
    }
}

function buscar_historial_compras_credito($datos_busqueda)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos_busqueda['fecha_inicial'], $datos_busqueda['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje'], 2);
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function buscar_historial_compras($datos_busqueda)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos_busqueda['fecha_inicial'], $datos_busqueda['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje'], 2);
        
        $conect = conectar_bd::realizar_conexion();
        
        //Where Fechas
        list($mes, $dia, $anio) = split('[/.-]', $datos_busqueda['fecha_inicial']);
        
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $datos_busqueda['fecha_final']);
        
        switch($datos_busqueda['tipo_compra'])
        {
            case '0':
                $where_fechas = "WHERE co.fecha_compra BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
                $query = "  SELECT co.fecha_compra, co.precio_compra, us.nombre, us.apellido_pat, '-' as cantidad_credito, '-' as proveedor, 'Común' as tipo_compra
                            FROM compras AS co
                            LEFT JOIN usuarios AS us ON co.id_usuario = us.id_usuario
                            $where_fechas";
                
                $query .= "UNION";
                
                $where_fechas = "WHERE ce.fecha_operacion BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
                $query .= "  SELECT ce.fecha_operacion AS fecha_compra, ce.total_compra AS precio_compra, us.nombre, us.apellido_pat, ce.total_a_credito as cantidad_credito, pr.nombre_proveedor as proveedor, 'Crédito' as tipo_compra
                            FROM compras_especiales AS ce
                            LEFT JOIN usuarios AS us ON ce.id_usuario = us.id_usuario
                            LEFT JOIN proveedores AS pr ON ce.id_proveedor = pr.id_proveedor
                            $where_fechas";
                break;
            case 'comun':
                $where_fechas = "WHERE co.fecha_compra BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
                $query = "  SELECT co.*, us.nombre, us.apellido_pat, '-' as cantidad_credito, '-' as proveedor, 'Común' as tipo_compra
                            FROM compras AS co
                            LEFT JOIN usuarios AS us ON co.id_usuario = us.id_usuario
                            $where_fechas";
                break;
            case 'credito':
                $where_fechas = "WHERE ce.fecha_operacion BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
                $query = "  SELECT ce.fecha_operacion AS fecha_compra, ce.total_compra AS precio_compra, us.nombre, us.apellido_pat, ce.total_a_credito as cantidad_credito, pr.nombre_proveedor as proveedor, 'Crédito' as tipo_compra
                            FROM compras_especiales AS ce
                            LEFT JOIN usuarios AS us ON ce.id_usuario = us.id_usuario
                            LEFT JOIN proveedores AS pr ON ce.id_proveedor = pr.id_proveedor
                            $where_fechas";
                break;
        }
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $resultado['contenido'] = armar_tabla_resultado($rows);

        } 
        else 
        {
            throw new Exception("No se encontraron datos");
        }

        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($compras)
{
    $respuesta = array(
        'html' => '',
        'total' => 0
    );
    
    foreach($compras as $compra)
    {
        $fecha          = $compra['fecha_compra'];
        $cantidad       = $compra['precio_compra'];
        $usuario        = $compra['nombre'] . " " . $compra['apellido_pat'];
        $cantidad_cre   = $compra['cantidad_credito'];
        $proveedor      = $compra['proveedor'];
        $tipo_compra    = $compra['tipo_compra'];
        
        $respuesta['html'] .= "<tr>
                                    <td>$fecha</td>
                                    <td>$cantidad</td>
                                    <td>$usuario</td>
                                    <td>$cantidad_cre</td>
                                    <td>$proveedor</td>
                                    <td>$tipo_compra</td>
                               </tr>";
        
        $respuesta['total'] += $cantidad;
    }
    
    return $respuesta;
}
