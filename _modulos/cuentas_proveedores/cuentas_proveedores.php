<?php
session_start();
require_once '../../_conection/bd_connect.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'buscar_cuentas_pagar':
            $datos_busqueda = $_POST;
            buscar_cuentas_pagar($datos_busqueda);
        break;
        case 'pagar_compra':
            $id_compra = $_POST['id_compra'];
            pagar_compra($id_compra);
        break;
    }
}

function buscar_cuentas_pagar($datos_busqueda)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $resultado = validar_campos($datos_busqueda);
        
        if($resultado['tipo'] == 3)
        {
            throw new Exception($resultado['mensaje']);
        }
        
        $conect = conectar_bd::realizar_conexion();
        
        $where_estatus = '';
        
        $fecha_inicial = $datos_busqueda['fecha_inicial'];
        list($mes, $dia, $anio) = split('[/.-]', $fecha_inicial);
        
        $fecha_final = $datos_busqueda['fecha_final'];
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $fecha_final);
        
        $id_proveedor = $datos_busqueda['id_proveedor'];
        $estatus = $datos_busqueda['estatus'];
        if($estatus != '0')
        {
            switch($estatus)
            {
                case 'PA':
                    $where_estatus = "AND estatus = 'PA'";
                break;
                case 'PE':
                    $where_estatus = "AND estatus = 'PE'";
                break;
            }
            
        }
        
        $query = "  SELECT ce.*, p.nombre_proveedor, p.saldo
                    FROM compras_especiales AS ce
                    LEFT JOIN proveedores AS p ON p.id_proveedor = ce.id_proveedor
                    WHERE ce.fecha_operacion BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'
                    AND ce.id_proveedor = $id_proveedor
                    $where_estatus";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            
            $resultado['contenido'] = armar_tabla_resultado($rows);
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        echo json_encode($resultado);
    }
}

function validar_campos($datos_busqueda)
{
    $resultado = array(
        'tipo' => 1,
        'mensaje' => ''
    );
    
    $fecha_inicial = $datos_busqueda['fecha_inicial'];
    $fecha_final = $datos_busqueda['fecha_final'];
    $id_proveedor = $datos_busqueda['id_proveedor'];
    
    $mensaje = '';
    
    if(trim($fecha_inicial) == '')
    {
        $resultado['tipo'] = 3;
        $mensaje .= ' Debe ingresar una fecha inicial.';
    }
    if(trim($fecha_final) == '')
    {
        $resultado['tipo'] = 3;
        $mensaje .= ' Debe ingresar una fecha final.';
    }
    if($id_proveedor == 0)
    {
        $resultado['tipo'] = 3;
        $mensaje .= ' Debe seleccionar un proveedor.';
    }
    
    if(trim($fecha_inicial) != '' && trim($fecha_final) != '')
    {
        //Validar que la fecha inicial no sea mayor que la fecha final
        list($mes, $dia, $anio) = split('[/.-]', $fecha_inicial);
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $fecha_final);
        
        if(($anio > $anio_f) || ($anio == $anio_f && $mes > $mes_f) || ($anio == $anio_f && $mes == $mes_f && $dia > $dia_f))
        {
            $resultado['tipo'] = 3;
            $mensaje .= ' La fecha inicial no debe ser mayor a la fecha final.';
        }
    }
        
    
    $resultado['mensaje'] = trim($mensaje);
    
    return $resultado;
}

function armar_tabla_resultado($datos)
{
    $html = '';
    
    foreach($datos as $valor)
    {
        $id_compra = $valor['id_compra'];
        $fecha_operacion   = substr($valor['fecha_operacion'], 0, 10);
        $total_compra   = $valor['total_compra'];
        $total_pagado = $valor['total_pagado'];
        $total_a_credito = $valor['total_a_credito'];
        $nombre_proveedor = $valor['nombre_proveedor'];
        $observaciones = $valor['observaciones'];
        $fecha_pago = substr($valor['fecha_pago'], 0, 10) == '0000-00-00' ? '-' : substr($valor['fecha_pago'], 0, 10);
        $referencia_pago = $valor['referencia_pago'] == 0 ? "-" : "Pago - " . $valor['referencia_pago'];
        
        switch($valor['estatus'])
        {
            case 'PE':
                $estatus = '<p class="orange">Pendiente</p>';
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_compra' onclick='pagar_compra(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Pagar Cuenta'>
                                        <i class='ace-icon fa fa-dollar bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'PA':
                $estatus = '<p class="green">Pagado</p>';
                $opciones = "   <div class='hiden-sm hiden-xs'>
                                    <a class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Pago realizado'>
                                        <i class='ace-icon fa fa-lock bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'RP':
                $estatus = '<p class="blue">Pago</p>';
                $opciones = "   <div class='hiden-sm hiden-xs'>
                                    <a class='blue tooltip-info black' data-rel='tooltip' data-placement='top' data-original-title='Referencia de pago'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                </div>";
                break;
        }
        
        $html .=    "<tr>
                        <td align='center'>$id_compra</td>
                        <td align='center'>$fecha_operacion</td>
                        <td align='center'>$fecha_pago</td>
                        <td align='center'>$referencia_pago</td>
                        <td align='right'>$total_compra</td>
                        <td align='right'>$total_pagado</td>
                        <td align='right'>$total_a_credito</td>
                        <td align='center'>$nombre_proveedor</td>
                        <td>$observaciones</td>
                        <td align='center'>$estatus</td>
                        <td align='center'>$opciones</td>
                    </tr>";
    }
    
    return $html;
}

function pagar_compra($id_compra)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $conect->begin_transaction();
        
        $query = "SELECT * FROM compras_especiales WHERE id_compra = $id_compra";
        
        $filas = $conect->query($query);
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            error_log("LA COMPRA..." . print_r($rows, true));
            
            $fecha_hoy = date("Y-m-d H:i:s");
            $total_compra = $rows[0]['total_compra'];
            $total_pagado = $rows[0]['total_pagado'];
            $total_a_credito = $rows[0]['total_a_credito'];
            $id_proveedor = $rows[0]['id_proveedor'];
            $id_usuario = $_SESSION['usuario']['id_usuario'];
            
            //Insertar el pago
            $query = "  INSERT INTO compras_especiales (fecha_operacion, total_compra, total_pagado, total_a_credito, observaciones, id_proveedor, id_usuario, estatus, fecha_pago, referencia_pago)
                        VALUES ('$fecha_hoy', $total_compra, $total_pagado, $total_a_credito, '', $id_proveedor, $id_usuario, 'RP', '', 0)";
            
            $insertar_fila = $conect->query($query);
        
            if(!$insertar_fila)
            {
                throw new Exception("Error al guardar la compra en la base de datos.");
            }
            
            $referencia_pago = $conect->insert_id;
            
            //Actualizar el estatus de la cuenta saldada
            $query = "UPDATE compras_especiales SET estatus = 'PA', fecha_pago = '$fecha_hoy', referencia_pago = $referencia_pago WHERE id_compra = $id_compra";
            $actualizar = $conect->query($query);
            if(!$actualizar)
            {
                throw new Exception("Error al actualizar el estatus del pago.");
            }
            
            //Actualizar el saldo al proveedor
            $query = "SELECT saldo FROM proveedores WHERE id_proveedor = $id_proveedor";
            $filas = $conect->query($query);
            if (!empty($filas) && $filas->num_rows > 0) 
            {
                $rows = $filas->fetch_all(MYSQLI_ASSOC);
                
                $saldo_proveedor = $rows[0]['saldo'];
                $nuevo_saldo = $saldo_proveedor - $total_a_credito;
                
                $query = "UPDATE proveedores SET saldo = $nuevo_saldo WHERE id_proveedor = $id_proveedor";
                $actualizar = $conect->query($query);
                if(!$actualizar)
                {
                    throw new Exception("Error al actualizar el estatus del pago.");
                }
            }
            else
            {
                throw new Exception("No se pudo consultar el saldo del proveedor");
            }
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        $conect->commit();
        $conect->close();
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
       echo json_encode($resultado);
    }
}