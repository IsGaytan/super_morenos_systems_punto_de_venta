//Convertir esos input en un datepicker
$('#fecha_inicial_cuentas').datepicker();
$('#fecha_final_cuentas').datepicker();

//Cargar el combo de proveedores
$.post('../_general/general.php',
{
    function: 'obtiene_combo_proveedores'
},
function(data){
    var datos = typeof data === 'object' ? data : JSON.parse(data);

    if(datos.tipo === 1)
    {
         $('#prov_cue_cobr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('#prov_cue_cobr').chosen();
         $('#prov_cue_cobr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
    }
    else
    {
         $('#prov_cue_cobr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('#prov_cue_cobr').chosen();
         $('#prov_cue_cobr').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
    }
});

function buscar_cuentas_pagar()
{
    $('#busca_cuentas_compras_error').hide();
    
    $.post('cuentas_proveedores/cuentas_proveedores.php',{
        fecha_inicial   : $('#fecha_inicial_cuentas').val(),
        fecha_final     : $('#fecha_final_cuentas').val(),
        id_proveedor    : $('#prov_cue_cobr').val(),
        estatus         : $('#select_estatus').val(),
        function        : 'buscar_cuentas_pagar'
    }, function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#section_tabla_resultados').hide(); 
            $('#busca_cuentas_compras_error').html(datos.mensaje);
            $('#busca_cuentas_compras_error').show();
            setTimeout(function(){
                $('#busca_cuentas_compras_error').hide(3000);
            }, 3000);
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '23%' },
                    { 'sWidth': '8%' },
                    { 'sWidth': '8%' }
                ]  
            });
            $('#section_tabla_resultados').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
        }
    });
}

function pagar_compra(id_compra)
{
    $.post('cuentas_proveedores/cuentas_proveedores.php',{
        id_compra   : id_compra,
        function    : 'pagar_compra'
    }, function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
        }
        else
        {
            $.gritter.add({
                title: 'Mensaje',
                text: "Pago registrado correctamente, en unos segundos lo verá reflejado.",
                class_name: 'gritter-success'
            });
            buscar_cuentas_pagar();
        }
    });
}