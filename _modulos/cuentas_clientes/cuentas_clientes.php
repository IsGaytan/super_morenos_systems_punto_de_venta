<?php
session_start();
require_once '../../_conection/bd_connect.php';
include '../../_general/general_functions.php';
require_once '../../_general/item_printer.php';
require_once '../vender/impresiones_vender.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'buscar_cuentas_cobrar':
            buscar_cuentas_cobrar($_POST);
            break;
        case 'abonar_cliente':
            abonar_cliente($_POST);
            break;
        case 'buscar_pagos':
            buscar_pagos($_POST['id_cliente']);
            break;
    }
}

function buscar_cuentas_cobrar($datos_buscar)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar las fechas y que seleccione un usuario
        $validacion = general_functions::valida_fechas($datos_buscar['fecha_inicial'], $datos_buscar['fecha_final']);
        if($validacion['hay_error'])
        {
            throw new Exception($validacion['mensaje']);
        }
        
        //Consultar el historial de ventas mediante el filtro de búsqueda
        $conect = conectar_bd::realizar_conexion();
        
        $where_estatus = '';
        $where_cliente = '';
        
        $fecha_inicial = $datos_buscar['fecha_inicial'];
        list($mes, $dia, $anio) = split('[/.-]', $fecha_inicial);
        
        $fecha_final = $datos_buscar['fecha_final'];
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $fecha_final);
        
        $id_cliente = $datos_buscar['id_cliente'];
        
        if($id_cliente != '0')
        {
            $where_cliente = "AND cl.id_cliente = $id_cliente";
        }
                
        $estatus = $datos_buscar['estatus'];

        if($estatus != '0')
        {
            switch($estatus)
            {
                case 'PA':
                    $where_estatus = "AND vc.estatus = 'PA'";
                break;
                case 'PE':
                    $where_estatus = "AND vc.estatus = 'PE'";
                break;
            }
        }
        
        $query = "  SELECT ve.*, vc.*, cl.nombre_cliente, cl.apellido_pat, cl.alias, us.nombre_usuario
                    FROM ventas_credito AS vc
                    LEFT JOIN ventas AS ve ON ve.id_venta = vc.id_venta
                    LEFT JOIN clientes AS cl ON cl.id_cliente = vc.id_cliente
                    LEFT JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    WHERE ve.fecha BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'
                    $where_cliente
                    $where_estatus";

        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $resultado['contenido'] = armar_tabla_resultado($rows);
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        echo json_encode($resultado);
    } catch (Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function abonar_cliente($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar los datos del formulario
        $mensaje = '';
        if($datos['id_cliente'] == 0)
            $mensaje .= ", Debe seleccionar un cliente";
        if($datos['cantidad_abonar'] == '0.00')
            $mensaje .= ", La cantidad a abonar no debe ser igual a $0.00";
        if(trim($datos['comentarios']) == '')
            $mensaje .= ", Debe ingresar los comentarios necesarios";
        
        if($mensaje != '')
        {
            $mensaje = substr($mensaje, 2);
            throw new Exception($mensaje);
        }
        
        //Datos del formulario
        $id_cliente = $datos['id_cliente'];
        $cantidad_abonar = $datos['cantidad_abonar'];
        $observaciones = $datos['comentarios'];
        
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        //Consultar las cuentas del cliente seleccionado que aún no están saldadas.
        $conect = conectar_bd::realizar_conexion();
        
        $query = "  SELECT vc.*, ve.total
                    FROM ventas_credito AS vc 
                    LEFT JOIN ventas AS ve ON ve.id_venta = vc.id_venta 
                    WHERE vc.estatus = 'PE' 
                        AND vc.id_cliente = $id_cliente";
        
        $filas = $conect->query($query);
        
        //Si hay cuentas pendientes, procesar el algoritmo para saldar cuentas dependiendo la cantidad.
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            //Abonar cuenta por cuenta
            $conect->begin_transaction();
            
            $cantidad_abonar_restante = $cantidad_abonar;
            $abono_real = 0;
            
            foreach($rows as $cuenta_pendiente)
            {
                $cantidad_a_cubrir = number_format($cuenta_pendiente['cantidad_credito'] - $cuenta_pendiente['cantidad_saldada'],2);
                
                if($cantidad_a_cubrir <= $cantidad_abonar_restante)
                {
                    $cantidad_abonar_restante = number_format($cantidad_abonar_restante - $cantidad_a_cubrir, 2);
                    
                    $abono_real += $cantidad_a_cubrir;
                    
                    $fecha_hoy = date("Y-m-d H:i:s");
                    $id_venta = $cuenta_pendiente['id_venta'];

                    //Cambiar estatus 'PE' por 'PA' a dicha venta y agregar cantidad_saldada = cantidad_credito
                    $stmt = $conect->prepare("UPDATE ventas_credito SET estatus = 'PA', cantidad_saldada = ?, fecha_pago = ?, id_usuario_recibio = ? WHERE id_venta = ?;");
                    $stmt->bind_param('dsii', $cuenta_pendiente['cantidad_credito'], $fecha_hoy, $id_usuario, $id_venta);
                    
                    $stmt->execute();
                    
                    
                }
                else if($cantidad_a_cubrir > $cantidad_abonar_restante)
                {
                    $cantidad_restante = $cantidad_a_cubrir - $cantidad_abonar_restante;
                    
                    $abono_real += $cantidad_abonar_restante;
                    
                    //Actualizar cantidad_saldada en la base de datos ingresando $cantidad_abonar_restante
                    $stmt = $conect->prepare("UPDATE ventas_credito SET cantidad_saldada = (cantidad_saldada + ?) WHERE id_venta = ?;");
                    $stmt->bind_param('di', $cantidad_abonar_restante, $id_venta);
                    $id_venta = $cuenta_pendiente['id_venta'];
                    $stmt->execute();
                    
                    $cantidad_abonar_restante = 0;
                }
            }
            //Insertar el pago en la tabla pago
            $stmt = $conect->prepare("INSERT INTO abonos (fecha, cantidad_abono, id_usuario, id_cliente, observaciones) VALUES (?,?,?,?,?);");
            $stmt->bind_param('sdiis', $fecha_hoy, $abono_real, $id_usuario, $id_cliente, $observaciones);
            
            $fecha_hoy = date("Y-m-d H:i:s");
            
            $stmt->execute();
            
            $id_abono = $conect->insert_id;
            
            //Actualizar saldo cliente
            $stmt = $conect->prepare("UPDATE clientes SET saldo = (saldo - ?) WHERE id_cliente = ?;");
            $stmt->bind_param('di', $abono_real, $id_cliente);
            $stmt->execute();
            
            //Obtener los datos del cliente para colocar en el ticket
            $query = "SELECT * FROM clientes WHERE id_cliente = $id_cliente";
            $filas = $conect->query($query);
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            
            $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
            $filas = $conect->query($query);
            $rows1 = $filas->fetch_all(MYSQLI_ASSOC);
            
            $impresion = new Impresiones_abonos($id_abono, $abono_real, $rows[0]['nombre_cliente'] . " " . $rows[0]['apellido_pat'], $rows[0]['saldo'], $rows1[0]['nombre'] . " " . $rows1[0]['apellido_pat']);
            $impresion ->imprimir_ticket_abono();
            
            $conect->commit();
            
            //Verificar si se consumió todo el abono o no
            if($cantidad_abonar_restante > 0)
            {
                $resultado['tipo'] = 2;
                $resultado['mensaje'] = "Se abonó correctamente, las cuentas del cliente están saldadas. El cambio es <b>$$cantidad_abonar_restante</b>";
            }
            else
            {
                $resultado['mensaje'] = "Se abonó correctamente, aún quedan cuentas pendientes. Si desea consultarlas, busque el historial de cuentas del cliente.";
            }
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function buscar_pagos($id_cliente)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $where_cliente = '';
        
        if($id_cliente != '0')
            $where_cliente = "WHERE ab.id_cliente = $id_cliente";
        
        $query = "  SELECT ab.*, cl.nombre_cliente, cl.apellido_pat, us.nombre_usuario 
                    FROM abonos AS ab
                    LEFT JOIN clientes AS cl ON cl.id_cliente = ab.id_cliente
                    LEFT JOIN usuarios AS us ON us.id_usuario = ab.id_usuario
                    $where_cliente";

        $filas = $conect->query($query);
        
        //Si hay cuentas pendientes, procesar el algoritmo para saldar cuentas dependiendo la cantidad.
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);
            
            error_log("LOS DATOS..." . print_r($rows, true));
            //Armar el html de la tabla
            foreach($rows as $abono)
            {
                $fecha = $abono['fecha'];
                $cantidad_abono = $abono['cantidad_abono'];
                $nombre_cliente = $abono['nombre_cliente'] . ' ' . $abono['apellido_pat'];
                $usuario = $abono['nombre_usuario'];
                $observaciones = $abono['observaciones'];
                
                $resultado['contenido'] .= "<tr>
                                                <td>$fecha</td>
                                                <td>".number_format($cantidad_abono, 2)."</td>
                                                <td>$nombre_cliente</td>
                                                <td>$usuario</td>
                                                <td>$observaciones</td>
                                            </tr>";
                
            }
        }
        else
        {
            throw new Exception("No se encontraron pagos.");
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
        
}

function armar_tabla_resultado($ventas)
{
    $html_tabla = '';
    foreach($ventas as $venta)
    {
        $nombre_cliente = $venta['nombre_cliente'] . ' ' . $venta['apellido_pat'];
        $fecha_venta = $venta['fecha'];
        $total = $venta['total'];
        $pagado = $venta['cantidad_cubierta'];
        $fiado = $venta['cantidad_credito'];
        $pagado_abonos = $venta['cantidad_saldada'];
        $vendio = $venta['nombre_usuario'];
        $observaciones = $venta['observaciones'];
        $estatus = $venta['estatus'] == 'PE' ? 'Pendiente' : 'Pagado';
        
        $html_tabla .= " <tr>
                            <td>$nombre_cliente</td>
                            <td>$fecha_venta</td>
                            <td>$total</td>
                            <td>$pagado</td>
                            <td>$fiado</td>
                            <td>$pagado_abonos</td>
                            <td>$observaciones</td>
                            <td>$estatus</td>
                        </tr>";
    }
    
    return $html_tabla;
}