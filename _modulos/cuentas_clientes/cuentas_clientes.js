//Convertir esos input en un datepicker
$('#fecha_inicial_cuentas').datepicker();
$('#fecha_final_cuentas').datepicker();
$('#cantidad_abonar').numeric();

//Cargar el combo de clientes para el cobro a crédito
$.post('clientes/clientes.php',
{
    function : 'obtener_lista_clientes'
}, function(data)
{
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    
    $('#clientes_cuentas_cobrar').html("<option value='0'>TODOS</option>" + datos.contenido);
    $('#clientes_cuentas_cobrar').chosen();
    $('#clientes_cuentas_cobrar_abonar').html("<option value='0'>*SELECCIONE UNO</option>" + datos.contenido);
    $('#clientes_cuentas_cobrar_abonar').chosen();
    $('#clientes_cuentas_buscar_abono').html("<option value='0'>TODOS</option>" + datos.contenido);
    $('#clientes_cuentas_buscar_abono').chosen();
});

function buscar_cuentas_cobrar()
{
    $.post('cuentas_clientes/cuentas_clientes.php', 
    {
        fecha_inicial : $('#fecha_inicial_cuentas').val(),
        fecha_final : $('#fecha_final_cuentas').val(),
        id_cliente : $('#clientes_cuentas_cobrar').val(),
        estatus : $('#select_estatus').val(),
        function : 'buscar_cuentas_cobrar'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo == 3)
        {
            $.gritter.add({
                title: '¡ALERTA!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
            $('#section_tabla_resultados').hide();
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
        }
    });
}

function abonar_cliente()
{
    $.post('cuentas_clientes/cuentas_clientes.php',
    {
       id_cliente : $('#clientes_cuentas_cobrar_abonar').val(),
       cantidad_abonar : $('#cantidad_abonar').val(),
       comentarios : $('#comentarios_abono').val(),
       function : 'abonar_cliente'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 1)
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: datos.mensaje,
                class_name: 'gritter-success'
            });
        }
        else if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ALERTA!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else if(datos.tipo === 2)
        {
            bootbox.dialog({
                message: "<h4 class='blue'>"+datos.mensaje+"</h4>",
                buttons:
                {
                    "success" :
                    {
                       "label" : "Ok",
                       "className" : "btn-sm btn-success",
                       "callback": function() {

                       }
                    }
                }
            });
        } 
    });
}

function buscar_pagos()
{
    $.post('cuentas_clientes/cuentas_clientes.php',
    {
        id_cliente : $('#clientes_cuentas_buscar_abono').val(),
        function : 'buscar_pagos'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ALERTA!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
            
            $('#section_tabla_abonos').hide();
        }
        else
        {
            $('#tabla_resultados_abonos').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla_abono').html(datos.contenido);
            $('#tabla_resultados_abonos').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '20%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '20%' }
                ]  
            });
            $('#section_tabla_abonos').show();
        }
    });
}


