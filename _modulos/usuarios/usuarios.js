function registrar_usuario()
{
    $.post('usuarios/usuarios.php',
    {
        nombre : $('#nombre_usuario').val(),
        apellido : $('#apellido_pat').val(),
        username : $('#username').val(),
        password : $('#password').val(),
        rol : $('#rol').val(),
        function : 'registrar_usuario'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 1)
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: 'Usuario registrado correctamente.',
                class_name: 'gritter-success'
            });
            
            $('#usuarios').click();
        }
        else
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
    });
}

function buscar_usuarios()
{
    $.post('usuarios/usuarios.php',
    {
        nombre : $('#nombre_buscar').val(),
        rol : $('#select_rol').val(),
        estatus : $('#select_estatus').val(),
        function : 'buscar_usuarios'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 1)
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '10%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
            $('.tooltip-error').tooltip();
        }
        else
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
    });
}

function eliminar_usuario(id_usuario)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea dar de baja este usuario?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Eliminar",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                    $.post('usuarios/usuarios.php',
                    {
                        id_usuario : id_usuario,
                        function : 'eliminar_usuario'
                    }, function(data)
                    {
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: '¡CORRECTO!',
                                text: 'El usuario ha sido dado de baja correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_usuarios').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: '¡ERROR!',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function reactivar_usuario(id_usuario)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea reactivar este usuario?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Reactivar",
                "className" : "btn-sm btn-success",
                "callback": function() {
                    $.post('usuarios/usuarios.php',
                    {
                        id_usuario : id_usuario,
                        function : 'reactivar_usuario'
                    }, function(data)
                    {
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: '¡CORRECTO!',
                                text: 'El usuario ha sido reactivado correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_usuarios').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: '¡ERROR!',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}