<?php
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST))
{
    switch($_POST['function'])
    {
        case 'registrar_usuario':
            registrar_usuario($_POST);
            break;
        case 'buscar_usuarios':
            buscar_usuarios($_POST);
            break;
        case 'eliminar_usuario':
            eliminar_usuario($_POST['id_usuario']);
            break;
        case 'reactivar_usuario':
            reactivar_usuario($_POST['id_usuario']);
            break;
    }
}

function registrar_usuario($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        //Validar los datos del formulario
        $hay_error = false;
        
        if(trim($datos['nombre']) == '' || trim($datos['apellido']) == '' || trim($datos['username']) == '' || trim($datos['password']) == '')
        {
            throw new Exception("Debe completar todos los datos del formulario.");
        }
        if(strpos($datos['username'], ' ') || strpos($datos['password'], " "))
        {
            throw new Exception("El nombre de usuario y/o contraseña <b>NO</b> deben contener espacios en blanco.");
        }
        
        //Realizar la inserción
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare("  INSERT INTO usuarios (nombre_usuario, password, rol, nombre, apellido_pat) VALUES (?,?,?,?,?)");
        $stmt->bind_param('sssss', $nombre_usuario, $password, $rol, $nombre, $apellido_pat);
        
        $nombre_usuario = $datos['username'];
        $password = $datos['password'];
        $rol = $datos['rol'];
        $nombre = $datos['nombre'];
        $apellido_pat = $datos['apellido'];
        
        $stmt->execute();
        
        $conect->close();
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}

function buscar_usuarios($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $agrego_where = false;
        $nombre     = $datos['nombre'];
        $rol        = $datos['rol'];
        $estatus    = $datos['estatus'];
        
        $query = "SELECT * FROM usuarios";
        
        if($nombre != '')
        {   
            $query .= " WHERE nombre_usuario = '$nombre'";
            
            $agrego_where = true;
        }
        if($rol != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND rol = '$rol'";
            }
            else
            {
                $query .= " WHERE rol = '$rol'";
            }
            $agrego_where = true;
        }
        if($estatus != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND estatus = '$estatus'";
            }
            else
            {
                $query .= " WHERE estatus = '$estatus'";
            }
        }
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            //Armar la tabla de resultados
            foreach($rows as $usuario)
            {
                $id_usuario = $usuario['id_usuario'];
                $nombre_usuario = $usuario['nombre_usuario'];
                $password = $usuario['password'];
                $rol = $usuario['rol'];
                $nombre = $usuario['nombre'] . ' ' . $usuario['apellido_pat'];
                $estatus = $usuario['estatus'] == 'AC' ? 'Activo' : 'Inactivo';

                switch($usuario['estatus'])
                {
                    case 'AC':
                        $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                            <a id = '$id_usuario' onclick='eliminar_usuario(this.id)' class='red tooltip-error' data-rel='tooltip' data-placement='top' data-original-title='Eliminar Usuario'>
                                                <i class='ace-icon fa fa-trash bigger-130'></i>
                                            </a>
                                            <a id = '$id_usuario' onclick='modificar_usuario(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Modificar Usuario'>
                                                <i class='ace-icon fa fa-pencil bigger-130'></i>
                                            </a>
                                        </div>";
                        break;
                    case 'BA':
                        $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                            <a id = '$id_usuario' onclick='reactivar_usuario(this.id)' class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Reactivar Usuario'>
                                                <i class='ace-icon fa fa-history bigger-130'></i>
                                            </a>
                                            <a id = '$id_usuario' onclick='modificar_usuario(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Modificar Usuario'>
                                                <i class='ace-icon fa fa-pencil bigger-130'></i>
                                            </a>
                                        </div>";
                        break;
                }
        
                $resultado['contenido'] .= "<tr>
                                                <td align='center'>$id_usuario</td>
                                                <td align='center'>$nombre</td>
                                                <td align='center'>$nombre_usuario</td>
                                                <td align='center'>$password</td>
                                                <td align='center'>". ucwords($rol) ."</td>
                                                <td align='center'>$estatus</td>
                                                <td align='center'>$opciones</td>
                                            </tr>";
            }
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function eliminar_usuario($id_usuario)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare(" UPDATE usuarios SET estatus = 'BA' WHERE id_usuario = ?");
        $stmt->bind_param('i', $id_usuario);
        
        $stmt->execute();
        
        $conect->close();
        
        echo json_encode($resultado);
        
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}

function reactivar_usuario($id_usuario)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => ''
        );
        
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare(" UPDATE usuarios SET estatus = 'AC' WHERE id_usuario = ?");
        $stmt->bind_param('i', $id_usuario);
        
        $stmt->execute();
        
        $conect->close();
        
        echo json_encode($resultado);
        
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}