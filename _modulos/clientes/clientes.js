$('.input-mask-phone').mask('999-999-9999');
$('#cp_cliente').mask('99999');

function dos_decimales(input){
    var monto = parseFloat($(input).val());
    $(input).val(monto.toFixed(2));
}

function registrar_cliente()
{
    $('#registro_cliente_error').hide();
    
    var nombre = $('#nombre_cliente').val();
    var apellido_pat = $('#apellido_pat').val();
    var apellido_mat = $('#apellido_mat').val();
    var alias = $('#alias').val();
    var telefono = $('#telefono_cliente').val();
    var calle = $('#calle_cliente').val();
    var numero = $('#numero_cliente').val();
    var colonia = $('#colonia_cliente').val();
    var municipio = $('#municipio_cliente').val();
    var cp = $('#cp_cliente').val();
    var limite = $('#limite_credito').val();
    var validacion = false;
    
    var respuesta = validar_campos_vacios(nombre, apellido_pat);
    
    if(respuesta != '')
    {
        $.gritter.add({
            title: '¡AVISO!',
            text: "<b>Los siguientes campos son requeridos: </b>" + respuesta + '.',
            class_name: 'gritter-warning'
        });
    }
    else
    {
        validacion = valida_solo_letras(nombre);
        if(!validacion)
        {
            $('#registro_cliente_error').html("Agregue un nombre válido");
            $('#registro_cliente_error').show();
        }
        else
        {
            validacion = valida_solo_letras(apellido_pat);
            if(!validacion)
            {
                $('#registro_cliente_error').html("Agregue un apellido paterno válido");
                $('#registro_cliente_error').show();
            }
            else
            {
                $('#registro_cliente_error').hide();
                $.post('clientes/clientes.php',
                {
                    nombre: nombre,
                    apellido_pat: apellido_pat,
                    apellido_mat: apellido_mat,
                    alias: alias,
                    telefono: telefono,
                    calle: calle,
                    numero: numero,
                    colonia: colonia,
                    municipio: municipio,
                    cp: cp,
                    limite: limite,
                    function: 'registra_cliente'
                },function(data){

                    var datos = typeof data === 'object' ? data : JSON.parse(data);

                    if(datos.tipo != 1)
                    {
                        $.gritter.add({
                            title: '¡ERROR!',
                            text: datos.mensaje,
                            class_name: 'gritter-error'
                        });
                    }
                    else
                    {
                        limpiar_campos();
                        $.gritter.add({
                            title: '¡CORRECTO!',
                            text: datos.mensaje,
                            class_name: 'gritter-success'
                        });
                    }
                });
            }
        }
    }
}

function buscar_clientes()
{
    var nombre = $('#nombre_buscar').val();
    var apellido = $('#apellido_pat_buscar').val();
    var alias = $('#alias_buscar').val();
    var estatus = $('#select_estatus').val();
    
    $.post('clientes/clientes.php',
    {
        nombre: nombre,
        apellido: apellido,
        alias: alias,
        estatus: estatus,
        function: 'consulta_clientes'
    },function(data){
        debugger;
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo != 1)
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#section_tabla_resultados').hide(); 
            $('#busca_cliente_error').html(datos.mensaje);
            $('#busca_cliente_error').show();
            setTimeout(function(){
                $('#busca_cliente_error').hide(1000);
            }, 3000);
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '5%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
            $('.tooltip-error').tooltip();
        }
    });
}

function eliminar_cliente(id_cliente)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea dar de baja este cliente?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-times'></i>Eliminar",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                    $.post('clientes/clientes.php',
                    {
                        id_cliente: id_cliente,
                        function : 'eliminar_cliente'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El cliente se ha dado de baja correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_clientes').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function reactivar_cliente(id_cliente)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea reactivar este cliente?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Reactivar",
                "className" : "btn-sm btn-success",
                "callback": function() {
                    $.post('clientes/clientes.php',
                    {
                        id_cliente: id_cliente,
                        function : 'reactivar_cliente'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El cliente se ha reactivado correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_clientes').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function modificar_cliente(id_cliente)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea modificar los datos de este cliente?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-pencil'></i>Modificar",
                "className" : "btn-sm btn-warning",
                "callback": function() {
                    $.post('clientes/clientes.php',
                    {
                        id_cliente: id_cliente,
                        function : 'obtener_datos_cliente'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            //Llenar el formulario con los datos del cliente
                            $('#nombre_cliente').val(datos.contenido.nombre_cliente);
                            $('#apellido_pat').val(datos.contenido.apellido_pat);
                            $('#apellido_mat').val(datos.contenido.apellido_mat);
                            $('#alias').val(datos.contenido.alias);
                            $('#telefono_cliente').val(datos.contenido.telefono);
                            $('#calle_cliente').val(datos.contenido.calle);
                            $('#numero_cliente').val(datos.contenido.numero);
                            $('#colonia_cliente').val(datos.contenido.colonia);
                            $('#municipio_cliente').val(datos.contenido.municipio);
                            $('#cp_cliente').val(datos.contenido.cp);
                            $('#limite_credito').val(datos.contenido.limite_credito);
                            
                            //Mostrar el botón de modificar y ocultar el de registrar
                            $('#modificar_cliente').attr('id_cliente', datos.contenido.id_cliente);
                            $('#modificar_cliente').show();
                            $('#registrar_cliente').hide(); 
                           
                            //Entrar al módulo de registrar para modificar los datos del cliente
                            $('#pestana_registrar_clientes').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }
                    });
                }
           }
        }
    });
}

function modificar_datos_cliente(id_cliente)
{
    $.post('clientes/clientes.php',
    {
        id_cliente      : id_cliente,
        nombre          : $('#nombre_cliente').val(),
        apellido_pat    : $('#apellido_pat').val(),
        apellido_mat    : $('#apellido_mat').val(),
        alias           : $('#alias').val(),
        telefono        : $('#telefono_cliente').val(),
        calle           : $('#calle_cliente').val(),
        numero          : $('#numero_cliente').val(),
        colonia         : $('#colonia_cliente').val(),
        municipio       : $('#municipio_cliente').val(),
        cp              : $('#cp_cliente').val(),
        limite          : $('#limite_credito').val(),
        function        : 'modificar_cliente'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo != 1)
        {
            $.gritter.add({
                title: '¡ERROR!',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
        }
        else
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: 'El cliente se ha modificado correctamente.',
                class_name: 'gritter-success'
            });
            $('#clientes').click();
        }
    });
}

function validar_campos_vacios(nombre, apellido_pat)
{
    var mensaje = '';
    
    if(nombre === '')
        mensaje = mensaje + ', Nombre del cliente';
    if(apellido_pat === '')
        mensaje = mensaje + ', Apellido paterno';
        
    if(mensaje != '')
    {
        mensaje = mensaje.substring(2);
    }
    
    return mensaje;
}

function valida_solo_letras( texto ) 
{
    expr = /^([a-zA-ZáéíóúÁÉÍÓÚ .-])+$/;
    if ( !expr.test(texto) )
        return false;
    else
        return true;
}

function limpiar_campos()
{
    $('#nombre_cliente').val('');
    $('#apellido_pat').val('');
    $('#apellido_mat').val('');
    $('#alias').val('');
    $('#telefono_cliente').val('');
    $('#calle_cliente').val('');
    $('#numero_cliente').val('');
    $('#colonia_cliente').val('');
    $('#municipio_cliente').val('');
    $('#cp_cliente').val('');
}

