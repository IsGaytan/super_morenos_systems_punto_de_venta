<?php
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'registra_cliente':
            $datos_cliente = $_POST;
            registra_cliente($datos_cliente);
            break;
        case 'consulta_clientes':
            $datos_cliente = $_POST;
            consulta_clientes($datos_cliente);
            break;
        case 'eliminar_cliente':
            $id_cliente = $_POST['id_cliente'];
            eliminar_cliente($id_cliente);
            break;
        case 'reactivar_cliente':
            $id_cliente = $_POST['id_cliente'];
            reactivar_cliente($id_cliente);
            break;
        case 'modificar_cliente':
            $datos_cliente = $_POST;
            modificar_cliente($datos_cliente);
            break;
        case 'obtener_datos_cliente':
            $id_cliente = $_POST['id_cliente'];
            obtener_datos_cliente($id_cliente);
            break;
        case 'obtener_lista_clientes':
            obtener_lista_clientes();
            break;
    }
}

function registra_cliente($datos_cliente)
{ 
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $nombre         = $datos_cliente['nombre'];
        $apellido_pat   = $datos_cliente['apellido_pat'];
        $apellido_mat   = $datos_cliente['apellido_mat'];
        $alias          = $datos_cliente['alias'];
        $telefono       = $datos_cliente['telefono'];
        $calle          = $datos_cliente['calle'];
        $numero         = $datos_cliente['numero'];
        $colonia        = $datos_cliente['colonia'];
        $municipio      = $datos_cliente['municipio'];
        $cp             = $datos_cliente['cp'];
        $saldo          = 0;
        $limite         = $datos_cliente['limite'] ? "" : 0;
        
        $conect->begin_transaction();
        
        $query = "INSERT INTO clientes (nombre_cliente, apellido_pat, apellido_mat, alias, telefono, calle, numero, colonia, municipio, cp, estatus, saldo, limite_credito)
                VALUES ('$nombre', '$apellido_pat', '$apellido_mat', '$alias', '$telefono', '$calle', '$numero', '$colonia', '$municipio', '$cp', 'AC', $saldo, $limite)";
        
        $insertar_fila = $conect->query($query);
        if(!$insertar_fila)
        {
            $resultado['tipo'] = 3;
            throw new Exception("Error al guardar el cliente en la base de datos.");
        }
        else
        {
            $resultado['mensaje'] = "Se guardó correctamente al cliente <b>$nombre $apellido_pat</b>";
        }
        
        $conect->commit();
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $conect->close();
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function modificar_cliente($datos_cliente)
{
    
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $funcion_general = new general_functions();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $mensaje = validar_campos($datos_cliente);
        
        if($mensaje != '')
        {
            throw new Exception("<b>Los siguientes campos son requeridos: </b>" . $mensaje);
        }
        else
        {
            $validacion = $funcion_general->valida_solo_letras($datos_cliente['nombre']);
            if(!$validacion)
                throw new Exception("Agregue un nombre válido.");
            else
            {
                $validacion = $funcion_general->valida_solo_letras($datos_cliente['apellido_pat']);
                if(!$validacion)
                    throw new Exception("Agregue un apellido paterno válido.");
                else
                {
                    $conect->begin_transaction();

                    $stmt = $conect->prepare("  UPDATE clientes SET nombre_cliente = ?, apellido_pat = ?, apellido_mat = ?, alias = ?, telefono = ?,
                                                calle = ?, numero = ?, colonia = ?, municipio = ?, cp = ?, limite_credito = ? WHERE id_cliente = ?");
                    $stmt->bind_param('ssssssssssdi', $datos_cliente['nombre'], $datos_cliente['apellido_pat'], $datos_cliente['apellido_mat'], $datos_cliente['alias'],
                                        $datos_cliente['telefono'], $datos_cliente['calle'], $datos_cliente['numero'], $datos_cliente['colonia'], $datos_cliente['municipio'],
                                        $datos_cliente['cp'], $datos_cliente['limite'], $datos_cliente['id_cliente']);

                    $stmt->execute();

                    $conect->commit();

                    $conect->close();
                }
            }
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function consulta_clientes($datos_cliente)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $agrego_where = false;
        $nombre     = $datos_cliente['nombre'];
        $apellido   = $datos_cliente['apellido'];
        $alias      = $datos_cliente['alias'];
        $estatus    = $datos_cliente['estatus'];
        
        
        $query = 'SELECT * FROM clientes';
        
        if($nombre != '')
        {   
            if($agrego_where)
            {
                $query .= " AND nombre_cliente = '$nombre'";
            }
            else
            {
                $query .= " WHERE nombre_cliente = '$nombre'";
            }
            $agrego_where = true;
        }
        
        if($apellido != '')
        {
            if($agrego_where)
            {
                $query .= " AND apellido_pat = '$apellido'";
            }
            else
            {
                $query .= " WHERE apellido_pat = '$municipio'";
            }
            $agrego_where = true;
        }
        
        if($alias != "")
        {
            if($agrego_where)
            {
                $query .= " AND alias = '$alias'";
            }
            else
            {
                $query .= " WHERE alias = '$alias'";
            }
            $agrego_where = true;
        }
        
        if($estatus != "0")
        {
            if($agrego_where)
            {
                $query .= " AND estatus = '$estatus'";
            }
            else
            {
                $query .= " WHERE estatus = '$estatus'";
            }
            $agrego_where = true;
        }
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
             $rows = $filas->fetch_all(MYSQLI_ASSOC);
             $resultado['contenido'] = armar_tabla_resultado_clientes($rows);
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $conect->close();
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function eliminar_cliente($id_cliente)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE clientes SET estatus = 'BA' WHERE id_cliente = ?");
        $stmt->bind_param('s', $id_cliente);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al dar de baja el cliente.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function reactivar_cliente($id_cliente)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE clientes SET estatus = 'AC' WHERE id_cliente = ?");
        $stmt->bind_param('s', $id_cliente);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al reactivar el cliente.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function obtener_datos_cliente($id_cliente)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $query = "SELECT * FROM clientes WHERE id_cliente = $id_cliente";
        
        $filas = $conect->query($query);
        
        if(!empty($filas) && $filas->num_rows > 0)
        {
            $datos_cliente = $filas->fetch_all(MYSQLI_ASSOC);
            $resultado['contenido'] = $datos_cliente[0];
        } 
        else 
        {
            throw new Exception("Error al obtener los datos del cliente.");
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado_clientes($datos_clientes)
{
    
    $html = '';
    
    foreach($datos_clientes as $datos_cliente)
    {
        $id_cliente = $datos_cliente['id_cliente'];
        $nombre_cliente = $datos_cliente['nombre_cliente'] . ' ' . $datos_cliente['apellido_pat'] . ' ' . $datos_cliente['apellido_mat'];
        $alias = $datos_cliente['alias'];
        $telefono = $datos_cliente['telefono'];
        $direccion = $datos_cliente['calle'] . ' ' . $datos_cliente['numero'] . ', ' . $datos_cliente['colonia'] . ', ' . $datos_cliente['municipio'] . ', ' . $datos_cliente['cp'];
        $saldo = $datos_cliente['saldo'];
        $limite = $datos_cliente['limite_credito'];
        $estatus = $datos_cliente['estatus'];
        $opciones = 'Opciones';
        
        switch($estatus)
        {
            case 'AC':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_cliente' onclick='eliminar_cliente(this.id)' class='red tooltip-error' data-rel='tooltip' data-placement='top' data-original-title='Eliminar'>
                                        <i class='ace-icon fa fa-trash bigger-130'></i>
                                    </a>
                                    <a id = '$id_cliente' onclick='modificar_cliente(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'BA':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_cliente' onclick='modificar_cliente(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                    <a id = '$id_cliente' onclick='reactivar_cliente(this.id)' class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Reactivar'>
                                        <i class='ace-icon fa fa-history bigger-130'></i>
                                    </a>
                                </div>";
                break;
        }
        
        $html .=    "<tr>
                        <td align='center'>$id_cliente</td>
                        <td>$nombre_cliente</td>
                        <td>$alias</td>
                        <td>$telefono</td>
                        <td>$direccion</td>
                        <td align='center'>$saldo</td>
                        <td align='center'>$limite</td>
                        <td align='center'>$estatus</td>
                        <td align='center'>$opciones</td>
                    </tr>";
    }
    
    return $html;
}

function validar_campos($datos_cliente)
{
    $mensaje = '';
    
    if($datos_cliente['nombre'] == '')
        $mensaje .= ', Nombre del cliente';
    if($datos_cliente['apellido_pat'] == '')
        $mensaje .= ', Apellido paterno';

    if($mensaje != '')
    {
        $mensaje = substr($mensaje, 2);
    }
    
    return $mensaje;
}

function obtener_lista_clientes()
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $query = "SELECT * FROM clientes WHERE estatus = 'AC'";
        
        $filas = $conect->query($query);
        
        if(!empty($filas) && $filas->num_rows > 0)
        {
            $datos_clientes = $filas->fetch_all(MYSQLI_ASSOC);
            
            foreach($datos_clientes as $cliente)
            {
                $id_cliente = $cliente['id_cliente'];
                $nombre_cliente = $cliente['nombre_cliente'] . " " . $cliente['apellido_pat'] . " " . "(" . $cliente['alias'] .")";
                
                $resultado['contenido'] .= "<option value='$id_cliente'>$nombre_cliente</option>"; 
            }
            
        } 
        else 
        {
            throw new Exception("");
        }
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}
