//Agregar DatePicker a campos de tipo Fecha
$('#fecha_inicial_balance').datepicker();
$('#fecha_final_balance').datepicker();

$.post('balance_caja/balance_caja.php',
{
    function    : 'combo_usuarios'
}, function(data)
{
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    
    $('#cajero_us').html(datos.contenido);
});

function generar_balance()
{
    $.post('balance_caja/balance_caja.php',
    {
        fecha_inicial   : $('#fecha_inicial_balance').val(),
        fecha_final     : $('#fecha_final_balance').val(),
        cajero          : $('#cajero_us').val(),
        function        : 'generar_balance'
    }, function(data)
    {
        debugger;
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
                $('#cuerpo_tabla').html(datos.contenido.html_tabla);
                $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                    "language": {
                        "url":     "../assets/js/Spanish.json"
                    },
                    "bJQueryUI": true,
                    "bAutoWidth": false,
                    "aoColumns" : [
                        { 'sWidth': '5%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '15%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' },
                        { 'sWidth': '10%' }
                    ]  
                });
                $('#cant_abo').html(datos.contenido.total_abonos);
                $('#cant_com').html(datos.contenido.total_compras);
                $('#cant_imp').html(datos.contenido.total_importe);
                $('#cant_rec').html(datos.contenido.total_recargas);
                $('#cant_efec_cont').html(datos.contenido.total_efect_cont);
                $('#cant_efec_cre').html(datos.contenido.total_efect_cred);
                $('#cant_val_cont').html(datos.contenido.total_vales_cont);
                $('#cant_tar_cont').html(datos.contenido.total_tarje_cont);
                $('#section_tabla_resultados').show();
                
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-warning'
                });
                break;
            case 3:
                $.gritter.add({
                    title: '¡ERROR!',
                    text: datos.mensaje,
                    class_name: 'gritter-error'
                });
                break;
        }
    });
}