<?php
session_start();
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'generar_balance':
            generar_balance($_POST);
            break;
        case 'combo_usuarios':
            combo_usuario();
            break;
    }
}

/*
 * Función para crear el html para un select donde se cargan los usuarios
 */
function combo_usuario()
{
    $resultado = array(
        'tipo' => 1,
        'contenido' => '<option value="0">Todos...</option>'
    );

    $conect = conectar_bd::realizar_conexion();

    $query = "SELECT id_usuario, nombre_usuario, rol, nombre, apellido_pat FROM usuarios WHERE estatus = 'AC'";

    $filas = $conect->query($query);

    if (!empty($filas) && $filas->num_rows > 0) 
    {
        $rows = $filas->fetch_all(MYSQLI_ASSOC);

        foreach($rows as $usuario)
        {
            $resultado['contenido'] .= "<option value='".$usuario['id_usuario']."'>".$usuario['nombre']." ".$usuario['apellido_pat']."</option>";
        }
    } 

    echo json_encode($resultado);   
}

function generar_balance($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );

        //Validar los datos del formulario
        $general = new general_functions;
        
        $validacion = $general->valida_fechas($datos['fecha_inicial'], $datos['fecha_final']);
        
        if($validacion['hay_error'])
            throw new Exception($validacion['mensaje'], 2);
        
        $conect = conectar_bd::realizar_conexion();
        
        //Where Fechas
        list($mes, $dia, $anio) = split('[/.-]', $datos['fecha_inicial']);
        
        list($mes_f, $dia_f, $anio_f) = split('[/.-]', $datos['fecha_final']);
        
        $where_fechas = "BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio_f-$mes_f-$dia_f 23:59:59'";
        
        $where_cajero = '';
        
        if($datos['cajero'] != '0')
        {
            $where_cajero = "AND us.id_usuario = " . $datos['cajero'];
        }
        
        $query = "  
                    SELECT ve.id_venta AS id_operacion, 'Venta' AS concepto, ve.fecha, ve.total, 
                            CASE
                                    ve.tipo WHEN '1' THEN 'Contado' 
                            END AS tipo_pago,
                        CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        CASE 
                                    ve.tipo_cobro WHEN 'EFECTIVO' THEN 'Efectivo'
                            END AS tipo_cobro,
                        '-' AS nombre_cliente, ' ' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM ventas AS ve
                    JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    WHERE ve.fecha $where_fechas
                    AND ve.tipo = 1 AND ve.tipo_cobro = 'EFECTIVO'
                    $where_cajero

                    UNION
                    
                    SELECT ve.id_venta AS id_operacion, 'Venta' AS concepto, ve.fecha, ve.total, 
                            CASE
                                    ve.tipo WHEN '1' THEN 'Contado' 
                            END AS tipo_pago,
                        CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        CASE 
                                    ve.tipo_cobro WHEN 'TARJETA_VALES' THEN 'Tarjeta Vales'
                            END AS tipo_cobro,
                        '-' AS nombre_cliente, ' ' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, vv.tarjetahabiente AS referencia 
                    FROM ventas AS ve
                    JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    JOIN ventas_vales AS vv ON vv.id_venta = ve.id_venta
                    WHERE ve.fecha $where_fechas
                    AND ve.tipo = 1 AND ve.tipo_cobro = 'TARJETA_VALES'
                    $where_cajero

                    UNION
                    
                    SELECT ve.id_venta AS id_operacion, 'Venta' AS concepto, ve.fecha, ve.total, 
                            CASE
                                    ve.tipo WHEN '1' THEN 'Contado' 
                            END AS tipo_pago,
                        CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        CASE 
                                    ve.tipo_cobro WHEN 'TARJETA' THEN 'Tarjeta C/D'
                            END AS tipo_cobro,
                        '-' AS nombre_cliente, ' ' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, vt.referencia AS referencia 
                    FROM ventas AS ve
                    JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    JOIN ventas_tarjeta AS vt ON vt.id_venta = ve.id_venta
                    WHERE ve.fecha $where_fechas
                    AND ve.tipo = 1 AND ve.tipo_cobro = 'TARJETA'
                    $where_cajero

                    UNION
                    
                    SELECT ve.id_venta AS id_operacion, 'Venta' AS concepto, ve.fecha, ve.total, 
                            CASE
                                    ve.tipo WHEN '2' THEN 'Crédito' 
                            END AS tipo_pago,
                        CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        CASE 
                                    ve.tipo_cobro WHEN 'EFECTIVO' THEN 'Efectivo'
                            END AS tipo_cobro,
                        cl.nombre_cliente AS nombre_cliente, cl.apellido_pat AS apellido_pat, vc.cantidad_credito, vc.cantidad_saldada, '-' AS referencia 
                    FROM ventas AS ve
                    JOIN usuarios AS us ON us.id_usuario = ve.id_usuario
                    JOIN ventas_credito AS vc ON vc.id_venta = ve.id_venta
                    JOIN clientes AS cl ON cl.id_cliente = vc.id_cliente
                    WHERE ve.fecha $where_fechas
                    AND ve.tipo = 2 AND ve.tipo_cobro = 'EFECTIVO'
                    $where_cajero

                    UNION

                    
                    SELECT re.id_recarga AS id_operacion, 'Recarga' AS concepto, re.fecha, re.monto AS total, 'Contado' AS tipo_pago,
                            CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        'Efectivo' AS tipo_cobro,
                        '-' AS nombre_cliente, '-' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM recargas AS re
                    JOIN usuarios AS us ON us.id_usuario = re.id_usuario
                    WHERE re.fecha $where_fechas
                    $where_cajero

                    UNION

                    SELECT im.id_importe AS id_operacion,
                            CASE
                                    im.estatus WHEN 'PA' THEN 'Importe pagado' WHEN 'PE' THEN 'Importe cobrado'
                            END AS concepto,
                            CASE
                                    im.estatus WHEN 'PA' THEN im.fecha_pago  WHEN 'PE' THEN im.fecha_operacion
                            END AS fecha,
                        CASE
                                    im.estatus WHEN 'PA' THEN im.cantidad*-1 WHEN 'PE' THEN im.cantidad
                            END AS total,
                            '-' AS tipo_pago,
                            CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        'Efectivo' AS tipo_cobro,
                        '-' AS nombre_cliente, '-' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM importes AS im
                    JOIN usuarios AS us ON us.id_usuario = im.id_usuario
                    WHERE (im.estatus = 'PE' AND im.fecha_operacion $where_fechas $where_cajero)
                            OR (im.estatus = 'PA' AND im.fecha_pago $where_fechas $where_cajero)
                    
                    UNION

                    SELECT im.id_importe AS id_operacion,
                            CASE
                                    im.estatus WHEN 'PA' THEN 'Importe cobrado'
                            END AS concepto,
                            CASE
                                    im.estatus WHEN 'PA' THEN im.fecha_operacion
                            END AS fecha,
                        CASE
                                    im.estatus WHEN 'PA' THEN im.cantidad
                            END AS total,
                            '-' AS tipo_pago,
                            CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        'Efectivo' AS tipo_cobro,
                        '-' AS nombre_cliente, '-' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM importes AS im
                    JOIN usuarios AS us ON us.id_usuario = im.id_usuario
                    WHERE im.estatus = 'PA' AND im.fecha_pago $where_fechas
                    $where_cajero

                    UNION
                    
                    SELECT ab.id_abono AS id_operacion, 'Abono a cuenta' AS concepto, ab.fecha, ab.cantidad_abono AS total, '-' AS tipo_pago,
                            CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        'Efectivo' AS tipo_cobro,
                        cl.nombre_cliente AS nombre_cliente, cl.apellido_pat AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM abonos AS ab
                    JOIN usuarios AS us ON us.id_usuario = ab.id_usuario
                    JOIN clientes AS cl On cl.id_cliente = ab.id_cliente
                    WHERE ab.fecha $where_fechas
                    $where_cajero

                    UNION

                    SELECT co.id_compra AS id_operacion, 'Compra rápida' AS concepto, co.fecha_compra, (co.precio_compra * -1) AS total, 'Contado' AS tipo_pago,
                            CONCAT(us.nombre, ' ', us.apellido_pat) AS nombre_cajero,
                        'Efectivo' AS tipo_cobro,
                        '-' AS nombre_cliente, '-' AS apellido_pat, '-' AS cantidad_credito, '-' AS cantidad_saldada, '-' AS referencia 
                    FROM compras AS co
                    JOIN usuarios AS us ON us.id_usuario = co.id_usuario
                    WHERE co.fecha_compra $where_fechas
                    AND co.compra_rapida = 'SI'
                    $where_cajero";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $resultado['contenido'] = armar_tabla_resultado($rows);
            
        } 
        else 
        {
            throw new Exception("No se encontraron datos", 2);
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage()
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($operaciones)
{
    $respuesta = array(
        'html_tabla' => '',
        'total_abonos' => 0,
        'total_compras' => 0,
        'total_importe' => 0,
        'total_recargas' => 0,
        'total_efect_cont' => 0,
        'total_efect_cred' => 0,
        'total_vales_cont' => 0,
        'total_tarje_cont' => 0
    );
    
    foreach($operaciones as $operacion)
    {
        $id_operacion   = $operacion['id_operacion'];
        $concepto       = $operacion['concepto']; 
        $fecha          = $operacion['fecha'];
        $total          = $operacion['total'];
        $tipo_pago      = $operacion['tipo_pago'];
        $nombre_cajero  = $operacion['nombre_cajero'];
        $tipo_cobro     = $operacion['tipo_cobro'];
        $cliente        = $operacion['nombre_cliente'] . ' ' . $operacion['apellido_pat'];
        
        $cantidad_credito = $operacion['cantidad_credito'];
        $cantidad_saldada = $operacion['cantidad_saldada'];
        
        if(is_numeric($cantidad_credito))
            $cantidad_credito = number_format($cantidad_credito, 2);
        
        if(is_numeric($cantidad_saldada))
            $cantidad_saldada = number_format($cantidad_saldada, 2);
        
        $referencia_tarjeta = $operacion['referencia'];
        
        $respuesta['html_tabla'] .= "   <tr>
                        <td align='center'>$id_operacion</td>
                        <td align='center'>$concepto</td>
                        <td align='center'>$fecha</td>
                        <td align='right'>$".number_format($total, 2)."</td>
                        <td align='center'>$tipo_pago</td>
                        <td align='center'>$nombre_cajero</td>
                        <td align='center'>$tipo_cobro</td>
                        <td align='center'>$cliente</td>
                        <td align='right'>$cantidad_credito / $cantidad_saldada</td>
                        <td align='center'>$referencia_tarjeta</td>
                    </tr>";
        
        switch(str_replace(' ', '', $concepto))
        {
            case 'Abonoacuenta':
                $respuesta['total_abonos'] += $total;
                break;
            case 'Comprarápida':
                $respuesta['total_compras'] += $total;
                break;
            case 'Importecobrado';
                $respuesta['total_importe'] += $total;
                break;
            case 'Importepagado':
                $respuesta['total_importe'] += $total;
                break;
            case 'Recarga':
                $respuesta['total_recargas'] += $total;
                break;
            case 'Venta':
                if($tipo_pago == 'Contado')
                {
                    switch(str_replace(' ','',$tipo_cobro))
                    {
                        case 'TarjetaVales':
                            $respuesta['total_vales_cont'] += $total;
                            break;
                        case 'Efectivo':
                            $respuesta['total_efect_cont'] += $total;
                            break;
                        case 'TarjetaC/D':
                            $respuesta['total_tarje_cont'] += $total;
                            break;
                    }
                }
                else
                {
                    $respuesta['total_efect_cred'] += $total;
                }
                break;
        }
    }
    
    $respuesta['total_abonos'] = "$" . number_format($respuesta['total_abonos'], 2);
    $respuesta['total_compras'] = "$" . number_format($respuesta['total_compras'], 2);
    $respuesta['total_importe'] = "$" . number_format($respuesta['total_importe'], 2);
    $respuesta['total_recargas'] = "$" . number_format($respuesta['total_recargas'], 2);
    $respuesta['total_efect_cont'] = "$" . number_format($respuesta['total_efect_cont'], 2);
    $respuesta['total_efect_cred'] = "$" . number_format($respuesta['total_efect_cred'], 2);
    $respuesta['total_vales_cont'] = "$" . number_format($respuesta['total_vales_cont'], 2);
    $respuesta['total_tarje_cont'] = "$" . number_format($respuesta['total_tarje_cont'], 2);
    
    return $respuesta;
}
