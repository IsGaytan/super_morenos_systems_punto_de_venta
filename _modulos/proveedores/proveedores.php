<?php
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'registra_proveedor':
            $datos_proveedor = $_POST;
            registra_proveedores($datos_proveedor);
        break;
        case 'consulta_proveedores':
            $datos_proveedor = $_POST;
            consulta_proveedores($datos_proveedor);
        break;
        case 'eliminar_proveedor':
            $id_proveedor = $_POST['id_proveedor'];
            eliminar_proveedor($id_proveedor);
        break;
        case 'reactivar_proveedor':
            $id_proveedor = $_POST['id_proveedor'];
            reactivar_proveedor($id_proveedor);
        break;
        case 'obtener_datos_proveedor':
            $id_proveedor = $_POST['id_proveedor'];
            obtener_datos_proveedor($id_proveedor);
        break;
        case 'modificar_datos_proveedor':
            $id_proveedor = $_POST;
            modificar_datos_proveedor($id_proveedor);
        break;
    }
}

function registra_proveedores($datos_proveedor)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $nombre = $datos_proveedor['nombre'];
        $telefono = $datos_proveedor['telefono'];
        $calle = $datos_proveedor['calle'];
        $numero = $datos_proveedor['numero_ext'];
        $colonia = $datos_proveedor['colonia'];
        $municipio = $datos_proveedor['municipio'];
        $estado = $datos_proveedor['estado'];
        $cp = $datos_proveedor['cp'];
        $periodo_visita = $datos_proveedor['periodo_visita'];
        $saldo = 0;
        
        $conect->begin_transaction();
        
        $query = "INSERT INTO proveedores (nombre_proveedor,telefono,calle, numero_ext, colonia, municipio, cp, estado, estatus, saldo)
                VALUES ('$nombre', '$telefono', '$calle', '$numero', '$colonia', '$municipio', '$cp', '$estado', 'AC', $saldo)";
        
        $insertar_fila = $conect->query($query);
        if(!$insertar_fila)
        {
            $resultado['tipo'] = 3;
            throw new Exception("Error al guardar el proveedor en la base de datos.");
        }
        else
        {
            $id_proveedor = $conect->insert_id;
            
            $respuesta = insertar_periodo_visita_proveedor($id_proveedor, $periodo_visita, $conect);
            if($respuesta['tipo'] === 3)
            {
                $resultado = $respuesta;
                throw new Exception($resultado['mensaje']);
            }
        }
        
        $conect->commit();
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $conect->close();
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function insertar_periodo_visita_proveedor($id_proveedor, $periodo_visita, $conect)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $lun = $periodo_visita['check_lun'];
        $mar = $periodo_visita['check_mar'];
        $mie = $periodo_visita['check_mie'];
        $jue = $periodo_visita['check_jue'];
        $vie = $periodo_visita['check_vie'];
        $sab = $periodo_visita['check_sab'];
        $dom = $periodo_visita['check_dom'];
        $periodo = $periodo_visita['periodo_semanal'];

        $query = "INSERT INTO dias_visita_proveedor(id_proveedor, lunes, martes, miercoles, jueves, viernes, sabado, domingo, periodo)
                VALUES ($id_proveedor, $lun, $mar, $mie, $jue, $vie, $sab, $dom, $periodo)";
        
        $insertar_fila = $conect->query($query);
        if(!$insertar_fila)
        {
            $resultado['tipo'] = 3;
            throw new Exception("Error al guardar la visita del proveedor en la base de datos.");
        }
        
        return $resultado;
    }catch(Exception $ex)
    {
        $conect->close();
        $resultado['mensaje'] = $ex->getMessage();
        return $resultado;
    }
}

function modificar_datos_proveedor($datos_proveedor)
{
    
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $funcion_general = new general_functions();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $mensaje = validar_campos_proveedor($datos_proveedor);
        
        if($mensaje != '')
        {
            throw new Exception($mensaje);
        }
        else
        {
            $validacion = $funcion_general->valida_solo_letras($datos_proveedor['nombre']);
            if(!$validacion)
                throw new Exception("Agregue un nombre válido.");
            else
            {
                $conect->begin_transaction();

                $stmt = $conect->prepare("  UPDATE proveedores SET nombre_proveedor = ?, telefono = ?, calle = ?, numero_ext = ?, colonia = ?,
                                            municipio = ?, cp = ?, estado = ? WHERE id_proveedor = ?");
                $stmt->bind_param('ssssssssi', $datos_proveedor['nombre'], $datos_proveedor['telefono'], $datos_proveedor['calle'], $datos_proveedor['numero_ext'], 
                                              $datos_proveedor['colonia'], $datos_proveedor['municipio'], $datos_proveedor['cp'], $datos_proveedor['estado'], $datos_proveedor['id_proveedor']);

                $stmt->execute();

                modificar_periodo_visita_proveedor($datos_proveedor['id_proveedor'], $datos_proveedor['periodo_visita'], $conect);

                $conect->commit();

                $conect->close();
            }
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function modificar_periodo_visita_proveedor($id_proveedor, $periodo_visita, $conect)
{
    $stmt = $conect->prepare("UPDATE dias_visita_proveedor SET lunes = ?, martes = ?, miercoles = ?, jueves = ?, viernes = ?, sabado = ?, domingo = ?, periodo = ?
                              WHERE id_proveedor = ?");
    
    $stmt->bind_param('iiiiiiiii', $periodo_visita['check_lun'], $periodo_visita['check_mar'], $periodo_visita['check_mie'], $periodo_visita['check_jue'],
            $periodo_visita['check_vie'], $periodo_visita['check_sab'], $periodo_visita['check_dom'], $periodo_visita['periodo_semanal'], $id_proveedor);
    
    $stmt->execute();
}
function consulta_proveedores($datos_proveedores)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $agrego_where = false;
        $nombre = $datos_proveedores['nombre'];
        $municipio = $datos_proveedores['municipio'];
        $dias = $datos_proveedores['dias'];
        $periodo = $datos_proveedores['periodo'];
        $estatus = $datos_proveedores['estatus'];
        
        $query = 'SELECT pr.*, dvp.* FROM proveedores pr INNER JOIN dias_visita_proveedor dvp ON dvp.id_proveedor = pr.id_proveedor';
        
        if($nombre != '')
        {   
            if($agrego_where)
            {
                $query .= " AND pr.nombre_proveedor = '$nombre'";
            }
            else
            {
                $query .= " WHERE pr.nombre_proveedor = '$nombre'";
            }
            $agrego_where = true;
        }
        
        if($municipio != '')
        {
            if($agrego_where)
            {
                $query .= " AND pr.municipio = '$municipio'";
            }
            else
            {
                $query .= " WHERE pr.municipio = '$municipio'";
            }
            $agrego_where = true;
        }
        
        if($estatus != "0")
        {
            if($agrego_where)
            {
                $query .= " AND pr.estatus = '$estatus'";
            }
            else
            {
                $query .= " WHERE pr.estatus = '$estatus'";
            }
            $agrego_where = true;
        }
        
        if($dias != "0")
        {
            if($agrego_where)
            {
                $query .= " AND dvp.$dias = 1";
            }
            else
            {
                $query .= " WHERE dvp.$dias = 1";
            }
            $agrego_where = true;
        }
        
        if($periodo != "0")
        {
            if($periodo === 'sem')
            {
                if($agrego_where)
                {
                    $query .= " AND dvp.periodo = 1";
                }
                else
                {
                    $query .= " WHERE dvp.periodo = 1";
                }
                $agrego_where = true;
            }
            if($periodo === 'qui')
            {
                if($agrego_where)
                {
                    $query .= " AND dvp.periodo = 0";
                }
                else
                {
                    $query .= " WHERE dvp.periodo = 0";
                }
                $agrego_where = true;
            }
        }
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $resultado['contenido'] = armar_tabla_resultado($rows);
             
        } else 
        {
            throw new Exception("No se encontraron datos");
        }
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($datos)
{
    $html = '';
    
    foreach($datos as $valor)
    {
        $id_proveedor       = $valor['id_proveedor'];
        $nombre_proveedor   = $valor['nombre_proveedor'];
        $telefono           = $valor['telefono'];
        $direccion          = $valor['calle'] . ' ' . $valor['numero_ext'] . ', ' . $valor['colonia'] . ', ' . $valor['municipio'] . ', ' . $valor['estado'] . ', ' . $valor['cp'];
        $estatus            = $valor['estatus'];
        $periodo            = '';
        $opciones            = '';
        
        switch($estatus)
        {
            case 'AC':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_proveedor' onclick='eliminar_proveedor(this.id)' class='red tooltip-error' data-rel='tooltip' data-placement='top' data-original-title='Eliminar'>
                                        <i class='ace-icon fa fa-trash bigger-130'></i>
                                    </a>
                                    <a id = '$id_proveedor' onclick='modificar_proveedor(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'BA':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_proveedor' onclick='modificar_proveedor(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                    <a id = '$id_proveedor' onclick='reactivar_proveedor(this.id)' class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Reactivar'>
                                        <i class='ace-icon fa fa-history bigger-130'></i>
                                    </a>
                                </div>";
                break;
        }
        
        $estatus            = $valor['estatus'] == 'AC' ? "Activo" : "Inactivo";
        
        if($valor['lunes'] === "1")
        {
            $periodo .= 'Lun - ';
        }
        
        if($valor['martes'] === "1")
        {
            $periodo .= 'Mar - ';
        }
        if($valor['miercoles'] === "1")
        {
            $periodo .= 'Mie - ';
        }
        if($valor['jueves'] === "1")
        {
            $periodo .= 'Jue - ';
        }
        if($valor['viernes'] === "1")
        {
            $periodo .= 'Vie - ';
        }
        if($valor['sabado'] === "1")
        {
            $periodo .= 'Sab - ';
        }
        if($valor['domingo'] === "1")
        {
            $periodo .= 'Dom - ';
        }
        
        $periodo = substr($periodo, 0, strlen($periodo)-2);
        
        if($valor['periodo'] === "1")
        {
            $periodo .= ' | Semanalmente';
        }
        else
        {
            $periodo .= ' | Quincenalmente';
        }
        
        $html .=    "<tr>
                        <td align='center'>$id_proveedor</td>
                        <td>$nombre_proveedor</td>
                        <td align='center'>$telefono</td>
                        <td>$direccion</td>
                        <td>$periodo</td>
                        <td align='center'>$estatus</td>
                        <td align='center'>$opciones</td>
                    </tr>";
    }
    
    return $html;
}

function eliminar_proveedor($id_proveedor)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE proveedores SET estatus = 'BA' WHERE id_proveedor = ?");
        $stmt->bind_param('s', $id_proveedor);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al dar de baja el proveedor.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function reactivar_proveedor($id_proveedor)
{
    
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE proveedores SET estatus = 'AC' WHERE id_proveedor = ?");
        $stmt->bind_param('s', $id_proveedor);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al reactivar el proveedor.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function obtener_datos_proveedor($id_proveedor)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $query = "SELECT * FROM proveedores WHERE id_proveedor = $id_proveedor";
        
        $filas = $conect->query($query);
        
        if(!empty($filas) && $filas->num_rows > 0)
        {
            $datos_cliente = $filas->fetch_all(MYSQLI_ASSOC);
            $resultado['contenido'] = $datos_cliente[0];
            
            $query = "SELECT * FROM dias_visita_proveedor WHERE id_proveedor = $id_proveedor";
            $filas = $conect->query($query);
            if(!empty($filas) && $filas->num_rows > 0)
            {
                $dias_visita = $filas->fetch_all(MYSQLI_ASSOC);
                $resultado['contenido']['dias_visita'] = $dias_visita[0];
            }
            else
            {
                throw new Exception("Error al obtener los datos del proveedor.");
            }
        } 
        else 
        {
            throw new Exception("Error al obtener los datos del proveedor.");
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function validar_campos_proveedor($datos_proveedores)
{
    $mensaje = '';
    $hay_periodo = true;
    
    if(trim($datos_proveedores['nombre']) == '')
    {
        $mensje .= ', Nombre';
    }
    
    foreach($datos_proveedores['periodo_visita'] as $elemento => $valor)
    {
        if($elemento == 'periodo_semanal' || $elemento == 'periodo_quincenal')
        {
            continue;
        }
        if($valor == '1')
        {
            $hay_periodo = true;
            break;
        }
        else
        {
            $hay_periodo = false;
        }
    }
    if(!$hay_periodo)
        $mensaje .= ', Periodo de visita';
    
    if($mensaje != '')
        $mensaje = substr($mensaje, 2);
    
    return $mensaje;
}