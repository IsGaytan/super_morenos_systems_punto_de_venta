$('.input-mask-phone').mask('999-999-9999');
$('#cp').mask('99999');

function registrar_proveedor()
{
    $('#registro_proveedor_error').hide();
    
    var nombre = $('#nombre').val();
    var telefono = $('#telefono').val();
    var calle = $('#calle').val();
    var numero_ext = $('#numero_ext').val();
    var colonia = $('#colonia').val();
    var municipio = $('#municipio').val();
    var estado = $('#estado').val();
    var cp = $('#cp').val();
    var periodo_visita = {};
    var validacion = false;
    
    if($("#radio_semanal").is(':checked'))
        periodo_visita['periodo_semanal'] = 1;
    else
        periodo_visita['periodo_semanal'] = 0;
    
    if($("#radio_quincenal").is(':checked'))
        periodo_visita['periodo_quincenal'] = 1;
    else
        periodo_visita['periodo_quincenal'] = 0;
    
    if($("#check_lun").is(':checked'))
        periodo_visita['check_lun'] = 1;
    else
        periodo_visita['check_lun'] = 0;
    
    if($("#check_mar").is(':checked'))
        periodo_visita['check_mar'] = 1;
    else
        periodo_visita['check_mar'] = 0;
    
    if($("#check_mie").is(':checked'))
        periodo_visita['check_mie'] = 1;
    else
        periodo_visita['check_mie'] = 0;
    
    if($("#check_jue").is(':checked'))
        periodo_visita['check_jue'] = 1;
    else
        periodo_visita['check_jue'] = 0;
    
    if($("#check_vie").is(':checked'))
        periodo_visita['check_vie'] = 1;
    else
        periodo_visita['check_vie'] = 0;
    
    if($("#check_sab").is(':checked'))
        periodo_visita['check_sab'] = 1;
    else
        periodo_visita['check_sab'] = 0;
    
    if($("#check_dom").is(':checked'))
        periodo_visita['check_dom'] = 1;
    else
        periodo_visita['check_dom'] = 0;
    
    var respuesta = validar_campos_vacios(nombre, periodo_visita);
            
    if(respuesta != '')
    {
        $('#registro_proveedor_error').html("<b>Los siguientes campos son requeridos: </b>" + respuesta + '.');
        $('#registro_proveedor_error').show();
    }
    else
    {
        validacion = valida_solo_letras(nombre);
        if(!validacion)
        {
            $('#registro_proveedor_error').html("Agregue un nombre válido");
            $('#registro_proveedor_error').show();
        }
        else
        {
            $('#registro_proveedor_error').hide();
            $.post('proveedores/proveedores.php',
            {
                nombre: nombre,
                telefono: telefono,
                calle: calle,
                numero_ext: numero_ext,
                colonia: colonia,
                municipio: municipio,
                estado: estado,
                cp: cp,
                periodo_visita: periodo_visita,
                function: 'registra_proveedor'
            },function(data){
                debugger;
                var datos = typeof data === 'object' ? data : JSON.parse(data);

                if(datos.tipo != 1)
                {
                    $('#registro_proveedor_error').html(datos.mensaje);
                    $('#registro_proveedor_error').show();
                    setTimeout(function(){
                        $('#registro_proveedor_error').hide(1000);
                    }, 3000);
                }
                else
                {
                    limpiar_campos();

                    $('#registro_proveedor_success').html("El proveedor ha sido registrado correctamente.");
                    $('#registro_proveedor_success').show();
                    setTimeout(function(){
                        $('#registro_proveedor_success').hide(500);
                    }, 3000);
                }
            });
        }
    }
}

function buscar_proveedores()
{
    var nombre = $('#nombre_buscar').val();
    var municipio = $('#municipio_buscar').val();
    var dias = $('#select_dias').val();
    var periodo = $('#select_periodo_visita').val();
    var estatus = $('#select_estatus').val();
    
    $.post('proveedores/proveedores.php',
    {
        nombre: nombre,
        municipio: municipio,
        dias: dias,
        periodo: periodo,
        estatus: estatus,
        function: 'consulta_proveedores'
    },function(data){
        debugger;
        var datos = typeof data === 'object' ? data : JSON.parse(data);

        if(datos.tipo != 1)
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#section_tabla_resultados').hide(); 
            $('#busca_proveedor_error').html(datos.mensaje);
            $('#busca_proveedor_error').show();
            setTimeout(function(){
                $('#busca_proveedor_error').hide(500);
            }, 3000);
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '15%' },
                    { 'sWidth': '25%' },
                    { 'sWidth': '20%' },
                    { 'sWidth': '10%' },
                    { 'sWidth': '10%' }
                ]  
            });
            $('#section_tabla_resultados').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
            $('.tooltip-error').tooltip();
        }
    });
}

function eliminar_proveedor(id_proveedor)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea dar de baja este proveedor?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-times'></i>Eliminar",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                    $.post('proveedores/proveedores.php',
                    {
                        id_proveedor: id_proveedor,
                        function : 'eliminar_proveedor'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El proveedor se ha dado de baja correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_proveedores').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function reactivar_proveedor(id_proveedor)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea reactivar este proveedor?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Reactivar",
                "className" : "btn-sm btn-success",
                "callback": function() {
                    $.post('proveedores/proveedores.php',
                    {
                        id_proveedor: id_proveedor,
                        function : 'reactivar_proveedor'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El proveedor se ha reactivado correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_proveedores').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function modificar_proveedor(id_proveedor)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea modificar los datos de este proveedor?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-pencil'></i>Modificar",
                "className" : "btn-sm btn-warning",
                "callback": function() {
                    $.post('proveedores/proveedores.php',
                    {
                        id_proveedor: id_proveedor,
                        function : 'obtener_datos_proveedor'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            //Llenar el formulario con los datos del cliente
                            $('#nombre').val(datos.contenido.nombre_proveedor);
                            $('#telefono').val(datos.contenido.telefono);
                            $('#calle').val(datos.contenido.calle);
                            $('#numero_ext').val(datos.contenido.numero_ext);
                            $('#colonia').val(datos.contenido.colonia);
                            $('#municipio').val(datos.contenido.municipio);
                            $('#cp').val(datos.contenido.cp);
                            $('#estado').val(datos.contenido.estado);
                            debugger;
                            if(datos.contenido.dias_visita.lunes === "1")
                                $('#check_lun').prop('checked', 'true');
                            if(datos.contenido.dias_visita.martes === "1")
                                $('#check_mar').prop('checked', 'true');
                            if(datos.contenido.dias_visita.miercoles === "1")
                                $('#check_mie').prop('checked', 'true');
                            if(datos.contenido.dias_visita.jueves === "1")
                                $('#check_jue').prop('checked', 'true');
                            if(datos.contenido.dias_visita.viernes === "1")
                                $('#check_vie').prop('checked', 'true');
                            if(datos.contenido.dias_visita.sabado === "1")
                                $('#check_sab').prop('checked', 'true');
                            if(datos.contenido.dias_visita.domingo === "1")
                                $('#check_dom').prop('checked', 'true');
                            if(datos.contenido.dias_visita.periodo === "0")
                                $('#radio_quincenal').prop('checked', true);
                            
                            //Mostrar el botón de modificar y ocultar el de registrar
                            $('#modificar_proveedor').attr('id_proveedor', datos.contenido.id_proveedor);
                            $('#modificar_proveedor').show();
                            $('#registrar_proveedor').hide(); 
                           
                            //Entrar al módulo de registrar para modificar los datos del cliente
                            $('#pestana_registrar_proveedores').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }
                    });
                }
           }
        }
    });
}

function modificar_datos_proveedor(id_proveedor)
{
    var periodo_visita = {};
    
    if($("#radio_semanal").is(':checked'))
        periodo_visita['periodo_semanal'] = 1;
    else
        periodo_visita['periodo_semanal'] = 0;
    
    if($("#radio_quincenal").is(':checked'))
        periodo_visita['periodo_quincenal'] = 1;
    else
        periodo_visita['periodo_quincenal'] = 0;
    
    if($("#check_lun").is(':checked'))
        periodo_visita['check_lun'] = 1;
    else
        periodo_visita['check_lun'] = 0;
    
    if($("#check_mar").is(':checked'))
        periodo_visita['check_mar'] = 1;
    else
        periodo_visita['check_mar'] = 0;
    
    if($("#check_mie").is(':checked'))
        periodo_visita['check_mie'] = 1;
    else
        periodo_visita['check_mie'] = 0;
    
    if($("#check_jue").is(':checked'))
        periodo_visita['check_jue'] = 1;
    else
        periodo_visita['check_jue'] = 0;
    
    if($("#check_vie").is(':checked'))
        periodo_visita['check_vie'] = 1;
    else
        periodo_visita['check_vie'] = 0;
    
    if($("#check_sab").is(':checked'))
        periodo_visita['check_sab'] = 1;
    else
        periodo_visita['check_sab'] = 0;
    
    if($("#check_dom").is(':checked'))
        periodo_visita['check_dom'] = 1;
    else
        periodo_visita['check_dom'] = 0;
    
    $.post('proveedores/proveedores.php',
    {
        id_proveedor    : id_proveedor,
        nombre          : $('#nombre').val(),
        telefono        : $('#telefono').val(),
        calle           : $('#calle').val(),
        numero_ext      : $('#numero_ext').val(),
        colonia         : $('#colonia').val(),
        municipio       : $('#municipio').val(),
        estado          : $('#estado').val(),
        cp              : $('#cp').val(),
        periodo_visita  : periodo_visita,
        function        : 'modificar_datos_proveedor'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo != 1)
        {
            $('#registro_proveedor_error').html(datos.mensaje);
            $('#registro_proveedor_error').show();
            setTimeout(function(){
                $('#registro_proveedor_error').hide(500);
            }, 3000);
        }
        else
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'Datos del proveedor modificados correctamente.',
                class_name: 'gritter-success'
            });
            $('#proveedores').click();
        }
    });
}

function validar_campos_vacios(nombre, dias_visita)
{
    
    var mensaje = '';
    if(nombre === '')
        mensaje = mensaje + ', Nombre del proveedor';
    
    var error = false;
    $.each( dias_visita, function( key, value ) {
        if(key === 'periodo_semanal' || key === 'periodo_quincenal')
        {
            error = false;
        }
        else
        {
            if(value === 1)
            {
                error = false;
                return false;
            }   
            else
            {
                error = true;
            }
        }
    });
    
    if(error)
        mensaje = mensaje + ', Días de visita';
        
    if(mensaje != '')
    {
        mensaje = mensaje.substring(2);
    }
    
    return mensaje;
}

function valida_solo_letras( texto ) {
    expr = /^([a-zA-ZáéíóúÁÉÍÓÚ .-])+$/;
    if ( !expr.test(texto) )
        return false;
    else
        return true;
}

function limpiar_campos()
{
    $("#check_lun").prop('checked', false);
    $("#check_mar").prop('checked', false);
    $("#check_mie").prop('checked', false);
    $("#check_jue").prop('checked', false);
    $("#check_vie").prop('checked', false);
    $("#check_sab").prop('checked', false);
    $("#check_dom").prop('checked', false);
    
    $('#nombre').val('');
    $('#telefono').val('');
    $('#calle').val('');
    $('#numero_ext').val('');
    $('#colonia').val('');
    $('#municipio').val('');
    $('#estado').val('');
    $('#cp').val('');
}