<?php
require_once '../../_conection/bd_connect.php';
require_once '../../_librerias/php_excel/Classes/PHPExcel.php';
require_once '../../_librerias/php_excel/Classes/PHPExcel/IOFactory.php';

define('ABARROTES', 1);
define('FERRETERIA', 2);
define('CREMERIA', 3);
define('PAPELERIA', 4);

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'registrar_productos':
            $datos_producto = $_POST;
            registrar_productos($datos_producto);
            break;
        case 'busca_productos':
            $datos_producto_buscar = $_POST;
            busca_productos($datos_producto_buscar);
            break;
        case 'eliminar_producto':
            $id_producto = $_POST['id_producto'];
            eliminar_producto($id_producto);
            break;
        case 'reactivar_producto':
            $id_producto = $_POST['id_producto'];
            reactivar_producto($id_producto);
            break;
        case 'obtener_datos_producto':
            $id_producto = $_POST['id_producto'];
            obtener_datos_producto($id_producto);
            break;
        case 'modificar_producto':
            $datos_producto = $_POST;
            modificar_producto($datos_producto);
            break;
        case 'generar_excel':
            generar_excel($_POST);
            break;
    }
}

function registrar_productos($datos_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $mensaje = validar_campos($datos_producto);
        
        if($mensaje != '')
        {
            throw new Exception("<b>Los siguientes campos son requeridos: </b>" . $mensaje);
        }
        
        $marca = $datos_producto['marca'];
        $pres_medida = $datos_producto['pres_medida'];
        $pres_unidad = $datos_producto['pres_unidad'];
        $unidad_medida = $datos_producto['unidad_medida'];
        $precio_compra = $datos_producto['precio_compra'];
        $porcentaje = $datos_producto['porcentaje'];
        $precio_venta = $datos_producto['precio_venta'];
        $codigo_barras = $datos_producto['codigo_barras'];
        $texto_ticket = $datos_producto['texto_ticket'];
        $existencia = $datos_producto['existencia'];
        $categoria = $datos_producto['categoria'];
        $id_proveedor = $datos_producto['id_proveedor'];
        $venta_libre = $datos_producto['venta_libre'];
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("  INSERT INTO productos (marca, pres_medida, pres_unidad, unidad_medida, precio_compra, precio_venta, codigo_barras, texto_ticket, existencia, categoria, id_proveedor, porcentaje, venta_libre)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        
        $stmt->bind_param('ssssddssdiidi', $marca, $pres_medida, $pres_unidad, $unidad_medida, $precio_compra, $precio_venta, $codigo_barras, $texto_ticket, $existencia, $categoria, $id_proveedor, $porcentaje, $venta_libre);

        $stmt->execute();
        
        $conect->commit();
        
        $conect->close();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        $conect->close();
        echo json_encode($resultado);
    }
}

function modificar_producto($datos_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $mensaje = validar_campos($datos_producto);
        
        if($mensaje != '')
        {
            throw new Exception("<b>Los siguientes campos son requeridos: </b>" . $mensaje);
        }
        
        $id_producto = $datos_producto['id_producto'];
        $marca = $datos_producto['marca'];
        $pres_medida = $datos_producto['pres_medida'];
        $pres_unidad = $datos_producto['pres_unidad'];
        $unidad_medida = $datos_producto['unidad_medida'];
        $precio_compra = $datos_producto['precio_compra'];
        $porcentaje = $datos_producto['porcentaje'];
        $precio_venta = $datos_producto['precio_venta'];
        $codigo_barras = $datos_producto['codigo_barras'];
        $texto_ticket = $datos_producto['texto_ticket'];
        $existencia = $datos_producto['existencia'];
        $categoria = $datos_producto['categoria'];
        $id_proveedor = $datos_producto['id_proveedor'];
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("  UPDATE productos SET marca = ?, pres_medida = ?, pres_unidad = ?, unidad_medida = ?, precio_compra = ?, 
                                    precio_venta = ?, codigo_barras = ?, texto_ticket = ?, existencia = ?, categoria = ?, id_proveedor = ?, porcentaje = ? WHERE id_producto = ?");
        
        $stmt->bind_param('ssssddssdiidi', $marca, $pres_medida, $pres_unidad, $unidad_medida, $precio_compra, $precio_venta, $codigo_barras, $texto_ticket, $existencia, $categoria, $id_proveedor, $porcentaje, $id_producto);

        $stmt->execute();
        
        $conect->commit();
        
        $conect->close();
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function busca_productos($datos_producto)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $respuesta = get_productos($datos_producto);
        
        if ($respuesta['tipo'] == 1) 
        {
             $resultado['contenido'] = armar_tabla_resultado($respuesta['contenido']);
        } else 
        {
            throw new Exception($respuesta['mensaje']);
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}


function generar_excel($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $respuesta = get_productos($datos);

        if ($respuesta['tipo'] == 1) 
        {
            $genera_excel = new PHPExcel();
            
            //Agregar el estido de centrado a TODAS las celdas del documento
            $genera_excel->getDefaultStyle()
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            //Enviar un par de metadatos para el documento
            $genera_excel->getProperties()
                                ->setCreator("Super Morenos")
                                ->setLastModifiedBy("Super Morenos")
                                ->setTitle("Exportar Excel con PHP")
                                ->setSubject("Documento de prueba")
                                ->setDescription("Documento generado con PHPExcel")
                                ->setCategory("reportes");
             
            //Indicar que de la celda A1 a la celda J1 estarán combinadas
            $genera_excel->setActiveSheetIndex(0)
                                ->mergeCells('A1:J1');
            
            //Definir el encabezado del documento (Título del reporte y columnas)
            $genera_excel->setActiveSheetIndex(0)
                                ->setCellValue('A1', "REPORTE DE PRODUCTOS - Super Moreno's System")
                                ->setCellValue('A2', 'ID')
                                ->setCellValue('B2', 'MARCA')
                                ->setCellValue('C2', 'PRESENTACIÓN')
                                ->setCellValue('D2', 'UNIDAD DE MEDIDA')
                                ->setCellValue('E2', 'PRECIO COMPRA')
                                ->setCellValue('F2', 'PRECIO VENTA')
                                ->setCellValue('G2', 'CODIGO BARRAS')
                                ->setCellValue('H2', 'EXISTENCIA')
                                ->setCellValue('I2', 'PROVEEDOR')
                                ->setCellValue('J2', 'ESTATUS');
            
            //Estilos para los encabezados
            $estilo_titulo_doc = array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array('rgb' => 'A5A5A5')
                                        )
                                    );
            
            $estilo_columnas_doc = array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array('rgb' => '127CBB')
                                        ),
                                        'font' => array(
                                            'color' => array('rgb' => 'FFFFFF')
                                        )
                                    );
            
            $genera_excel->setActiveSheetIndex(0)->getStyle('A1')->applyFromArray($estilo_titulo_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('A2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('B2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('C2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('D2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('E2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('F2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('G2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('H2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('I2')->applyFromArray($estilo_columnas_doc);
            $genera_excel->setActiveSheetIndex(0)->getStyle('J2')->applyFromArray($estilo_columnas_doc);
            
            
            //Definir ancho de columnas
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth('5');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth('30');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth('15');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth('20');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth('15');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth('15');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('G')->setWidth('20');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('H')->setWidth('15');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('I')->setWidth('30');
            $genera_excel->setActiveSheetIndex(0)->getColumnDimension('J')->setWidth('15');
            
            $cont = 2;
            
            foreach($respuesta['contenido'] as $producto)
            {
                $cont ++;
                
                $codigo_barras = $producto['codigo_barras'];
                
                $genera_excel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$cont, $producto['id_producto'])
                                ->setCellValue('B'.$cont, $producto['marca'])
                                ->setCellValue('C'.$cont, $producto['pres_medida'] . $producto['pres_unidad'])
                                ->setCellValue('D'.$cont, $producto['unidad_medida'])
                                ->setCellValue('E'.$cont, $producto['precio_compra'])
                                ->setCellValue('F'.$cont, $producto['precio_venta'])
                                ->setCellValue('G'.$cont, "$codigo_barras")
                                ->setCellValue('H'.$cont, $producto['existencia'])
                                ->setCellValue('I'.$cont, $producto['nombre_proveedor'])
                                ->setCellValue('J'.$cont, $producto['estatus']);
            }
             
            $genera_excel->getActiveSheet()->setTitle('Productos');
            $genera_excel->setActiveSheetIndex(0);
            
            $genera_excel = PHPExcel_IOFactory::createWriter($genera_excel, 'Excel2007');
            
            $genera_excel->save('ExcelIsGaytanProductos.xslx');
            
        } else 
        {
            throw new Exception($respuesta['mensaje']);
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function eliminar_producto($id_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE productos SET estatus = 'BA' WHERE id_producto = ?");
        $stmt->bind_param('s', $id_producto);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al dar de baja el producto.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function reactivar_producto($id_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $stmt = $conect->prepare("UPDATE productos SET estatus = 'AC' WHERE id_producto = ?");
        $stmt->bind_param('s', $id_producto);

        /* ejecuta sentencias prepradas */
        $stmt->execute();
        
        $conect->commit();
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => 'Error al reactivar el producto.',
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function obtener_datos_producto($id_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        $query = "SELECT * FROM productos WHERE id_producto = $id_producto";
        
        $filas = $conect->query($query);
        
        if(!empty($filas) && $filas->num_rows > 0)
        {
            $datos_cliente = $filas->fetch_all(MYSQLI_ASSOC);
            $resultado['contenido'] = $datos_cliente[0];
        } 
        else 
        {
            throw new Exception("Error al obtener los datos del producto.");
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        error_log("ERROR: " . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function armar_tabla_resultado($datos)
{
    $html = '';
    
    foreach($datos as $valor)
    {
        $id_producto       = $valor['id_producto'];
        $marca   = $valor['marca'];
        $presentacion   = $valor['pres_medida'] . $valor['pres_unidad'];
        $unidad_medida = $valor['unidad_medida'];
        $precio_compra = $valor['precio_compra'];
        $precio_venta = $valor['precio_venta'];
        $codigo_barras = $valor['codigo_barras'];
        $existencia = $valor['existencia'];
        $nombre_proveedor = $valor['nombre_proveedor'];
        $estatus = $valor['estatus'];
        $opciones = 'OPCIONES';
        switch($estatus)
        {
            case 'AC':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_producto' onclick='eliminar_producto(this.id)' class='red tooltip-error' data-rel='tooltip' data-placement='top' data-original-title='Eliminar'>
                                        <i class='ace-icon fa fa-trash bigger-130'></i>
                                    </a>
                                    <a id = '$id_producto' onclick='modificar_producto(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                </div>";
                break;
            case 'BA':
                $opciones = "   <div class='hiden-sm hiden-xs action-buttons'>
                                    <a id = '$id_producto' onclick='modificar_producto(this.id)' class='orange tooltip-warning' data-rel='tooltip' data-placement='top' data-original-title='Editar'>
                                        <i class='ace-icon fa fa-pencil bigger-130'></i>
                                    </a>
                                    <a id = '$id_producto' onclick='reactivar_producto(this.id)' class='green tooltip-success' data-rel='tooltip' data-placement='top' data-original-title='Reactivar'>
                                        <i class='ace-icon fa fa-history bigger-130'></i>
                                    </a>
                                </div>";
                break;
        }
        
        $html .=    "<tr>
                        <td align='center'>$id_producto</td>
                        <td>$marca</td>
                        <td align='center'>$presentacion</td>
                        <td align='center'>$unidad_medida</td>
                        <td align='right'>$precio_compra</td>
                        <td align='right'>$precio_venta</td>
                        <td align='center'>$codigo_barras</td>
                        <td align='center'>$existencia</td>
                        <td>$nombre_proveedor</td>
                        <td align='center'>$estatus</td>
                        <td align='center'>$opciones</td>
                    </tr>";
    }
    
    return $html;
}

function validar_campos($datos_producto)
{
    $mensaje = '';
    
    if(trim($datos_producto['marca']) == '')
        $mensaje .= ", Marca";
    if(trim($datos_producto['pres_medida']) == '')
        $mensaje .= ", Presentación";
    if(trim($datos_producto['unidad_medida']) == '0')
        $mensaje .= ", Unidad";
    if(trim($datos_producto['precio_compra']) == '0.00')
        $mensaje .= ", Precio de compra";
    if(trim($datos_producto['precio_venta']) == '0.00')
        $mensaje .= ", Precio de venta";
    if(trim($datos_producto['texto_ticket']) == '')
        $mensaje .= ", Texto para ticket";
    if(trim($datos_producto['categoria']) == '0')
        $mensaje .= ", Categoría";
    if(trim($datos_producto['id_proveedor']) == '0')
        $mensaje .= ", Proveedor";
    
    if($mensaje != '')
    {
        $mensaje = substr($mensaje, 2);
    }
        
    return $mensaje;
}

function get_productos($datos_producto)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();

        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );

        $agrego_where = false;
        $codigo_barras = $datos_producto['codigo_barras'];
        $marca = $datos_producto['marca'];
        $pres_medida = $datos_producto['pres_medida'];
        $pres_unidad = $datos_producto['pres_unidad'];
        $unidad = $datos_producto['unidad'];
        $categoria = $datos_producto['categoria'];
        $id_proveedor = $datos_producto['id_proveedor'];
        $estatus = $datos_producto['estatus'];

        $query = "  SELECT prod.*, prov.nombre_proveedor
                    FROM productos AS prod 
                    LEFT JOIN proveedores AS prov 
                        ON prov.id_proveedor = prod.id_proveedor";

        if($codigo_barras != '')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.codigo_barras = '$codigo_barras'";
            }
            else
            {
                $query .= " WHERE prod.codigo_barras = '$codigo_barras'";
            }
            $agrego_where = true;
        }

        if($marca != '')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.marca = '$marca'";
            }
            else
            {
                $query .= " WHERE prod.marca = '$marca'";
            }
            $agrego_where = true;
        }

        if($pres_medida != '')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.pres_medida = '$pres_medida'";
            }
            else
            {
                $query .= " WHERE prod.pres_medida = '$pres_medida'";
            }
            $agrego_where = true;
        }

        if($pres_unidad != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.pres_medida = '$pres_unidad'";
            }
            else
            {
                $query .= " WHERE prod.pres_medida = '$pres_unidad'";
            }
            $agrego_where = true;
        }

        if($unidad != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.unidad_medida = '$unidad'";
            }
            else
            {
                $query .= " WHERE prod.unidad_medida = '$unidad'";
            }
            $agrego_where = true;
        }

        if($categoria != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.categoria = $categoria";
            }
            else
            {
                $query .= " WHERE prod.categoria = $categoria";
            }
            $agrego_where = true;
        }

        if($id_proveedor != '0')
        {   
            if($agrego_where)
            {
                $query .= " AND prod.id_proveedor = $id_proveedor";
            }
            else
            {
                $query .= " WHERE prod.id_proveedor = $id_proveedor";
            }
            $agrego_where = true;
        }

        if($estatus != '0')
        {
            if($agrego_where)
            {
                $query .= " AND prod.estatus = '$estatus'";
            }
            else
            {
                $query .= " WHERE prod.estatus = '$estatus'";
            }
        }

        $query .= " ORDER BY prod.marca ASC";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $resultado['contenido'] = $filas->fetch_all(MYSQLI_ASSOC);   
        }
        else 
        {
            throw new Exception("No se encontraron datos");
        }

        return $resultado;
        
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        
        return $resultado;
    }
}
