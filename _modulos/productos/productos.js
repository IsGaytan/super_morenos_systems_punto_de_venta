//Carga los combos de proveedores para el registro y búsqueda de productos
$.post('../_general/general.php',
{
    estatus: 'AC',
    function: 'obtiene_combo_proveedores'
}, function(data){
   var datos = typeof data === 'object' ? data : JSON.parse(data);
   
   if(datos.tipo === 1)
   {
        $('#combo_proveedores').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
        $('#combo_proveedores_buscar').html('<option value="0">TODOS</option>'+datos.contenido);
        $('.chosen-select').chosen();
        $('.chosen-container-single').css('width', '100%');
   }
   else
   {
        $('#combo_proveedores').html(datos.mensaje);
        $('#combo_proveedores_buscar').html(datos.contenido);
        $('.chosen-select').chosen();
        $('#combo_proveedores_chosen').css('width', '330px');
   }
});

function dos_decimales(input){
    var monto = parseFloat($(input).val());
    $(input).val(monto.toFixed(2));
}

function sin_decimales(input){
	var monto = parseFloat($(input).val());
	$(input).val(monto.toFixed(0));
    }

function registrar_producto()
{
    $('#registro_producto_error').hide();
    var marca = $('#marca_producto').val();
    var pres = $('#presentacion').val();
    var pres_medida = $('#presentacion').val();
    var pres_unidad = $('#presentacion_unidad').val();
    var uni = $('#unidad_medida').val();
    var pre_com = $('#precio_compra').val();
    var porcentaje = $('#porcentaje').val();
    var pre_ven = $('#precio_venta').val();
    var cod_bar = $('#codigo_barras').val();
    var text_tic = $('#texto_ticket').val();
    var exis = $('#existencia').val();
    var cat = $('#categoria').val();
    var id_prov = $('#combo_proveedores').val();
    var venta_libre = $('#check_venta_libre').prop('checked') ? 1 : 0;
    
    $.post('productos/productos.php',
    {
        marca: marca,
        pres_medida: pres_medida,
        pres_unidad: pres_unidad,
        unidad_medida: uni,
        precio_compra: pre_com,
        porcentaje : porcentaje,
        precio_venta: pre_ven,
        codigo_barras: cod_bar,
        texto_ticket: text_tic,
        existencia: exis,
        categoria: cat,
        id_proveedor: id_prov,
        venta_libre: venta_libre,
        function: 'registrar_productos'
    }, function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);

        if(datos.tipo != 1)
        {
            $.gritter.add({
                title: 'AVISO',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'El producto se ha registrado correctamente.',
                class_name: 'gritter-info'
            });
        }
    });
    
}

function busca_productos()
{
    var cod_bar = $('#codigo_barras_buscar').val();
    var mar_pro = $('#marca_producto_buscar').val();
    var pres_medida = $('#presentacion_buscar').val();
    var pres_unidad = $('#presentacion_unidad_bus').val();
    var unidad = $('#unidad_medida_buscar').val();
    var cat = $('#categoria_buscar').val();
    var prov = $('#combo_proveedores_buscar').val();
    var estatus = $('#estatus_busqueda').val();
    
    $.post('productos/productos.php',{
        codigo_barras: cod_bar,
        marca: mar_pro,
        pres_medida: pres_medida,
        pres_unidad: pres_unidad,
        unidad: unidad,
        categoria: cat,
        id_proveedor: prov,
        estatus: estatus,
        function: 'busca_productos'
    }, function(data){
        var datos = typeof data === 'object' ? data: JSON.parse(data);
        
        if(datos.tipo != 1)
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#section_tabla_resultados').hide(); 
            $('#pdf_productos').hide();
            $('#excel_productos').hide();
            $.gritter.add({
                title: '¡AVISO!',
                text: 'No se encontraron  datos.',
                class_name: 'gritter-warning'
            });
        }
        else
        {
            $('#tabla_resultados').dataTable().fnDestroy(); //Destruye el dataTable para poder inicializarlo nuevamente
            $('#cuerpo_tabla').html(datos.contenido);
            $('#tabla_resultados').dataTable({ //Inicializa el dataTable en la tabla #tabla_resultado y le asigna el lenguaje español mediante un archivo json que agregué
                "language": {
                    "url":     "../assets/js/Spanish.json"
                },
                "bJQueryUI": true,
                "bAutoWidth": false,
                "aoColumns" : [
                    { 'sWidth': '5%' },
                    { 'sWidth': '14%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '9%' },
                    { 'sWidth': '12%' },
                    { 'sWidth': '6%' },
                    { 'sWidth': '9%' }
                ] 
            });
            $('#section_tabla_resultados').show();
            $('#pdf_productos').show();
            $('#excel_productos').show();
            $('.tooltip-success').tooltip();
            $('.tooltip-default').tooltip();
            $('.tooltip-info').tooltip();
            $('.tooltip-warning').tooltip();
            $('.tooltip-error').tooltip();
        }
    });
}

function eliminar_producto(id_producto)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea dar de baja este producto?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-times'></i>Eliminar",
                "className" : "btn-sm btn-danger",
                "callback": function() {
                    $.post('productos/productos.php',
                    {
                        id_producto: id_producto,
                        function : 'eliminar_producto'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El producto se ha dado de baja correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_productos').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function reactivar_producto(id_producto)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea reactivar este producto?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-check'></i>Reactivar",
                "className" : "btn-sm btn-success",
                "callback": function() {
                    $.post('productos/productos.php',
                    {
                        id_producto: id_producto,
                        function : 'reactivar_producto'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: 'El producto se ha reactivado correctamente.',
                                class_name: 'gritter-success'
                            });
                            $('#buscar_productos').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }

                    });
                }
           }
        }
    });
}

function modificar_producto(id_producto)
{
    bootbox.dialog({
        message: "<h4 class='blue'>¿Está seguro que desea modificar los datos de este producto?</h4>",
        buttons:
        {
            "success" :
            {
               "label" : "Cacelar",
               "className" : "btn-sm btn-default",
               "callback": function() {

               }
            },
            "danger" :
            {
                "label" : "<i class='ace-icon fa fa-pencil'></i>Modificar",
                "className" : "btn-sm btn-warning",
                "callback": function() {
                    $.post('productos/productos.php',
                    {
                        id_producto: id_producto,
                        function : 'obtener_datos_producto'
                    }, function(data){
                        var datos = typeof data === 'object' ? data : JSON.parse(data);

                        if(datos.tipo === 1)
                        {
                            //Llenar el formulario con los datos del cliente
                            $('#marca_producto').val(datos.contenido.marca);
                            $('#presentacion').val(datos.contenido.pres_medida);
                            $('#presentacion_unidad').val(datos.contenido.pres_unidad);
                            $('#unidad_medida').val(datos.contenido.unidad_medida);
                            $('#precio_compra').val(datos.contenido.precio_compra);
                            $('#porcentaje').val(datos.contenido.porcentaje);
                            $('#precio_venta').val(datos.contenido.precio_venta);
                            $('#codigo_barras').val(datos.contenido.codigo_barras);
                            $('#texto_ticket').val(datos.contenido.texto_ticket);
                            $('#existencia').val(datos.contenido.existencia);
                            $('#categoria').val(datos.contenido.categoria);
                            $('#combo_proveedores').val(datos.contenido.id_proveedor).trigger("chosen:updated");
                            
                            //Mostrar el botón de modificar y ocultar el de registrar
                            $('#modificar_producto').attr('id_producto', datos.contenido.id_producto);
                            $('#modificar_producto').show();
                            $('#registrar_producto').hide(); 
                           
                            //Entrar al módulo de registrar para modificar los datos del cliente
                            $('#pestana_registrar_productos').click();
                        }
                        else
                        {
                            $.gritter.add({
                                title: 'Mensaje',
                                text: datos.mensaje,
                                class_name: 'gritter-error'
                            });
                        }
                    });
                }
           }
        }
    });
}

function modificar_datos_producto(id_producto)
{
    $.post('productos/productos.php',
    {
        id_producto     : id_producto,
        marca           : $('#marca_producto').val(),
        pres_medida     : $('#presentacion').val(),
        pres_unidad     : $('#presentacion_unidad').val(),
        unidad_medida   : $('#unidad_medida').val(),
        precio_compra   : $('#precio_compra').val(),
        porcentaje      : $('#porcentaje').val(),
        precio_venta    : $('#precio_venta').val(),
        codigo_barras   : $('#codigo_barras').val(),
        texto_ticket    : $('#texto_ticket').val(),
        existencia      : $('#existencia').val(),
        categoria       : $('#categoria').val(),
        id_proveedor    : $('#combo_proveedores').val(),
        function        : 'modificar_producto'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo != 1)
        {
            $.gritter.add({
                title: 'AVISO',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'El producto se ha modificado correctamente.',
                class_name: 'gritter-info'
            });
            $('#productos').click();
        }
    });
}

function genera_excel_productos()
{    
    $.post('productos/productos.php',
    {
        codigo_barras   : $('#codigo_barras_buscar').val(),
        marca           : $('#marca_producto_buscar').val(),
        pres_medida     : $('#presentacion_buscar').val(),
        pres_unidad     : $('#presentacion_unidad_bus').val(),
        unidad          : $('#unidad_medida_buscar').val(),
        categoria       : $('#categoria_buscar').val(),
        id_proveedor    : $('#combo_proveedores_buscar').val(),
        estatus         : $('#estatus_busqueda').val(),
        function        : 'generar_excel'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data: JSON.parse(data);
    });
}

function calcular_precio_venta()
{
    var porcentaje = $('#porcentaje').val();
    var precio_com = $('#precio_compra').val();
    var precio_ven = 0;
    
    if(!isNaN(porcentaje) && porcentaje != '' && !isNaN(precio_com) && precio_com != '')
    {
        var ganancia = ((porcentaje/100)*precio_com);
        
        precio_ven = (parseFloat(ganancia) + parseFloat(precio_com));
        
        $('#precio_venta').val(precio_ven.toFixed(2));
    }
    else
    {
        $.gritter.add({
            title: 'AVISO',
            text: 'El precio de venta y el porcentaje deben contener valores numéricos.',
            class_name: 'gritter-warning'
        });
    }
}
