<?php
/**
 * Description of carrito_comprar
 *
 * @author IsGaytán
 */
session_start();

class carrito_vender
{
    private $carrito_vender = array();
    
    public function __construct()
    {
        if(!isset($_SESSION["carrito_vender"]))
        {
            $_SESSION["carrito_vender"] = null;
            $this->carrito_vender["precio_total"] = 0;
            $this->carrito_vender["articulos_total"] = 0;
        }
        $this->carrito_vender = $_SESSION['carrito_vender'];
    }
    
    public function add_carrito_vender($articulo = array())
    {        
        if(!is_array($articulo) || empty($articulo))
        {
            throw new Exception("Error, el articulo no es un array!", 1);	
        }

        if(!isset($articulo["id_producto"]) || !isset($articulo["nombre"]) || !isset($articulo["cantidad"]) || !isset($articulo["precio"]))
        {
                throw new Exception("Error, el articulo debe tener un id, nombre, cantidad y precio.", 1);	
        }

        if(!is_numeric($articulo["id_producto"]) || !is_numeric($articulo["cantidad"]) || !is_numeric($articulo["precio"]))
        {
                throw new Exception("Error, el id, cantidad y precio deben ser números!", 1);	
        }

        $hora_sistema = getdate(); //Obtengo la hora y fecha del sistema
        
        $id_fila = $hora_sistema['0']; //En esta posición están los segundos desde la época UNIX. Será el id
    
        $_SESSION["carrito_vender"]['articulos'][$id_fila] = $articulo;
        
        $_SESSION["carrito_vender"]['articulos'][$id_fila]['id_articulo'] = $id_fila;
        
        carrito_vender::update_carrito_vender();
    }
    
    public function remove_carrito_vender($id_articulo)
    {
        if(isset($_SESSION["carrito_vender"]["articulos"][$id_articulo]))
        {
            unset($_SESSION["carrito_vender"]["articulos"][$id_articulo]);
            
            carrito_vender::update_carrito_vender();
        }
    }
    
    public function aumentar_cantidad_vender($id_articulo)
    {
        if(isset($_SESSION['carrito_vender']['articulos'][$id_articulo]))
        {
            $cantidad = $_SESSION['carrito_vender']['articulos'][$id_articulo]['cantidad'];
            
            $nueva_cantidad = $cantidad + 1;
            
            $_SESSION['carrito_vender']['articulos'][$id_articulo]['cantidad'] = $nueva_cantidad;
            
            carrito_vender::update_carrito_vender();
        }
    }
    
    public function disminuir_cantidad_vender($id_articulo)
    {
        if(isset($_SESSION['carrito_vender']['articulos'][$id_articulo]))
        {
            $cantidad = $_SESSION['carrito_vender']['articulos'][$id_articulo]['cantidad'];
            
            $nueva_cantidad = $cantidad - 1;
            
            if($nueva_cantidad >= 0)
            {
                $_SESSION['carrito_vender']['articulos'][$id_articulo]['cantidad'] = $nueva_cantidad;
                
                carrito_vender::update_carrito_vender();
            }
        }
    }
    
    public function actualizar_cantidad_vender($id_articulo, $cantidad)
    {
        if(isset($_SESSION['carrito_vender']['articulos'][$id_articulo]))
        {   
            $_SESSION['carrito_vender']['articulos'][$id_articulo]['cantidad'] = $cantidad;
            
            carrito_vender::update_carrito_vender();
        }
    }
    
    public function update_carrito_vender()
    {
        $precio_carrito = 0;
        $cantidad_articulos = 0;
        
        if(count($_SESSION['carrito_vender']['articulos']) > 0)
        {
            //Actualizar la cantidad de artículos en el carrito
            $_SESSION['carrito_vender']['cantidad_articulos'] = count($_SESSION['carrito_vender']['articulos']);
            
            //Obtener los precios unitarios y las cantidades de cada artículo para sacar el total del carrito
            foreach ($_SESSION['carrito_vender']['articulos'] as $articulo)
            {
                $precio_carrito += $articulo['precio'] * $articulo['cantidad'];
                $cantidad_articulos += $articulo['cantidad'];
            }
            
            $_SESSION['carrito_vender']['precio_carrito'] = number_format($precio_carrito, 2);
            $_SESSION['carrito_vender']['cantidad_articulos_total'] = number_format($cantidad_articulos, 2);
        }
        
    }
    
    public function check_carrito_vender_content()
    {
        if(isset($_SESSION['carrito_vender']['articulos']) && !empty($_SESSION['carrito_vender']['articulos']))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function destruir_carrito()
    {
        if(isset($_SESSION['carrito_vender']))
        {
            unset($_SESSION['carrito_vender']);
            return true;
        }
        else
        {
            return false;
        }
    }
}
