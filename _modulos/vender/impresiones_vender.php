<?php
require_once '../../_librerias/escpos/Escpos.php';
require_once '../../_general/general_functions.php';
require_once '../../_general/item_printer.php';

class Impresiones_vender
{
    private $total;
    private $cantidad_recibida;
    private $cambio;
    private $titulo_venta;
    private $nombre_cajero;
    private $articulos;
    private $id_transaccion;
    
    function __construct($id_transaccion, $total_venta, $recibi, $cambio, $titulo_venta, $nombre_usuario, $articulos)
    {
        $this->total = $total_venta;
        $this->cantidad_recibida = $recibi;
        $this->cambio = $cambio;
        $this->titulo_venta = $titulo_venta;
        $this->nombre_cajero = $nombre_usuario;
        $this->articulos = $articulos;
        $this->id_transaccion = $id_transaccion;
    }
    public function imprimir_ticket_venta()
    {
        $fecha = date("Y-m-d H:i:s");
        $impresora = general_functions::nombre_impresora;
        $ruta_imagen = general_functions::ruta_imagen;
        
        $connector = new WindowsPrintConnector($impresora);

        //Instanciar la clase Escpos
        $printer = new Escpos($connector);

        //Crear la imagen para el ticket
        $tux = new EscposImage($ruta_imagen);
        
        //Alinear imagen al centro
        $printer -> setJustification(Escpos::JUSTIFY_CENTER);
        
        //Imprimir la imagen
       $printer -> graphics($tux);
	
        //Espacio en papel
        $printer -> feed();
        
        //Alineación por default
        $printer -> selectPrintMode();
        
        //Tamaño para el título del ticket
        $printer -> setTextSize(2,1);
        $printer -> text($this->titulo_venta."\n");
        
        //Espacio al papael
        $printer -> feed();
        
        //Tamaño de letra para el texto del ticket
        $printer -> setTextSize(1,1);
        
        //Agregar el folio del importe registrado al ticket
        $printer -> text(str_pad($this->id_transaccion,6,"0",STR_PAD_LEFT)."\n"); ///FOLIO DE LA VENTA
        $printer -> text($this->nombre_cajero."\n");
        $printer -> text($fecha."\n");
        
        $printer -> feed();
        
        $printer -> selectPrintMode();
        
        //Agregar los artículos
        foreach($this->articulos AS $articulo)
        {
            $item = new Item_printer($articulo['cantidad'] . " - " . $articulo['texto_ticket'], "$".number_format(($articulo['precio']*$articulo['cantidad']),2));
            $printer -> text($item);
        }
        
        $printer -> selectPrintMode();
        
        $printer -> feed();
        
        $printer ->setJustification(Escpos::JUSTIFY_CENTER);
        
        $item = new Item_printer("Total Venta", "$".number_format($this->total, 2));
        $printer -> text($item);
        $item = new Item_printer("Cantidad Recibida", "$".number_format($this->cantidad_recibida, 2));
        $printer -> text($item);
        $item = new Item_printer("Cambio", "$".number_format($this->cambio, 2));
        $printer -> text($item."\n");
        
        $printer -> feed();
        $printer -> feed();
        
        $printer -> text("Conserve su ticket para futuras aclaraciones\n");
        $printer -> text("¡Gracias por su compra!\n");
        
        $printer -> cut();

        $printer -> pulse($pin = 0, $on_ms = 120, $off_ms = 240);

        $printer -> close();
        
    }
}

class Impresiones_ventas_credito
{
    private $id_transaccion;
    private $cantidad_total;
    private $cantidad_cubierta;
    private $cantidad_credito;
    private $titulo_venta;
    private $nombre_cajero;
    private $nombre_cliente;
    private $saldo;
    private $articulos;
    
    function __construct($id_transaccion, $total_total, $cantidad_cubierta, $cantidad_credito, $titulo_venta, $nombre_cajero, $nombre_cliente, $saldo, $articulos)
    {
        $this->id_transaccion       = $id_transaccion;
        $this->cantidad_total       = $total_total;
        $this->cantidad_cubierta    = $cantidad_cubierta;
        $this->cantidad_credito     = $cantidad_credito;
        $this->titulo_venta         = $titulo_venta;
        $this->nombre_cajero        = $nombre_cajero;
        $this->nombre_cliente       = $nombre_cliente;
        $this->saldo                = $saldo;
        $this->articulos            = $articulos;
    }
    public function imprimir_ticket_venta()
    {   
        $fecha = date("Y-m-d H:i:s");
        $impresora = general_functions::nombre_impresora;
        $ruta_imagen = general_functions::ruta_imagen;
        
        $connector = new WindowsPrintConnector($impresora);

        //Instanciar la clase Escpos
        $printer = new Escpos($connector);

        //Crear la imagen para el ticket
        $tux = new EscposImage($ruta_imagen);
        
        //Alinear imagen al centro
        $printer -> setJustification(Escpos::JUSTIFY_CENTER);
        
        //Imprimir la imagen
	$printer -> graphics($tux);
	
        //Espacio en papel
        $printer -> feed();
        
        //Alineación por default
        $printer -> selectPrintMode();
        
        //Tamaño para el título del ticket
        $printer -> setTextSize(2,1);
        $printer -> text($this->titulo_venta."\n");
        
        //Espacio al papael
        $printer -> feed();
        
        //Tamaño de letra para el texto del ticket
        $printer -> setTextSize(1,1);
        
        //Agregar el folio del importe registrado al ticket
        $printer -> text(str_pad($this->id_transaccion,6,"0",STR_PAD_LEFT)."\n"); ///FOLIO DE LA VENTA
        $printer -> text($this->nombre_cajero."\n");
        $printer -> text($this->nombre_cliente."\n");
        $printer -> text($fecha."\n");
        
        $printer -> feed();
        
        $printer -> selectPrintMode();
        
        //Agregar los artículos
        foreach($this->articulos AS $articulo)
        {
            $item = new Item_printer($articulo['texto_ticket'], "$".number_format($articulo['precio'],2));
            $printer -> text($item);
        }
        
        $printer -> selectPrintMode();
        
        $printer -> feed();
        
        $printer ->setJustification(Escpos::JUSTIFY_CENTER);
        
        $item = new Item_printer("Total Venta", "$".number_format($this->cantidad_total, 2));
        $printer -> text($item);
        $item = new Item_printer("Cantidad Cubierta", "$".number_format($this->cantidad_cubierta, 2));
        $printer -> text($item);
        $item = new Item_printer("Cantidad Crédito", "$".number_format($this->cantidad_credito, 2));
        $printer -> text($item);
        $item = new Item_printer("Saldo de la cuenta", "$".number_format($this->saldo, 2));
        $printer -> text($item."\n");
        
        $printer -> feed();
        $printer -> feed();
        
        $printer -> text("Conserve su ticket para futuras aclaraciones\n");
        $printer -> text("¡Gracias por su compra!\n");
        
        $printer -> cut();

        $printer -> pulse($pin = 0, $on_ms = 120, $off_ms = 240);

        $printer -> close();
        
    }
}

class Impresiones_abonos
{
    private $id_abono;
    private $cantidad_abono;
    private $nombre_cliente;
    private $saldo;
    private $nombre_cajero;
    
    function __construct($id_abono, $cantidad_abono, $nombre_cliente, $saldo, $nombre_cajero)
    {
        $this->id_abono         = $id_abono;
        $this->cantidad_abono   = $cantidad_abono;
        $this->nombre_cliente   = $nombre_cliente;
        $this->saldo            = $saldo;
        $this->nombre_cajero    = $nombre_cajero;
    }
    public function imprimir_ticket_abono()
    {   
        $fecha = date("Y-m-d H:i:s");
        $impresora = general_functions::nombre_impresora;
        $ruta_imagen = general_functions::ruta_imagen;
        
        $connector = new WindowsPrintConnector($impresora);

        //Instanciar la clase Escpos
        $printer = new Escpos($connector);

        //Crear la imagen para el ticket
        $tux = new EscposImage($ruta_imagen);
        
        //Alinear imagen al centro
        $printer -> setJustification(Escpos::JUSTIFY_CENTER);
        
        //Imprimir la imagen
	$printer -> graphics($tux);
	
        //Espacio en papel
        $printer -> feed();
        
        //Alineación por default
        $printer -> selectPrintMode();
        
        //Tamaño para el título del ticket
        $printer -> setTextSize(2,1);
        $printer -> text("Comprobante de Abono\n");
        
        //Espacio al papael
        $printer -> feed();
        
        //Tamaño de letra para el texto del ticket
        $printer -> setTextSize(1,1);
        
        //Agregar el folio del importe registrado al ticket
        $printer -> text(str_pad($this->id_abono,6,"0",STR_PAD_LEFT)."\n"); ///FOLIO DE LA VENTA
        $printer -> text($this->nombre_cajero."\n");
        $printer -> text($this->nombre_cliente."\n");
        $printer -> text($fecha."\n");
        $printer -> feed();
        $printer -> text("Cantidad abono: $".  number_format($this->cantidad_abono, 2)."\n");
        $printer -> text("Saldo nuevo: $".  number_format($this->saldo, 2));
        
        $printer -> feed();
        $printer -> feed();
        
        $printer -> selectPrintMode();
        
        $printer ->setJustification(Escpos::JUSTIFY_CENTER);
        
        $printer -> text("¡Gracias por su abono!");
        
        $printer -> feed();
        $printer -> feed();
        
        $printer -> cut();

        $printer -> pulse($pin = 0, $on_ms = 120, $off_ms = 240);

        $printer -> close();
        
    }
}

class Impresiones_tarjeta
{
    private $id_venta;
    private $total_venta;
    private $tipo_venta;
    private $nombre_usuario;
    private $articulos_cobrar;
    private $referencia;
    
    function __construct($id_venta, $total_venta, $tipo_venta, $nombre_usuario, $articulos_cobrar, $referencia)
    {
        $this->id_venta         = $id_venta;
        $this->total_venta      = $total_venta;
        $this->tipo_venta       = $tipo_venta;
        $this->nombre_usuario   = $nombre_usuario;
        $this->articulos_cobrar = $articulos_cobrar;
        $this->referencia       = $referencia;
    }
    public function imprimir_ticket_venta_tarjeta()
    {   
        $fecha = date("Y-m-d H:i:s");
        $impresora = general_functions::nombre_impresora;
        $ruta_imagen = general_functions::ruta_imagen;
        
        $connector = new WindowsPrintConnector($impresora);

        //Instanciar la clase Escpos
        $printer = new Escpos($connector);

        //Crear la imagen para el ticket
        $tux = new EscposImage($ruta_imagen);
        
        //Alinear imagen al centro
        $printer -> setJustification(Escpos::JUSTIFY_CENTER);
        
        //Imprimir la imagen
	$printer -> graphics($tux);
	
        //Espacio en papel
        $printer -> feed();
        
        //Alineación por default
        $printer -> selectPrintMode();
        
        //Tamaño para el título del ticket
        $printer -> setTextSize(2,1);
        $printer -> text($this->tipo_venta."\n");
        
        //Espacio al papael
        $printer -> feed();
        
        //Tamaño de letra para el texto del ticket
        $printer -> setTextSize(1,1);
        
        //Agregar el folio del importe registrado al ticket
        $printer -> text(str_pad($this->id_transaccion,6,"0",STR_PAD_LEFT)."\n"); ///FOLIO DE LA VENTA
        $printer -> text($this->nombre_usuario."\n");
        $printer -> text($fecha."\n");
        
        $printer -> feed();
        
        $printer -> selectPrintMode();
        
        //Agregar los artículos
        foreach($this->articulos_cobrar AS $articulo)
        {
            $item = new Item_printer($articulo['texto_ticket'], "$".number_format($articulo['precio'],2));
            $printer -> text($item);
        }
        
        $printer -> selectPrintMode();
        
        $printer -> feed();
        
        $printer ->setJustification(Escpos::JUSTIFY_CENTER);
        
        $item = new Item_printer("Total Venta", "$".number_format($this->total_venta, 2));
        $printer -> text($item);
        
        $printer -> feed();
        
        $printer -> text("Conserve su ticket para futuras aclaraciones\n");
        $printer -> text("¡Gracias por su compra!\n");
        
        $printer -> cut();

        $printer -> close();
        
    }
}