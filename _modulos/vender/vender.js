//Cargar el combo de productos
//al entrar al módulo
$.post('../_general/general.php',
{
    function: 'obtiene_combo_productos'
},
function(data){
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    
    if(datos.tipo === 1)
    {
         $('#productos_vender').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select').chosen();
    }
    else
    {
         $('#productos_vender').html('<option value="0">*SELECCIONE UNO...</option>'+datos.contenido);
         $('.chosen-select').chosen();
    }
});

//Cargar el contenido del carrito al entrar al módulo
$.post('vender/vender.php',
{
    function: 'obtener_lista_vender'
},
function(data){
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    
    $('#body_tabla_compra').html(datos.html);
    $('#total_a_cobrar').val(datos.costo);
    $('.tooltip-error').tooltip();
});

//Cargar el combo de clientes para el cobro a crédito
$.post('clientes/clientes.php',
{
    function : 'obtener_lista_clientes'
}, function(data)
{
    var datos = typeof data === 'object' ? data : JSON.parse(data);
    debugger;
    $('#clientes_modal_cobrar_parcial').html(datos.contenido);
});

//Llamar función que permite sólo registrar dos decimales
$('#cantidad_cobrar_pagar').numeric();
$('#cantidad_credito_cobrar').numeric();
$('#cantidad_importe').numeric();
$('#limite_credito_nuevo').numeric();
$('#telefono_recarga').mask('(999)-999-9999');

//Agregar un producto al carrito mediante código de barras
$('#codebar_vender').change(function()
{
    
    $.post('vender/vender.php',
    {
        codigo_barras : $('#codebar_vender').val(),
        function: 'agregar_articulo_carrito_comprar'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        $('#body_tabla_compra').html(datos.contenido.html);
        $('#total_a_cobrar').val(datos.contenido.costo);
        $('.tooltip-error').tooltip();
        $('#codebar_vender').val('');
    });
});

//Agregar un producto al carrito mediante nombre del producto
function agregar_producto_chose()
{
    $.post('vender/vender.php',
    {
        id_producto : $('#productos_vender').val(),
        function: 'agregar_articulo_carrito_comprar'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        $('#body_tabla_compra').html(datos.contenido.html);
        $('#total_a_cobrar').val(datos.contenido.costo);
        $('.tooltip-error').tooltip();
    });
}

//Responde al botón "Eliminar" de la lista de productos en la venta
function eliminar_articulo_venta(id_articulo)
{
    $.post('vender/vender.php', 
    {
        id_articulo : id_articulo,
        function : 'eliminar_articulo_venta'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        $('#body_tabla_compra').html(datos.html);
        $('#total_a_cobrar').val(datos.costo);
        $('.tooltip-error').tooltip();
    });
}

function modificar_cantidad(action, id_articulo)
{
    $.post('vender/vender.php',
    {
        action : action,
        id_articulo : id_articulo,
        function : 'modificar_cantidad_vender'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        $('#body_tabla_compra').html(datos.html);
        $('#total_a_cobrar').val(datos.costo);
    });
}

function lanzar_modal_cobrar()
{
    $.post('vender/vender.php',
    {
        function : 'validar_contenido_carrito'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            var total_cobrar = $('#total_a_cobrar').val();
    
            $('#mensaje_cobrar').html("¿Realizar cobro por un total de $<strong>"+total_cobrar+"</strong>?");
            $('#input_total_aux').val(total_cobrar);
            $('#modal_cobrar').modal('show');
        }
    });   
}

function registrar_recarga()
{
    $.post('vender/vender.php',
    {
        telefono : $('#telefono_recarga').val(),
        monto : $('#monto_recarga').val(),
        compania : $('#compania_telefonica').val(),
        folio : $('#folio_recarga').val(),
        transaccion : $('#transaccion_recarga').val(),
        numero_autorizacion : $('#numero_autorizacion_recarga').val(),
        function : 'registrar_recarga'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡Mensaje!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: datos.mensaje,
                class_name: 'gritter-success'
            });
            
            //Cerrar el modal
            $('#modal_recargas').modal('hide');
            
            //Limpiar los campos
            $('#telefono_recarga').val('');
            $('#folio_recarga').val('');
            $('#transaccion_recarga').val('');
            $('#numero_autorizacion_recarga').val('');
        }
    });
}

function realizar_cobro()
{
    var tipo_descuento = 'no';
    
    var descuento = 0;
    //Validar los check de descuento para enviar el descuento por Ajax
    if($('#porcentual').prop('checked'))
    {   
        tipo_descuento = 'porcentual';
        
        descuento = $('#input_descuento_porcentual').val();
    }
    else if($('#pesos').prop('checked'))
    {
        descuento = $('#input_descuento_pesos').val();
        
        tipo_descuento = 'pesos';
    }
    
    $.post('vender/vender.php',
    {
        recibi          : $('#input_recibi').val(),
        descuento       : descuento,
        tipo_descuento  : tipo_descuento,
        //cambio      : $('#input_cambio').val(),
        function : 'venta_efectivo'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                //$('#modal_cobrar').modal('hide');
                //$('#vender').click();
                $('#modal_cobrar').modal('hide');
                $('#div_modo_cobro').hide();
                $('#div_resumen_venta').show().html(datos.contenido);

                setTimeout(function(){
                    
                    $('#vender').click();
                }, 1000);
                
                $.gritter.add({
                    title: '¡CORRECTO!',
                    text: datos.mensaje,
                    class_name: 'gritter-success'
                });
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-warning'
                });
                break;
            case 3:
                $.gritter.add({
                    title: '¡ERROR!',
                    text: datos.mensaje,
                    class_name: 'gritter-danger'
                });
                break;
        }
    });
}

function lanzar_modal_cobrar_tarjeta()
{
    $.post('vender/vender.php',
    {
        function : 'validar_contenido_carrito'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            var total_cobrar = $('#total_a_cobrar').val();
    
            $('#mensaje_cobrar_tarjeta').html("¿Realizar cobro por tarjeta por un total de $<strong>"+total_cobrar+"</strong>?");
            $('#modal_cobrar_tarjeta').modal('show');
        }
    });   
}

function realizar_cobro_tarjeta()
{
    $.post('vender/vender.php',
    {
        terminal_venta : $('#terminal_venta').val(),
        transaccion_venta : $('#transaccion_venta').val(),
        lote_venta : $('#lote_venta').val(),
        factura_venta : $('#factura_venta').val(),
        referencia_venta : $('#referencia_venta').val(),
        function : 'realizar_cobro_tarjeta'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ERROR!',
                text: datos.mensaje,
                class_name: 'gritter-danger'
            });
        }
        else
        {
            $('#modal_cobrar_tarjeta').modal('hide');
            setTimeout(function(){
                $('#vender').click();
                $.gritter.add({
                    title: '¡CORRECTO!',
                    text: datos.mensaje,
                    class_name: 'gritter-success'
                });
            }, 1000);
        }
    });
}

function realizar_cobro_tarjeta_vales()
{
    $.post('vender/vender.php',
    {
        tarjetahabiente : $('#tarjetahabiente').val(),
        num_tarjeta     : $('#num_tarjeta').val(),
        function        : 'realizar_cobro_tarjeta_vales'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                $('#modal_cobrar_tarjeta').modal('hide');
                setTimeout(function(){
                    $('#vender').click();
                    $.gritter.add({
                        title: '¡CORRECTO!',
                        text: datos.mensaje,
                        class_name: 'gritter-success'
                    });
                }, 1000);
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-warning'
                });
                break;
            case 3:
                $.gritter.add({
                    title: '¡ERROR!',
                    text: datos.mensaje,
                    class_name: 'gritter-danger'
                });
                break;
        }
    });
}

function lanzar_modal_cobrar_parcial()
{
    $.post('vender/vender.php',
    {
        function : 'validar_contenido_carrito'
    },function(data){
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: 'Mensaje',
                text: 'No tiene artículos en el carrito para realizar un cobro a crédito.',
                class_name: 'gritter-warning'
            });
        }
        else
        {
            var total_pagar = $('#total_a_cobrar').val();

            $('#cantidad_a_cobrar').html(total_pagar);
            $('#cantidad_credito_cobrar').val(total_pagar);
            $('#modal_cobrar_parcial').modal('show');
        }
    });
}

$('#cantidad_cobrar_pagar').blur(function ()
{
    var cantidad_total_cobrar = $('#cantidad_a_cobrar').text();
    var cantidad_credito = $('#cantidad_credito_cobrar').val();
    var cantidad_cobrar = $('#cantidad_cobrar_pagar').val();
    if(cantidad_cobrar != '')
    {
        $('#registrar_venta_parcial_btn').prop('disabled', false);
        cantidad_cobrar = parseFloat(cantidad_cobrar);
        cantidad_credito = parseFloat(cantidad_credito);
        
        if(cantidad_total_cobrar < cantidad_cobrar)
        {
            $('#registrar_venta_parcial_btn').prop('disabled', true);
            $.gritter.add({
                title: 'Mensaje',
                text: 'La cantidad a cobrar no puede ser mayor que la cantidad a crédito.',
                class_name: 'gritter-error'
            });
        }
        else
        {
            cantidad_total_cobrar = parseFloat(cantidad_total_cobrar);
            $('#cantidad_credito_cobrar').val(cantidad_total_cobrar-cantidad_cobrar);
        }
    }
});

//Registrar la venta a crédito
function realizar_cobro_parcial()
{
    $.post('vender/vender.php',
    {
        cantidad_credito : $('#cantidad_credito_cobrar').val(),
        id_cliente : $('#clientes_modal_cobrar_parcial').val(),
        function : 'validar_credito'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);

        //2 - Límite de crédito excedido, autorizar aumento
        //3 - Error fatal
        //1 - Límite de crédito validado y registro de venta
        if(datos.tipo === 2)
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
            //Ocultar el modal de la venta
            $('#modal_cobrar_parcial').modal('hide');
            
            //Llenar los datos del formulario para autorizar el aumento de límite de crédito
            $('#cliente_aumentar_credito').val(datos.contenido.nombre_cliente + ' ' + datos.contenido.apellido_pat);
            $('#limite_credito_actual').val(datos.contenido.limite_credito);
            $('#id_cliente_aumentar_credito').val(datos.contenido.id_cliente);
            
            //Mostrar el modal de aumento de límite de crédito
            $('#modal_aumentar_credito').modal('show');
        }
        else if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ERROR!',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
        }
        else if(datos.tipo === 1)
        {
            //Registrar la venta
            $.post('vender/vender.php',
            {
                cantidad_pagar : $('#cantidad_cobrar_pagar').val(),
                cantidad_credito : $('#cantidad_credito_cobrar').val(),
                observaciones : $('#observaciones_venta_credito').val(),
                id_cliente : $('#clientes_modal_cobrar_parcial').val(),
                function : 'registrar_venta_parcial'
            }, function(data)
            {
                var datos = typeof data === 'object' ? data : JSON.parse(data);
                
                if(datos.tipo === 3)
                {
                    $.gritter.add({
                        title: '¡ERROR!',
                        text: "Error al registrar la venta",
                        class_name: 'gritter-error'
                    });
                }
                else
                {
                    $('#modal_cobrar_parcial').modal('hide');
                    setTimeout(function(){
                        $('#vender').click();
                        $.gritter.add({
                            title: '¡CORRECTO!',
                            text: "Venta registrada correctamente; se actualizó el saldo del cliente.",
                            class_name: 'gritter-success'
                        });
                    }, 1000);
                }
            });
        }
    });
}

function aumentar_limite_credito()
{
    $.post('vender/vender.php', 
    {
        limite_actual   : $('#limite_credito_actual').val(),
        limite_nuevo    : $('#limite_credito_nuevo').val(),
        codigo_autoriza : $('#codigo_autorizacion').val(),
        id_cliente      : $('#id_cliente_aumentar_credito').val(),
        function        : 'aumentar_limite_credito'
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡ERROR!',
                text: datos.mensaje,
                class_name: 'gritter-warning'
            });
        }
        else
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: "Límite de crédito aumentado correctamente.",
                class_name: 'gritter-success'
            });
            
            //Cerrar el modal de aumento de crédito
            $('#modal_aumentar_credito').modal('hide');
            
            //Limpiar los campos
            $('#limite_credito_actual').val('');
            $('#limite_credito_nuevo').val('');
            $('#codigo_autorizacion').val('');
            $('#id_cliente_aumentar_credito').val('');
            
            //Abrir nuevamente el modal para la venta a crédito
            $('#modal_cobrar_parcial').modal('show');
        }
    });
}

function registrar_importe()
{
    $.post('vender/vender.php',
    {
        referencia : $('#referencia_importe').val(),
        cantidad : $('#cantidad_importe').val(),
        tipo_envase : $('#tipo_envase').val(),
        notas : $('#notas').val(),
        function : 'registrar_importe' 
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        if(datos.tipo === 3)
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: datos.mensaje,
                class_name: 'gritter-error'
            });
        }
        else
        {
            $.gritter.add({
                title: '¡CORRECTO!',
                text: datos.mensaje,
                class_name: 'gritter-success'
            });
            
            $('#referencia_importe').val('');
            $('#cantidad_importe').val('0.00');
            $('#notas').val('');
            
            $('#modal_importe').modal('hide');
        }
    });
}

function actualizar_cantidad(id_articulo)
{
    var cantidad = $('#in_act_'+id_articulo).val();
    
    $.post('vender/vender.php',
    {
        cantidad : cantidad,
        id_articulo: id_articulo,
        function : 'actualizar_cantidad_articulo' 
    }, function(data)
    {
        var datos = typeof data === 'object' ? data : JSON.parse(data);
        
        switch(datos.tipo)
        {
            case 1:
                $.gritter.add({
                    title: '¡CORRECTO!',
                    text: datos.mensaje,
                    class_name: 'gritter-success'
                });
                
                $('#body_tabla_compra').html(datos.contenido.html);
                $('#total_a_cobrar').val(datos.contenido.costo);
                break;
            case 2:
                $.gritter.add({
                    title: '¡AVISO!',
                    text: datos.mensaje,
                    class_name: 'gritter-error'
                });
                break;
        }
    });
}

function radios_descuento(id_radio)
{
    switch(id_radio)
    {
        case 'no':
            $('#form-descuento-pesos').hide();
            $('#form-descuento-porcentual').hide();
            break;
        case 'porcentual':
            $('#form-descuento-pesos').hide();
            $('#form-descuento-porcentual').show();
            break;
        case 'pesos':
            $('#form-descuento-pesos').show();
            $('#form-descuento-porcentual').hide();
            break;
    }
}

//Matriz de comandos
var shift_presionado = false;
var tecla_shift = 16, tecla_a = 65, tecla_c = 67, tecla_t = 84, tecla_r = 82, tecla_g = 71, tecla_i = 73;

/*$(document).keydown(function(e){
    if (e.keyCode == tecla_shift)
        shift_presionado = true;
    
    switch(e.keyCode)
    {
        case tecla_a:
            if(shift_presionado)
                agregar_producto_chose();
            break;
        case tecla_c:
            if(shift_presionado)
                lanzar_modal_cobrar();
            break;
        case tecla_t:
            if(shift_presionado)
                lanzar_modal_cobrar_tarjeta();
            break;
        case tecla_r:
            if(shift_presionado)
                lanzar_modal_cobrar_parcial();
            break;
        case tecla_g:
            if(shift_presionado)
                window.open("http://localhost/ProyectosIs/cobro_agua_menores/index.php/inicio", '_blank');
            break;
        case tecla_i:
            $('#btn_importe').click();
            break;
    }

});*/

$('#btn_calcular_cambio').click(function(){
    var total = $('#input_total_aux').val();
    
    var recibi = $('#input_recibi').val();
    
    var descuento = 0;

    //Validar qué radio está seleccionado para aplicar el descuento
    if($('#porcentual').prop('checked'))
    {
        var porcentaje = $('#input_descuento_porcentual').val();
        
        var valor = total*porcentaje;
        
        descuento = (valor/100);
        
        if(recibi < (total-descuento))
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: "El valor recibido no puede ser menor que el total a pagar incluyendo el descuento.",
                class_name: 'gritter-warning'
            });
            
            return;
        }
    }
    else if($('#pesos').prop('checked'))
    {
        descuento = $('#input_descuento_pesos').val();
        
        if(recibi < (total-descuento))
        {
            $.gritter.add({
                title: '¡AVISO!',
                text: "El valor recibido no puede ser menor que el total a pagar incluyendo el descuento.",
                class_name: 'gritter-warning'
            });
            
            return;
        }
    }
    
    if(recibi < (total-descuento))
    {
        $.gritter.add({
            title: '¡AVISO!',
            text: "El valor recibido no puede ser menor que el total a pagar.",
            class_name: 'gritter-warning'
        });

        return;
    }
    
    var cambio = ((recibi-total)-descuento);
    
    $('#input_cambio').val(cambio);
});

$(document).keyup(function(e){
    if (e.keyCode == tecla_shift)
    shift_presionado = false;
});

$('#concluir_venta').click(function(){
    $('#modal_cobrar').modal('hide');
    $('#vender').click();
});