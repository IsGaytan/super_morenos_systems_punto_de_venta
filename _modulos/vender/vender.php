<?php
require_once '../../_conection/bd_connect.php';
require_once '../../_general/general_functions.php';
require_once 'carrito_vender/carrito_vender.class.php';
require_once '../../_librerias/escpos/Escpos.php';
require_once '../../_general/item_printer.php';
require_once 'impresiones_vender.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'agregar_articulo_carrito_comprar':
            $datos = $_POST;
            agregar_articulo_carrito_comprar($datos);
            break;
        case 'eliminar_articulo_venta':
            eliminar_articulo_venta($_POST['id_articulo']);
            break;
        case 'modificar_cantidad_vender':
            modificar_cantidad_vender($_POST);
            break;
        case 'obtener_lista_vender':
            obtener_lista_vender();
            break;       
        case 'validar_contenido_carrito':
            validar_contenido_carrito_vender();
            break;
        case 'venta_efectivo':
            $datos_venta = $_POST;
            venta_efectivo($datos_venta);
            break;
        case 'realizar_cobro_tarjeta':
            realizar_cobro_tarjeta($_POST);
            break;
        case 'validar_credito':
            validar_credito($_POST);
            break;
        case 'registrar_recarga':
            registrar_recarga($_POST);
            break;
        case 'registrar_importe':
            registrar_importe($_POST);
            break;
        case 'aumentar_limite_credito':
            aumentar_limite_credito($_POST);
            break;
        case 'registrar_venta_parcial':
            registrar_venta_parcial($_POST);
            break;
        case 'actualizar_cantidad_articulo':
            actualizar_cantidad_articulo($_POST);
            break;
        case 'realizar_cobro_tarjeta_vales':
            realizar_cobro_tarjeta_vales($_POST);
            break;
    }
}

function agregar_articulo_carrito_comprar($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $datos_articulo = '';
        
        $connect = conectar_bd::realizar_conexion();
        
        //SI trae el id del producto es porque proviene del combo box
        //si NO lo trae, es porque viene del código de barras
        if(isset($datos['id_producto']))
        {
            $id_producto = $datos['id_producto'];
            
            $query = "SELECT * FROM productos WHERE id_producto = $id_producto;";
            
            $filas = $connect->query($query);
            
            if (!empty($filas) && $filas->num_rows > 0) 
            {
                $rows = $filas->fetch_all(MYSQLI_ASSOC);

                $datos_articulo['id_producto'] = $rows[0]['id_producto'];
                $datos_articulo['nombre']      = $rows[0]['marca'] . $rows[0]['pres_medida'] . $rows[0]['pres_unidad'];
                $datos_articulo['cantidad']    = 1;
                $datos_articulo['precio']      = $rows[0]['precio_venta'];
                $datos_articulo['codigo_barras']   = $rows[0]['codigo_barras'];
                $datos_articulo['venta_libre']   = $rows[0]['venta_libre'];
                $datos_articulo['texto_ticket']   = $rows[0]['texto_ticket'];
                
                carrito_vender::add_carrito_vender($datos_articulo);
                 
                $resultado['contenido'] = obtener_html_tabla_vender();
            } else 
            {
                throw new Exception("No se encontraron datos");
            }
        }
        else if(isset($datos['codigo_barras']))
        {
            $codigo_barras = $datos['codigo_barras'];
            
            $query = "SELECT * FROM productos WHERE codigo_barras = $codigo_barras;";
            
            $filas = $connect->query($query);
            
            if (!empty($filas) && $filas->num_rows > 0) 
            {
                $rows = $filas->fetch_all(MYSQLI_ASSOC);

                $datos_articulo['id_producto']     = $rows[0]['id_producto'];
                $datos_articulo['nombre']          = $rows[0]['marca'] . $rows[0]['pres_medida'] . $rows[0]['pres_unidad'];
                $datos_articulo['cantidad']        = 1;
                $datos_articulo['precio']          = $rows[0]['precio_venta'];
                $datos_articulo['codigo_barras']   = $rows[0]['codigo_barras'];
                $datos_articulo['venta_libre']   = $rows[0]['venta_libre'];
                $datos_articulo['texto_ticket']   = $rows[0]['texto_ticket'];

                carrito_vender::add_carrito_vender($datos_articulo);
                 
                $resultado['contenido'] = obtener_html_tabla_vender();
            } else 
            {
                throw new Exception("No se encontraron datos");
            }
            
        }
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        
    }
}

//Eliminar del carrito de venta el artículo que llega como parámetro
function eliminar_articulo_venta($id_articulo)
{
    carrito_vender::remove_carrito_vender($id_articulo);
    
    $res = obtener_html_tabla_vender();
            
    echo json_encode($res);
}

function modificar_cantidad_vender($datos_modificar)
{
    $resultado = '';

    $id_articulo = $datos_modificar['id_articulo'];
    $action = $datos_modificar['action'];

    if($action == 'up')
    {
        //Aumentar la cantidad (cantidad+1) en el carrito donde se encuentre el artículo con e id_articulo que corresponde
        carrito_vender::aumentar_cantidad_vender($id_articulo);
    }
    else
    {
        //Disminuir la cantidad (cantidad-1) en el carrito donde se encuentre el artículo con el id_articulo que corresponde
        carrito_vender::disminuir_cantidad_vender($id_articulo);
    }

    $resultado = obtener_html_tabla_vender();

    echo json_encode($resultado);
        
}

function obtener_html_tabla_vender()
{
    $respuesta = array(
        'html' => '',
        'costo' => ''
    );
    
    //carrito_vender::destruir_carrito();
    if(carrito_vender::check_carrito_vender_content())
    {
        $articulos_carrito = $_SESSION['carrito_vender']['articulos'];
        
        foreach($articulos_carrito as $articulo)
        {
            $codigo_barras  = $articulo['codigo_barras'];
            $nombre         = $articulo['nombre'];
            $cantidad       = $articulo['cantidad'];
            $precio         = $articulo['precio'];
            $id_articulo    = $articulo['id_articulo'];
            
            $controles = "";
            
            if($articulo['venta_libre'] == 0)
            {
                $controles = "<div class='input-group'>
                                        <input type='text' class='input-mini spinbox-input form-control' value='$cantidad' disabled>
                                        <div class='spinbox-buttons input-group-btn btn-group-vertical'>
                                            <button btn_aumentar='$id_articulo' action='up' onclick='modificar_cantidad($(this).attr(\"action\"),$(this).attr(\"btn_aumentar\"));' type='button' class='btn spinbox-up btn-xs btn-info'>
                                                <i class=' ace-icon fa fa-chevron-up'></i>
                                            </button>
                                            <button btn_disminuir='$id_articulo' action='down' onclick='modificar_cantidad($(this).attr(\"action\"),$(this).attr(\"btn_disminuir\"));' type='button' class='btn spinbox-down btn-xs btn-info'>
                                                <i class=' ace-icon fa fa-chevron-down'></i>
                                            </button>
                                        </div>
                                    </div>";
            }
            else
            {
                $controles = "<div class='input-group'>
                                        <input id='in_act_$id_articulo' type='text' class='input-mini form-control' value='$cantidad'>
                                        <span class='input-group-btn'>
                                            <button btn_actualizar='$id_articulo' type='button' class='btn btn-sm btn-info' onclick='actualizar_cantidad($(this).attr(\"btn_actualizar\"));'>
                                                <i class='ace-icon fa fa-refresh'></i>
                                            </button>
                                        </span>
                                    </div>";
            }
            
            $respuesta['html'] .= "  <tr>
                            <td class='text-muted center'>$codigo_barras</td>
                            <td class='orange'>$nombre</td>
                            <td class='center'>
                                <div class='ace-spinner middle' style='width:50%'>
                                    $controles
                                </div>
                            </td>
                            <td class='green' style='text-align: right;'>$precio</td>
                            <td class='center'>
                                <div clsss='hidden-sm hidden-xs action-buttons'>
                                    <a id='$id_articulo' class='red tooltip-error' onclick='eliminar_articulo_venta(this.id);' data-rel='tooltip' data-placement='top' data-original-title='Eliminar de la lista'>
                                        <i class='ace-icon fa fa-trash-o bigger-130'></i>
                                    </a>
                                </div>
                            </td>
                            <td class='green' style='text-align: right;'>" . $cantidad*$precio . "</td>
                        </tr>";
        }
        $respuesta['costo'] = $_SESSION['carrito_vender']['precio_carrito'];
    }
    else
    {
        $respuesta['html'] = "<tr><td colspan='6' align='center'>No hay artículos en la lista</td></tr>";
        $respuesta['costo'] = 0.00;
    }
    
    return $respuesta;
}

function obtener_lista_vender()
{
    $res = obtener_html_tabla_vender();
    
    echo json_encode($res);
}

function validar_contenido_carrito_vender()
{
    try
    {
        $respuesta = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $tiene_contenido = carrito_vender::check_carrito_vender_content();

        if(!$tiene_contenido)
        {
            throw new Exception('No tiene artículos agregados, no puede realizar un cobro.');
        }
        
        echo json_encode($respuesta);
        
    }catch(Exception $ex)
    {
        $respuesta = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        echo json_encode($respuesta);
    }
}

function venta_efectivo($datos_venta)
{
    try
    {
        $resultado = array(
            'tipo' => 2,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $tipo_cobro = "EFECTIVO";
        $tipo = 1;
        
        
        $total_venta = $_SESSION['carrito_vender']['precio_carrito'];
        $descuento = 0;
        $cambio = 0;
        
        $resumen = "<h4 class='green'>El TOTAL de la venta fue <b>$".  number_format($total_venta, 2)."</b></h4>";
        
        switch($datos_venta['tipo_descuento'])
        {
            case 'no':
                if(!is_numeric($datos_venta['recibi']))
                {
                    throw new Exception("La cantidad recibida tiene que ser un valor numérico.",2);
                }

                if($datos_venta['recibi'] < $_SESSION['carrito_vender']['precio_carrito'])
                {
                    throw new Exception("Lo que recibe no puede ser menor a lo que cobrará.",2);
                }
                
                $cambio = $datos_venta['recibi'] - $total_venta;
                break;
            case 'porcentual':
                if(!is_numeric($datos_venta['descuento']) || !is_numeric($datos_venta['recibi']))
                {
                    throw new Exception("La cantidad recibida y el porcentaje de descuento tienen que ser valores numéricos.",2);
                }
                
                if($datos_venta['descuento'] > 20)
                {
                    throw new Exception("No puede dar un descuento mayor al 20%.",2);
                }
                
                $descuento = (($_SESSION['carrito_vender']['precio_carrito'] * $datos_venta['descuento']) / 100);

                if($datos_venta['recibi'] < ($_SESSION['carrito_vender']['precio_carrito'] - $descuento))
                {
                    throw new Exception("Lo que recibe no puede ser menor a lo que cobrará (incluyendo el descuento).",2);
                }
                
                $cambio = $datos_venta['recibi'] - ($total_venta - $descuento);
                
                break;
            case 'pesos':
                if(!is_numeric($datos_venta['descuento']) || !is_numeric($datos_venta['recibi']))
                {
                    throw new Exception("La cantidad recibida y la cantidad de descuento tienen que ser valores numéricos.",2);
                }
                
                if($datos_venta['descuento'] > $_SESSION['carrito_vender']['precio_carrito'])
                {
                    throw new Exception("No puede dar un descuento con valor mayor a lo que tiene que cobrar.",2);
                }
                
                $descuento = $datos_venta['descuento'];

                if($datos_venta['recibi'] < ($_SESSION['carrito_vender']['precio_carrito'] - $descuento))
                {
                    throw new Exception("Lo que recibe no puede ser menor a lo que cobrará (incluyendo el descuento).",2);
                }
                
                $cambio = $datos_venta['recibi'] - ($total_venta - $descuento);
                
                break;
        }
        
        if($datos_venta['tipo_descuento'] == 'pesos' || $datos_venta['tipo_descuento'] == 'porcentual')
        {
            $resumen .= "<h4 class='green'>Se aplicó un DESCUENTO de<b>$".  number_format($descuento, 2)."</b></h4>";
        }
        
        $resumen .= "<h4 class='green'>El CAMBIO: <b>$".  number_format($cambio, 2)."</b></h4>";
        
        /*
         * Obtener los datos del usuario que realizó el importe para colocarlos en el ticket.
         */
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        $conect = conectar_bd::realizar_conexion();
        $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
        $filas = $conect->query($query);
        $rows = $filas->fetch_all(MYSQLI_ASSOC);
        $conect->close();
        
        $nombre_usuario = $rows[0]['nombre'] . " " . $rows[0]['apellido_pat'];
        
        $articulos_cobrar = $_SESSION['carrito_vender']['articulos'];
        
        $resultado = realizar_cobro($tipo_cobro, $tipo, $descuento);
        
        if($resultado['tipo'] == 1)
        {
            $resultado['contenido'] = $resumen;
            
            $obj_impresion = new Impresiones_vender($resultado['id_venta'],$total_venta, $datos_venta['recibi'], $cambio, $tipo_venta = "Venta General", $nombre_usuario, $articulos_cobrar);
        
            $obj_impresion ->imprimir_ticket_venta();
        }
        else
        {
            throw new Exception($resultado['mensaje'],3);
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

/*
 * Es llamada por las ventas en efectivo, en tarjeta y ventas efectivo a crédito
 * Actualiza la existencia de los artículos y registra los datos generales de la venta en la tabla ventas
 * author@ - IsGaytanD
 */
function realizar_cobro($tipo_cobro, $tipo, $descuento = 0)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => '',
            'id_venta' => 0
        );
        $articulos_cobrar = $_SESSION['carrito_vender']['articulos'];
        
        $conect = conectar_bd::realizar_conexion();
      
        $conect->begin_transaction();
        
        foreach($articulos_cobrar as $articulo)
        {
            //Descontar de las existencias
            $stmt = $conect->prepare("UPDATE productos SET existencia = (existencia - ?) WHERE id_producto = ?;");
            $stmt->bind_param('di', $articulo['cantidad'], $articulo['id_producto']);
            $stmt->execute();
        }
        
        //Insertar los datos de la venta
        $stmt = $conect->prepare("INSERT INTO ventas (fecha, total, tipo, id_usuario, tipo_cobro) VALUES (?,?,?,?,?)");
        $stmt->bind_param('sdiis', $fecha_hoy, $precio, $tipo, $id_usuario, $tipo_cobro);
        
        $fecha_hoy = date("Y-m-d H:i:s");
        $precio = $_SESSION['carrito_vender']['precio_carrito'] - $descuento;
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        $stmt->execute();
        
        $resultado['id_venta'] = $conect->insert_id;
        
        $conect->commit();

        $conect->close();
        
        carrito_vender::destruir_carrito();
        
        $resultado['mensaje'] = 'Venta realizada correctamente, imprimiendo ticket...';
        
        return($resultado);
    }
    catch(Exception $ex)
    {
        error_log("EL ERROR..." . print_r($ex->getMessage(), true));
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        return($resultado);
    }
}

function realizar_cobro_tarjeta($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        if(trim($datos['terminal_venta']) == '' || trim($datos['transaccion_venta']) == '' || trim($datos['lote_venta']) == '' || trim($datos['factura_venta']) == '' || trim($datos['referencia_venta']) == '')
        {
            throw new Exception("Debe completar todos los datos para realizar el cobro.");
        }
        
        $tipo_cobro = "TARJETA";
        $tipo = 1;
        
        $articulos_cobrar = $_SESSION['carrito_vender']['articulos'];
        $total_venta = $_SESSION['carrito_vender']['precio_carrito'];
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        $resultado = realizar_cobro($tipo_cobro, $tipo);
        
        if($resultado['tipo'] == 1)
        {
            $conect = conectar_bd::realizar_conexion();

            $stmt = $conect->prepare("INSERT INTO ventas_tarjeta (id_venta, terminal, transaccion, lote, factura, referencia) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param('isssss', $id_venta, $terminal, $transaccion, $lote, $factura, $referencia);

            $id_venta = $resultado['id_venta'];
            $terminal = $datos['terminal_venta'];
            $transaccion = $datos['transaccion_venta'];
            $lote = $datos['lote_venta'];
            $factura = $datos['factura_venta'];
            $referencia = $datos['referencia_venta'];
            
            $stmt->execute();
            
            $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
            $filas = $conect->query($query);
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $nombre_usuario = $rows[0]['nombre'] . " " . $rows[0]['apellido_pat'];

            $obj_impresion = new Impresiones_tarjeta($id_venta, $total_venta, $tipo_venta = "Venta con Tarjeta", $nombre_usuario, $articulos_cobrar, $datos['referencia_venta']);

            $obj_impresion ->imprimir_ticket_venta_tarjeta();
            
            $conect->close();
        }
        else
        {
            throw new Exception($res['mensaje']);
        }
        
        echo json_encode($resultado);    
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );

        echo json_encode($resultado);
    }
}

/*
 * Es llamada por las ventas en efectivo, en tarjeta y ventas efectivo a crédito
 * Actualiza la existencia de los artículos y registra los datos generales de la venta en la tabla ventas
 * author@ - IsGaytanD
 */
function realizar_cobro_tarjeta_vales($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );

        if(trim($datos['tarjetahabiente']) == '' || trim($datos['num_tarjeta']) == '')
        {
            throw new Exception("Debe agregar una referencia y los cuatro últimos números de la tarjeta para realizar el ocbro.", 2);
        }
        
        if(!is_numeric(trim($datos['num_tarjeta'])))
        {
            throw new Exception("Debe ingresar sólo números en el campo 'Número de la tarjeta'.", 2);
        }
        
        $tipo_cobro = "TARJETA_VALES";
        $tipo = 1;
        
        $articulos_cobrar = $_SESSION['carrito_vender']['articulos'];
        $total_venta = $_SESSION['carrito_vender']['precio_carrito'];
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        $resultado = realizar_cobro($tipo_cobro, $tipo);
        
        if($resultado['tipo'] == 1)
        {
            $conect = conectar_bd::realizar_conexion();

            $stmt = $conect->prepare("INSERT INTO ventas_vales (id_venta, tarjetahabiente, digitos_tarjeta) VALUES (?,?,?)");
            $stmt->bind_param('isi', $id_venta, $tarjetahabiente, $num_tarjeta);

            $id_venta           = $resultado['id_venta'];
            $tarjetahabiente    = $datos['tarjetahabiente'];
            $num_tarjeta        = $datos['num_tarjeta'];
            
            $stmt->execute();
            
            $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
            $filas = $conect->query($query);
            $rows = $filas->fetch_all(MYSQLI_ASSOC);

            $nombre_usuario = $rows[0]['nombre'] . " " . $rows[0]['apellido_pat'];

            $obj_impresion = new Impresiones_tarjeta($id_venta, $total_venta, $tipo_venta = "Venta con Tarjeta de Vales", $nombre_usuario, $articulos_cobrar, $datos['tarjetahabiente']);

            $obj_impresion ->imprimir_ticket_venta_tarjeta();
            
            $conect->close();
        }
        else
        {
            throw new Exception($res['mensaje']);
        }
        
        echo json_encode($resultado);    
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );

        echo json_encode($resultado);
    }
}

function validar_credito($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );

        $conect = conectar_bd::realizar_conexion();
        
        $id_cliente = $datos['id_cliente'];
        
        $query = "SELECT * FROM clientes WHERE estatus = 'AC' AND id_cliente = $id_cliente";
        
        $filas = $conect->query($query);
        
        if(!empty($filas) && $filas->num_rows > 0)
        {
            $datos_cliente = $filas->fetch_all(MYSQLI_ASSOC);

            $nuevo_saldo = $datos_cliente[0]['saldo'] + $datos['cantidad_credito'];

            if($nuevo_saldo > $datos_cliente[0]['limite_credito'])
            {
                $resultado = array(
                    'tipo' => 2,
                    'mensaje' => 'El cliente está exediendo su límite de crédito con esta compra.',
                    'contenido' => $datos_cliente[0]
                );
            }
            
        } 
        else
        {
            throw new Exception("Error al consultar los datos del cliente, intente más tarde.");
        }
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );

        echo json_encode($resultado);
    }
}

function registrar_recarga($datos)
{
    try 
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        //Validar los campos vacíos
        $error = false;
        if($datos['telefono'] === '')
            $error = true;
        if($datos['monto'] === '')
            $error = true;
        if($datos['compania'] === '')
            $error = true;
        if($datos['folio'] === '')
            $error = true;
        if($datos['transaccion'] === '')
            $error = true;
        if($datos['numero_autorizacion'] === '')
            $error = true;
        
        if($error)
            throw new Exception("Por favor complete todos los campos.");
        
        //Si no hay error en el formulario realizar el registro en la base de datos.
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare("INSERT INTO recargas (monto, compania, folio, transaccion, numero_autorizacion, comision, telefono, fecha, id_usuario)
                 VALUES (?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('dssssdssi', $monto, $compania, $folio, $transaccion, $numero_autorizacion, $comision, $telefono, $fecha, $id_usuario);

        $monto = number_format($datos['monto'], 2);
        $compania = $datos['compania'];
        $folio = $datos['folio'];
        $transaccion = $datos['transaccion'];
        $numero_autorizacion = $datos['numero_autorizacion'];
        $comision = number_format(5, 2);
        $telefono = $datos['telefono'];
        $fecha = date("Y-m-d H:i:s");
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        
        $stmt->execute();

        $conect->close();
        
        $resultado['mensaje'] = "Recarga registrada correctamente.";
        
        echo json_encode($resultado);
    } catch (Exception $ex) {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function registrar_importe($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $hay_error = false;
        //Validar que no vengan los campos vacíos
        if(trim($datos['referencia']) == '')
            $hay_error = true;
        //if($datos['cantidad'] == '0.00' || $datos['cantidad'] == '0' || trim($datos['cantidad'] == ''))
        //    $hay_error = true;
        if($datos['tipo_envase'] == '')
            $hay_error = true;
        if(trim($datos['notas']) == '')
            $hay_error = true;
        
        if($hay_error)
            throw new Exception ("Debe completar todos los datos que se solicitan");
        
        switch($datos['tipo_envase'])
        {
            case 'refresco':
                $tipo_envase = "Refresco";
                break;
            case 'cerveza':
                $tipo_envase = "Cerveza";
                break;
            case 'garrafon_agua':
                $tipo_envase = "Garrafón agua";
                break;
        }
        
        //Si no hay errores en el formulario, proceder al registro del importe
        $conect = conectar_bd::realizar_conexion();

        $stmt = $conect->prepare("INSERT INTO importes (referencia, cantidad, tipo_envase, notas, id_usuario, fecha_operacion)
                 VALUES (?,?,?,?,?,?)");
        $stmt->bind_param('sdssis', $referencia, $cantidad, $tipo_envase, $notas, $id_usuario, $fecha);

        $referencia = $datos['referencia'];
        $cantidad = number_format($datos['cantidad'], 2);
        $tipo_envase = $datos['tipo_envase'];
        $notas = $datos['notas'];
        $id_usuario = $_SESSION['usuario']['id_usuario'];
        $fecha = date("Y-m-d H:i:s");
        
        $stmt->execute();

        $folio_importe = str_pad($conect->insert_id,5,"0",STR_PAD_LEFT);
        
        /*
         * Obtener los datos del cajero que realizó el importe para colocarlos en el ticket.
         */
        $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
        $filas = $conect->query($query);
        $rows = $filas->fetch_all(MYSQLI_ASSOC);
        
        $nombre_usuario = $rows[0]['nombre'] . " " . $rows[0]['apellido_pat'];
        
        $conect->close();
        
        $impresora = general_functions::nombre_impresora;
        
        $connector = new WindowsPrintConnector($impresora);

        //Instanciar la clase Escpos
        $printer = new Escpos($connector);
        
        //Obtener la ruta de la imagen
        $ruta_imagen = general_functions::ruta_imagen;
        
        //Crear la imagen para el ticket
        $tux = new EscposImage($ruta_imagen);
        
        //Alinear imagen al centro
        $printer -> setJustification(Escpos::JUSTIFY_CENTER);
        
        //Imprimir la imagen
	$printer -> graphics($tux);
	
        //Espacio en papel
        $printer -> feed();
        
        //Alineación por default
        $printer -> selectPrintMode();
        
        //Tamaño para el título del ticket
        $printer -> setTextSize(2,1);
        $printer -> text("Comprobante de importe\n");
        
        //Espacio al papael
        $printer -> feed();
        
        //Tamaño de letra para el texto del ticket
        $printer -> setTextSize(1,1);
        
        //Agregar el folio del importe registrado al ticket
        $printer -> text($folio_importe."\n");
        $printer -> text($nombre_usuario."\n");
        
        $printer -> feed();
        
        $printer -> selectPrintMode();
        
        $item = new Item_printer($tipo_envase, $datos['cantidad']);
        $printer -> text($item);
        
        $printer -> selectPrintMode();
        
        $printer -> text($datos['referencia'] . ": " . $datos['notas']);
        
        $printer -> feed();
        
        $printer ->setJustification(Escpos::JUSTIFY_CENTER);
        
        $printer -> text($fecha);
        
        $printer -> feed();
        $printer -> feed();
        
        $printer -> cut();

        $printer -> pulse($pin = 0, $on_ms = 120, $off_ms = 240);

        $printer -> close();
        
        $resultado['mensaje'] = "Importe registrado correctamente.";
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function aumentar_limite_credito($datos_autorizacion)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        if($datos_autorizacion['limite_actual'] >= $datos_autorizacion['limite_nuevo'])
        {
            throw new Exception("El nuevo límite no puede ser menor o igual al límite actual.");
        }
        
        if($datos_autorizacion['codigo_autoriza'] != general_functions::password_autorzacion)
        {
            throw new Exception("El password de autorización no es correcto.");
        }
        
        //Actualizar el límite de crédito en el cliente
        $conect = conectar_bd::realizar_conexion();
        
        $stmt = $conect->prepare("UPDATE clientes SET limite_credito = ? WHERE id_cliente = ?;");
        $stmt->bind_param('di', $datos_autorizacion['limite_nuevo'], $datos_autorizacion['id_cliente']);
        $stmt->execute();
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function registrar_venta_parcial($datos_venta)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        if(trim($datos_venta['observaciones']) == '')
        {
            throw new Exception("Debe completar todos los datos para realizar el cobro.");
        }
        
        $tipo_cobro = "EFECTIVO";
        $tipo = 2;
        
        $total_venta = $_SESSION['carrito_vender']['precio_carrito'];
        $articulos = $_SESSION['carrito_vender']['articulos'];
        
        $res = realizar_cobro($tipo_cobro, $tipo);
        
        if($res['tipo'] == 1)
        {
            $conect = conectar_bd::realizar_conexion();

            $conect->begin_transaction();
            
            $stmt = $conect->prepare("INSERT INTO ventas_credito (id_venta, cantidad_cubierta, cantidad_credito, estatus, observaciones, id_cliente) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param('iddssi', $id_venta, $cantidad_cubierta, $cantidad_credito, $estatus, $observaciones, $id_cliente);

            $id_venta = $res['id_venta'];
            $cantidad_cubierta = $datos_venta['cantidad_pagar'];
            $cantidad_credito = $datos_venta['cantidad_credito'];
            $estatus = 'PE';
            $observaciones = $datos_venta['observaciones'];
            $id_cliente = $datos_venta['id_cliente'];
            
            $stmt->execute();
            
            $stmt = $conect->prepare("UPDATE clientes SET saldo = (saldo + ?) WHERE id_cliente = ?;");
            $stmt->bind_param('di', $cantidad_credito, $datos_venta['id_cliente']);
            $stmt->execute();
            
            $conect->commit();
            
            //Obtener los datos para realizar la impresión del ticket
            $id_usuario = $_SESSION['usuario']['id_usuario'];
            
            $query = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
            $filas = $conect->query($query);
            $rows = $filas->fetch_all(MYSQLI_ASSOC);    
            
            $nombre_usuario = $rows[0]['nombre'] . " " . $rows[0]['apellido_pat'];
            
            $query = "SELECT * FROM clientes WHERE id_cliente = $id_cliente";
            $filas = $conect->query($query);
            $rows = $filas->fetch_all(MYSQLI_ASSOC);    
            $nombre_cliente = $rows[0]['nombre_cliente'] . " " . $rows[0]['apellido_pat'];
            $saldo_cliente = $rows[0]['saldo'];
            
            $obj_impresion = new Impresiones_ventas_credito($id_venta, $total_venta, $cantidad_cubierta, $cantidad_credito, $titulo_venta="Venta A Crédito", $nombre_usuario, $nombre_cliente, $saldo_cliente, $articulos);
        
            $obj_impresion ->imprimir_ticket_venta();
            
            $conect->close();
        }
        else
        {
            throw new Exception($res['mensaje']);
        }
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => 3,
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}

function actualizar_cantidad_articulo($datos)
{
    try
    {
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        if(!is_numeric($datos['cantidad']))
        {
            Throw new Exception("Debe ingresar un valor numérico.", 2);
        }
        
        carrito_vender::actualizar_cantidad_vender($datos['id_articulo'], $datos['cantidad']);
        
        $resultado['contenido'] = obtener_html_tabla_vender();
        
        $resultado['mensaje'] = "Cantidad actualizada correctamente.";
        
        echo json_encode($resultado);
    }
    catch(Exception $ex)
    {
        $resultado = array(
            'tipo' => $ex->getCode(),
            'mensaje' => $ex->getMessage(),
            'contenido' => ''
        );
        
        echo json_encode($resultado);
    }
}