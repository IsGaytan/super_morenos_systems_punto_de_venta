$('#vender').click(function(){
    $.post('../_modulos/vender/vender.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#vender').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#comprar').click(function(){
    $.post('../_modulos/comprar/comprar.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#comprar').addClass("active");
        
        $.post('../_modulos/comprar/comprar.php',
        {
            function : 'validar_contenido_carrito'
        }, function(data)
        {
            var datos = typeof data === 'object' ? data : JSON.parse(data);

            if(datos.tipo === 1)
            {
                $('#body_tabla_compra').html(datos.contenido.html);
                $('#input_total_pagar').val(datos.contenido.precio_carrito);
                $('#input_cantidad_art_carrito').val(datos.contenido.cantidad_articulos_total);
                $('.tooltip-error').tooltip();
            }
        });
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#productos').click(function(){
    $.post('../_modulos/productos/productos.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#productos').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#clientes').click(function(){
    $.post('../_modulos/clientes/clientes.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#clientes').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#cuentas_clientes').click(function(){
    $.post('../_modulos/cuentas_clientes/cuentas_clientes.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#cuentas_clientes').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#proveedores').click(function(){
    $.post('../_modulos/proveedores/proveedores.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#proveedores').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#cuentas_proveedores').click(function(){
    $.post('../_modulos/cuentas_proveedores/cuentas_proveedores.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#cuentas_proveedores').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#usuarios').click(function(){
    $.post('../_modulos/usuarios/usuarios.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#usuarios').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#importes').click(function(){
    $.post('../_modulos/importes/importes.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#importes').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#historial_ventas').click(function(){
    $.post('../_modulos/historial_ventas/historial_ventas.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#historial_ventas').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});


$('#historial_compras').click(function(){
    $.post('../_modulos/historial_compras/historial_compras.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#historial_compras').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#balance_caja').hasClass('active'))
        $('#balance_caja').removeClass('active');
});

$('#balance_caja').click(function(){
    $.post('../_modulos/balance_caja/balance_caja.html', { }, function(data){
        $('#contenido_modulo').html(data);
        
        $('#balance_caja').addClass("active");
    });
    
    //Quitar la clase active al componente que la tenga para dejársela al que hizo click
    if($('#productos').hasClass('active'))
        $('#productos').removeClass('active');
    
    if($('#clientes').hasClass('active'))
        $('#clientes').removeClass('active');
    
    if($('#vender').hasClass('active'))
        $('#vender').removeClass('active');
    
    if($('#comprar').hasClass('active'))
        $('#comprar').removeClass('active');
    
    if($('#proveedores').hasClass('active'))
        $('#proveedores').removeClass('active');
    
    if($('#cuentas_proveedores').hasClass('active'))
        $('#cuentas_proveedores').removeClass('active');
    
    if($('#cuentas_clientes').hasClass('active'))
        $('#cuentas_clientes').removeClass('active');
    
    if($('#usuarios').hasClass('active'))
        $('#usuarios').removeClass('active');
    
    if($('#importes').hasClass('active'))
        $('#importes').removeClass('active');
    
    if($('#historial_ventas').hasClass('active'))
        $('#historial_ventas').removeClass('active');
    
    if($('#historial_compras').hasClass('active'))
        $('#historial_compras').removeClass('active');
});



