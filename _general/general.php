<?php
require_once '../_conection/bd_connect.php';

if(isset($_POST['function']))
{
    switch($_POST['function'])
    {
        case 'obtiene_combo_proveedores':
            obtiene_combo_proveedores();
            break;
        case 'obtiene_combo_productos':
            obtiene_combo_productos();
            break;
        case 'obten_datos_producto':
            $datos = $_POST;
            obten_datos_producto($datos);
            break;
    }
}

function obtiene_combo_proveedores()
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $query = "SELECT id_proveedor, nombre_proveedor FROM proveedores WHERE estatus = 'AC'";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
             $rows = $filas->fetch_all(MYSQLI_ASSOC);
             $html = '';
             
             foreach($rows as $datos_proveedores)
             {
                 $id_proveedor = $datos_proveedores['id_proveedor'];
                 $nombre_proveedor = $datos_proveedores['nombre_proveedor'];
                 
                 
                 $html .= "<option value='$id_proveedor'>$nombre_proveedor</option>";
                 
             }
             
             $resultado['contenido'] = $html;
        } else 
        {
            throw new Exception('');
        }
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function obtiene_combo_productos()
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $query = "SELECT id_producto, marca, pres_medida, pres_unidad FROM productos WHERE estatus = 'AC'";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
             $rows = $filas->fetch_all(MYSQLI_ASSOC);
             $html = '';
             
             foreach($rows as $datos_productos)
             {
                 $id_producto = $datos_productos['id_producto'];
                 $marca_presentacion = $datos_productos['marca'] . ' ' . $datos_productos['pres_medida'] . $datos_productos['pres_unidad'];

                 $html .= "<option value='$id_producto'>$marca_presentacion</option>";
             }
             
             $resultado['contenido'] = $html;
        } else 
        {
            throw new Exception('');
        }
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}

function obten_datos_producto($datos)
{
    try
    {
        $conect = conectar_bd::realizar_conexion();
        
        $resultado = array(
            'tipo' => 1,
            'mensaje' => '',
            'contenido' => ''
        );
        
        $conect->begin_transaction();
        
        if(isset($datos['codigo_barras']))
        {
            $condicion = "codigo_barras = '". $datos['codigo_barras']. "'";
        }
        
        if(isset($datos['id_producto']))
        {
            $condicion = "id_producto = '". $datos['id_producto']. "'";
        }
        
        $query = "SELECT * FROM productos WHERE $condicion";
        
        $filas = $conect->query($query);
        
        if (!empty($filas) && $filas->num_rows > 0) 
        {
            $rows = $filas->fetch_assoc();
             
            $resultado['contenido'] = $rows;
        } else 
        {
            throw new Exception('No se encontraron datos.');
        }
        
        echo json_encode($resultado);
    }catch(Exception $ex)
    {
        $resultado['tipo'] = 3;
        $resultado['mensaje'] = $ex->getMessage();
        echo json_encode($resultado);
    }
}
