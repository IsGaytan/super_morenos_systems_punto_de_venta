<?php
/**
 * Description of general_functions
 *
 * @author IsGaytán
 */
class general_functions
{
    CONST password_autorzacion = 123456;
    
    public function valida_solo_letras($dato)
    {
        if(preg_match("/^([a-zA-ZáéíóúÁÉÍÓÚ .-])+$/", $dato))
            return true;
        else
            return false;
    }
    
    public function valida_fechas($fecha_inicial, $fecha_final)
    {
        $validacion = array(
            'hay_error' => false,
            'mensaje' => ''
        );
        
        if(trim($fecha_inicial) == '')
        {
            $validacion['hay_error'] = true;
        }
        if(trim($fecha_final) == '')
        {
            $validacion['hay_error'] = true;
        }
        
        if($validacion['hay_error'])
        {
            $validacion['mensaje'] = "Debe seleccionar ambas fechas.";
            return $validacion;
        }
        
        if(trim($fecha_inicial) != '' && trim($fecha_final) != '')
        {
            //Validar que la fecha inicial no sea mayor que la fecha final
            list($mes, $dia, $anio) = split('[/.-]', $fecha_inicial);
            list($mes_f, $dia_f, $anio_f) = split('[/.-]', $fecha_final);

            if(($anio > $anio_f) || ($anio == $anio_f && $mes > $mes_f) || ($anio == $anio_f && $mes == $mes_f && $dia > $dia_f))
            {
                $validacion['hay_error'] = true;
                $validacion['mensaje'] = "La fecha inicial no debe ser mayor que la fecha final.";
            }
        }
        
        return $validacion;
    }
    
    public function make_menu()
    {
        $content_file = file_get_contents('../_general/menus.yml', 'r');
                
        $array_menus = Spyc::YAMLLoad($content_file);
        
        $html_menu = '';
        
        foreach($array_menus as $menu)
        {
            $id_js = $menu['id_js'];
            $icon = $menu['menu_icon'];
            $nombre = $menu['nombre_menu'];

            switch($menu['tipo'])
            {
                case 1:
                    switch($_SESSION['usuario']['rol'])
                    {
                        case 'admin':
                            $html_menu .= " <li id='$id_js' class=''>
                                        <a href='#'>
                                            <i class='menu-icon fa $icon'></i>
                                            <span class='menu-text'> $nombre </span>
                                        </a>
                                        <b class='arrow'></b>
                                    </li>";
                            break;
                        case 'cajero':
                            if($menu['permisos'] == 2)
                            {
                                $html_menu .= " <li id='$id_js' class=''>
                                                <a href='#'>
                                                    <i class='menu-icon fa $icon'></i>
                                                    <span class='menu-text'> $nombre </span>
                                                </a>
                                                <b class='arrow'></b>
                                            </li>";
                            }
                            break;
                    }
                    break;
                case 2:
                    $html_menu .= " <li class=''>
                                        <a href='#' class='dropdown-toggle'>
                                            <i class='menu-icon fa $icon'></i>
                                            <span class='menu-text'> $nombre </span>
                                            <b class='arrow fa fa-angle-down'></b>
                                        </a>
                                        <b class='arrow'></b>

                                        <ul class='submenu'>";

                    foreach($menu['hijos'] as $menu_hijo)
                    {
                        $id_js = $menu_hijo['id_js'];
                        $nombre = $menu_hijo['nombre_menu'];

                        switch($_SESSION['usuario']['rol'])
                        {
                            case 'admin':
                                $html_menu .= " <li id='$id_js' class=''>
                                                    <a href='#'>
                                                        <i class='menu-icon fa fa-caret-right'></i>
                                                        $nombre
                                                    </a>
                                                    <b class='arrow'></b>
                                                </li>";
                                break;
                            case 'cajero':
                                if($menu_hijo['permisos'] == 2)
                                {
                                    $html_menu .= " <li id='$id_js' class=''>
                                                        <a href='#'>
                                                            <i class='menu-icon fa fa-caret-right'></i>
                                                            $nombre
                                                        </a>
                                                        <b class='arrow'></b>
                                                    </li>";
                                }
                                break;
                        }
                    }
                    $html_menu .= "</ul></li>";
                    break;
            }
        }
                
        return $html_menu;
    }
    
    CONST pass_autorizacion_descuento = "sms2016";
    
    /*//Is Gaytán
    public $ruta_imagen = "C://xampp/htdocs/ProyectosIs/super_morenos/_img/logo_ticket_super_morenos.jpg";
    //Is Gaytán
    public $nombre_impresora = "Print Receipt";*/
    
    /*//Is Gaytán SUPRAMAX
    public $ruta_imagen = "C://xampp/htdocs/proyectos_web/super_morenos/_img/logo_ticket_super_morenos.jpg";
    //Is Gaytán
    public $nombre_impresora = "Print Receipt";*/
    
    /*
    //Súper Morenos 2
    CONST ruta_imagen = "C://xampp/htdocs/super_morenos/_img/logo_ticket_super_morenos_mini.jpg";
    //Súper Morenos 2
    CONST nombre_impresora = "impresora_ticket";*/
    
    //Súper Morenos 1
    CONST ruta_imagen = "C://xampp/htdocs/proyectos_web/super_morenos/_img/logo_ticket_super_morenos_mini.jpg";
    //Súper Morenos 1
    CONST nombre_impresora = "impresora_ticket";
}
