-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-11-2015 a las 21:52:15
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `super_morenos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abonos`
--

CREATE TABLE IF NOT EXISTS `abonos` (
  `id_abono` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad_abono` float(6,2) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `observaciones` varchar(105) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `abonos`
--

INSERT INTO `abonos` (`id_abono`, `fecha`, `cantidad_abono`, `id_usuario`, `id_cliente`, `observaciones`) VALUES
(1, '2015-11-10 21:27:23', 20.00, 1, 4, 'Is viene a dejar de abono $20'),
(2, '2015-11-10 21:40:23', 20.00, 1, 4, 'Margarita viene a dejas $20 de Is'),
(3, '2015-11-10 21:46:10', 10.00, 1, 4, 'Is salda su cuenta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido_pat` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido_mat` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `numero` varchar(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `municipio` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'AC',
  `saldo` float(6,2) NOT NULL DEFAULT '0.00',
  `limite_credito` float(6,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `apellido_pat`, `apellido_mat`, `alias`, `telefono`, `calle`, `numero`, `colonia`, `municipio`, `cp`, `estatus`, `saldo`, `limite_credito`) VALUES
(4, 'Israel', 'Gaytan', 'Rodríguez', 'Is Gaytán', '472-729-8228', 'Primavera', '1992', 'Del Fresno', 'Guadalajara', 43100, 'AC', -2.50, 100.00),
(5, 'Alfredo', 'Sauceda', 'Hernández', 'Pelotón', '472-787-8788', 'Niños Héroes', '34', 'Menores', 'Silao', 36294, 'AC', 0.00, 100.00),
(6, 'Ramiro', 'Quintero', 'Hernández', 'El Rames', '472-151-5151', '5 de mayo', '3', 'Menores', 'Silao', 36594, 'AC', 7.00, 100.00),
(7, 'Mariano', 'Moreno', 'Rodriguez', 'Super Moreno', '472-123-4567', '5 de mayo', '21', 'Menores', 'Silao', 36294, 'AC', 0.00, 100.00),
(8, 'Margarita', 'Rodríguez', 'Macías', 'Mago', '472-123-1231', '5 de mayo', '21', 'Menores', 'Silao', 36294, 'AC', 0.00, 100.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_especiales`
--

CREATE TABLE IF NOT EXISTS `compras_especiales` (
  `id_compra` bigint(20) NOT NULL,
  `fecha_operacion` datetime NOT NULL,
  `total_compra` float(6,2) NOT NULL,
  `total_pagado` float(6,2) NOT NULL,
  `total_a_credito` float(6,2) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PE',
  `fecha_pago` datetime NOT NULL,
  `referencia_pago` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dias_visita_proveedor`
--

CREATE TABLE IF NOT EXISTS `dias_visita_proveedor` (
  `id` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `lunes` tinyint(1) NOT NULL,
  `martes` tinyint(1) NOT NULL,
  `miercoles` tinyint(1) NOT NULL,
  `jueves` tinyint(1) NOT NULL,
  `viernes` tinyint(1) NOT NULL,
  `sabado` tinyint(1) NOT NULL,
  `domingo` tinyint(1) NOT NULL,
  `periodo` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dias_visita_proveedor`
--

INSERT INTO `dias_visita_proveedor` (`id`, `id_proveedor`, `lunes`, `martes`, `miercoles`, `jueves`, `viernes`, `sabado`, `domingo`, `periodo`) VALUES
(3, 9, 1, 1, 0, 0, 1, 0, 0, 1),
(4, 10, 1, 0, 1, 0, 1, 0, 0, 1),
(5, 11, 0, 0, 0, 0, 0, 1, 0, 0),
(6, 1, 1, 0, 1, 0, 1, 0, 0, 1),
(7, 2, 1, 0, 0, 0, 1, 0, 0, 1),
(8, 3, 1, 0, 0, 0, 1, 1, 0, 1),
(9, 12, 1, 0, 0, 0, 0, 1, 0, 0),
(10, 13, 0, 0, 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `importes`
--

CREATE TABLE IF NOT EXISTS `importes` (
  `id_importe` int(11) NOT NULL,
  `referencia` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` float(6,2) NOT NULL,
  `tipo_envase` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `notas` varchar(105) COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_operacion` datetime NOT NULL,
  `estatus` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `movimiento_referencia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id_producto` int(11) NOT NULL,
  `marca` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `pres_medida` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `pres_unidad` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `unidad_medida` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `precio_compra` float(5,2) NOT NULL,
  `precio_venta` float(5,2) NOT NULL,
  `codigo_barras` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `texto_ticket` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `existencia` float(6,2) NOT NULL,
  `categoria` int(11) DEFAULT NULL,
  `id_proveedor` int(11) NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'AC'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `marca`, `pres_medida`, `pres_unidad`, `unidad_medida`, `precio_compra`, `precio_venta`, `codigo_barras`, `texto_ticket`, `existencia`, `categoria`, `id_proveedor`, `estatus`) VALUES
(6, 'Colgate triple acción', '75', 'ml', 'pieza', 11.00, 13.50, '6512479988', 'Colgate triple acción 75ml', 34.00, 1, 13, 'AC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `nombre_proveedor` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `numero_ext` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `municipio` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `estado` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT 'AC',
  `saldo` float(6,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre_proveedor`, `telefono`, `calle`, `numero_ext`, `colonia`, `municipio`, `cp`, `estado`, `estatus`, `saldo`) VALUES
(1, 'Sabritas', '472-100-0001', 'Calzada Miguel Hidalgo', '105', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(2, 'Coca-cola', '472-180-5252', 'Bulevard Raul Balleres', '1500', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(3, 'Barcel', '478-963-5656', 'Álvaro Obregón', '451', 'Rinconada de las flores', 'Silao', 36150, 'Guanajuato', 'AC', 0.00),
(9, 'Leche León', '477-123-1112', 'Bulevard Aeropuerto', '1504', 'Los Sauces', 'León', 37140, 'Guanajuato', 'AC', 0.00),
(10, 'Gamesa', '472-789-5454', 'Río Mayo', '45', 'Valle de San José', 'Silao', 36154, 'Guanajuato', 'AC', 0.00),
(11, 'Truper', '471-214-5445', '5 de mayo', '345', 'Barrio Nuevpo', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(12, 'Dulcería México', '472-729-2828', 'Calle Mercado', '21', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(13, 'p-And-g', '333-154-5444', 'Obregón', '1050', 'Españita', 'León', 37100, 'Guanajuato', 'AC', 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recargas`
--

CREATE TABLE IF NOT EXISTS `recargas` (
  `id_recarga` int(11) NOT NULL,
  `monto` float(6,2) NOT NULL,
  `compania` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `folio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `transaccion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `numero_autorizacion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `comision` float(6,2) DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recargas`
--

INSERT INTO `recargas` (`id_recarga`, `monto`, `compania`, `folio`, `transaccion`, `numero_autorizacion`, `comision`, `telefono`, `fecha`) VALUES
(1, 50.00, 'Iusacell', '123456', '987654', '132123132', 5.00, '(470)-729-8228', '2015-11-03 22:33:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `rol` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `password`, `rol`) VALUES
(1, 'Is_Gaytan', 'Is12345', 'admin'),
(2, 'Andres_Moreno', 'Andres12345', 'cajero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE IF NOT EXISTS `ventas` (
  `id_venta` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `total` float(6,2) NOT NULL,
  `tipo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `tipo_cobro` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `fecha`, `total`, `tipo`, `id_usuario`, `tipo_cobro`) VALUES
(13, '2015-11-09 22:29:45', 54.00, 1, 1, 'EFECTIVO'),
(14, '2015-11-09 22:30:57', 27.00, 1, 1, 'EFECTIVO'),
(15, '2015-11-10 21:03:10', 13.50, 1, 1, 'EFECTIVO'),
(16, '2015-11-10 21:03:35', 27.00, 1, 1, 'EFECTIVO'),
(17, '2015-11-10 21:23:55', 13.50, 2, 1, 'EFECTIVO'),
(18, '2015-11-10 21:24:24', 54.00, 2, 1, 'EFECTIVO'),
(19, '2015-11-10 21:24:54', 27.00, 2, 1, 'EFECTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_credito`
--

CREATE TABLE IF NOT EXISTS `ventas_credito` (
  `id_venta` int(11) NOT NULL,
  `cantidad_cubierta` float(6,2) NOT NULL,
  `cantidad_credito` float(6,2) NOT NULL,
  `cantidad_saldada` float(6,2) NOT NULL DEFAULT '0.00',
  `estatus` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `observaciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `id_usuario_recibio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ventas_credito`
--

INSERT INTO `ventas_credito` (`id_venta`, `cantidad_cubierta`, `cantidad_credito`, `cantidad_saldada`, `estatus`, `observaciones`, `id_cliente`, `fecha_pago`, `id_usuario_recibio`) VALUES
(17, 0.00, 13.50, 34.00, 'PA', 'Alejandro vino por una pasta y no la pagó', 4, '2015-11-10 21:27:23', 1),
(18, 20.00, 34.00, 34.00, 'PA', 'Mía vino por cuatro pastas y sólo traía $20', 4, '2015-11-10 21:46:10', 1),
(19, 20.00, 7.00, 0.00, 'PE', 'Mandaron a Brandón por 2 pastas y Ramiro sólo mandó $20', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_tarjeta`
--

CREATE TABLE IF NOT EXISTS `ventas_tarjeta` (
  `id_venta` int(11) NOT NULL,
  `terminal` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `transaccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `lote` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `factura` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `referencia` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `abonos`
--
ALTER TABLE `abonos`
  ADD PRIMARY KEY (`id_abono`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `compras_especiales`
--
ALTER TABLE `compras_especiales`
  ADD PRIMARY KEY (`id_compra`);

--
-- Indices de la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `importes`
--
ALTER TABLE `importes`
  ADD PRIMARY KEY (`id_importe`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `id_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `recargas`
--
ALTER TABLE `recargas`
  ADD PRIMARY KEY (`id_recarga`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`);

--
-- Indices de la tabla `ventas_credito`
--
ALTER TABLE `ventas_credito`
  ADD KEY `id_cliente_foreign_key_idx` (`id_cliente`),
  ADD KEY `id_venta_foreign_key_ventascredito_idx` (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `abonos`
--
ALTER TABLE `abonos`
  MODIFY `id_abono` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `compras_especiales`
--
ALTER TABLE `compras_especiales`
  MODIFY `id_compra` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `importes`
--
ALTER TABLE `importes`
  MODIFY `id_importe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `recargas`
--
ALTER TABLE `recargas`
  MODIFY `id_recarga` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
  ADD CONSTRAINT `dias_visita_proveedor_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`);

--
-- Filtros para la tabla `ventas_credito`
--
ALTER TABLE `ventas_credito`
  ADD CONSTRAINT `id_cliente_foreign_key` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_venta_foreign_key_ventascredito` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
