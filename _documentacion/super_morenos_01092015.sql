-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-09-2015 a las 17:47:20
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `super_morenos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido_pat` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido_mat` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `numero` varchar(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `municipio` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'AC',
  `saldo` float(6,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `apellido_pat`, `apellido_mat`, `alias`, `telefono`, `calle`, `numero`, `colonia`, `municipio`, `cp`, `estatus`, `saldo`) VALUES
(4, 'Israel', 'Gaytan', 'Rodríguez', 'Is Gaytán', '472-729-8228', 'Primavera', '1992', 'Del Fresno', 'Guadalajara', 43100, 'BA', 0.00),
(5, 'Alfredo', 'Sauceda', 'Hernández', 'Pelotón', '472-787-8788', 'Niños Héroes', '34', 'Menores', 'Silao', 36294, 'AC', 0.00),
(6, 'Ramiro', 'Quintero', 'Hernández', 'El Rames', '472-151-5151', '5 de mayo', '3', 'Menores', 'Silao', 36594, 'AC', 0.00),
(7, 'Mariano', 'Moreno', 'Rodriguez', 'Super Moreno', '472-123-4567', '5 de mayo', '21', 'Menores', 'Silao', 36294, 'AC', 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_especiales`
--

CREATE TABLE IF NOT EXISTS `compras_especiales` (
`id_compra` bigint(20) NOT NULL,
  `fecha_operacion` datetime NOT NULL,
  `total_compra` float(6,2) NOT NULL,
  `total_pagado` float(6,2) NOT NULL,
  `total_a_credito` float(6,2) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PE',
  `fecha_pago` datetime NOT NULL,
  `referencia_pago` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dias_visita_proveedor`
--

CREATE TABLE IF NOT EXISTS `dias_visita_proveedor` (
`id` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `lunes` tinyint(1) NOT NULL,
  `martes` tinyint(1) NOT NULL,
  `miercoles` tinyint(1) NOT NULL,
  `jueves` tinyint(1) NOT NULL,
  `viernes` tinyint(1) NOT NULL,
  `sabado` tinyint(1) NOT NULL,
  `domingo` tinyint(1) NOT NULL,
  `periodo` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dias_visita_proveedor`
--

INSERT INTO `dias_visita_proveedor` (`id`, `id_proveedor`, `lunes`, `martes`, `miercoles`, `jueves`, `viernes`, `sabado`, `domingo`, `periodo`) VALUES
(3, 9, 1, 1, 0, 0, 1, 0, 0, 1),
(4, 10, 1, 0, 1, 0, 1, 0, 0, 1),
(5, 11, 0, 0, 0, 0, 0, 1, 0, 0),
(6, 1, 1, 0, 1, 0, 1, 0, 0, 1),
(7, 2, 1, 0, 0, 0, 1, 0, 0, 1),
(8, 3, 1, 0, 0, 0, 1, 1, 0, 1),
(9, 12, 1, 0, 0, 0, 0, 1, 0, 0),
(10, 13, 0, 0, 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
`id_producto` int(11) NOT NULL,
  `marca` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `pres_medida` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `pres_unidad` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `unidad_medida` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `precio_compra` float(5,2) NOT NULL,
  `precio_venta` float(5,2) NOT NULL,
  `codigo_barras` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `texto_ticket` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `existencia` float(6,2) NOT NULL,
  `categoria` int(11) DEFAULT NULL,
  `id_proveedor` int(11) NOT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'AC'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `marca`, `pres_medida`, `pres_unidad`, `unidad_medida`, `precio_compra`, `precio_venta`, `codigo_barras`, `texto_ticket`, `existencia`, `categoria`, `id_proveedor`, `estatus`) VALUES
(6, 'Colgate triple acción', '75', 'ml', 'pieza', 11.00, 13.50, '6512479988', 'Colgate triple acción 75ml', 12.00, 1, 13, 'AC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
`id_proveedor` int(11) NOT NULL,
  `nombre_proveedor` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `numero_ext` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `municipio` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `estado` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `estatus` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT 'AC',
  `saldo` float(6,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre_proveedor`, `telefono`, `calle`, `numero_ext`, `colonia`, `municipio`, `cp`, `estado`, `estatus`, `saldo`) VALUES
(1, 'Sabritas', '472-100-0001', 'Calzada Miguel Hidalgo', '105', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(2, 'Coca-cola', '472-180-5252', 'Bulevard Raul Balleres', '1500', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(3, 'Barcel', '478-963-5656', 'Álvaro Obregón', '451', 'Rinconada de las flores', 'Silao', 36150, 'Guanajuato', 'AC', 0.00),
(9, 'Leche León', '477-123-1112', 'Bulevard Aeropuerto', '1504', 'Los Sauces', 'León', 37140, 'Guanajuato', 'AC', 0.00),
(10, 'Gamesa', '472-789-5454', 'Río Mayo', '45', 'Valle de San José', 'Silao', 36154, 'Guanajuato', 'AC', 0.00),
(11, 'Truper', '471-214-5445', '5 de mayo', '345', 'Barrio Nuevpo', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(12, 'Dulcería México', '472-729-2828', 'Calle Mercado', '21', 'Zona Centro', 'Silao', 36100, 'Guanajuato', 'AC', 0.00),
(13, 'p-And-g', '333-154-5444', 'Obregón', '1050', 'Españita', 'León', 37100, 'Guanajuato', 'AC', 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `password`) VALUES
(1, 'administrador', 'Is12345');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `compras_especiales`
--
ALTER TABLE `compras_especiales`
 ADD PRIMARY KEY (`id_compra`);

--
-- Indices de la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
 ADD PRIMARY KEY (`id_producto`), ADD KEY `id_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
 ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `compras_especiales`
--
ALTER TABLE `compras_especiales`
MODIFY `id_compra` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dias_visita_proveedor`
--
ALTER TABLE `dias_visita_proveedor`
ADD CONSTRAINT `dias_visita_proveedor_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
