/*
*
* ALTERS APARTIR DEL 11/11/2015
*
*/

--11/11/2015
--Is Gaytán
ALTER TABLE importes CHANGE COLUMN estatus estatus VARCHAR(2) DEFAULT 'PE';

--11/11/2015
--Is Gaytán
ALTER TABLE importes DROP COLUMN movimiento_referencia;
ALTER TABLE usuarios ADD COLUMN nombre VARCHAR(30) NOT NULL;
ALTER TABLE usuarios ADD COLUMN apellido_pat VARCHAR(30) NOT NULL;
ALTER TABLE usuarios ADD COLUMN estatus VARCHAR(2) NOT NULL DEFAULT 'AC';

--21/11/2015
--Is Gaytán
ALTER TABLE compras_especiales
ADD CONSTRAINT id_proveedor_fk_compras_especiales
FOREIGN KEY (id_proveedor)
REFERENCES proveedores (id_proveedor)
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE compras_especiales
ADD CONSTRAINT id_usuario_fk_compras_especiales
FOREIGN KEY (id_usuario)
REFERENCES usuarios (id_usuario)
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE importes
ADD CONSTRAINT id_usuario_fk_importes
FOREIGN KEY (id_usuario)
REFERENCES usuarios (id_usuario)
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE ventas_credito
ADD CONSTRAINT id_usuario_recibio_fk_ventas_credito
FOREIGN KEY (id_usuario_recibio)
REFERENCES usuarios (id_usuario)
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE ventas_tarjeta
ADD CONSTRAINT id_venta_fk_ventas_credito
FOREIGN KEY (id_venta)
REFERENCES ventas (id_venta)
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE recargas ADD COLUMN id_usuario INT(11);

ALTER TABLE recargas
ADD CONSTRAINT id_usuario_fk_recargas
FOREIGN KEY (id_usuario)
REFERENCES usuarios (id_usuario)
ON DELETE NO ACTION ON UPDATE NO ACTION;

-- 06/01/12
-- Is Gaytán
-- Agregar columna para guardar el porcentaje de descuento del producto
ALTER TABLE productos ADD COLUMN porcentaje FLOAT(6,2) DEFAULT 0;

-- 07/01/12
-- Is Gaytán
-- Agregrar la tabla para registrar las compras generales y su llave foranea
CREATE TABLE compras(
    id_compra INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    cantidad_tipos_articulos INT(11) NOT NULL,
    precio_compra FLOAT(6,2) NOT NULL,
    cantidad_articulos INT(11) NOT NULL,
    fecha_compra DATETIME,
    id_usuario INT(11)
);

ALTER TABLE compras
ADD CONSTRAINT id_usuario_fk_usuarios
FOREIGN KEY (id_usuario)
REFERENCES usuarios (id_usuario)
ON DELETE NO ACTION ON UPDATE NO ACTION;

-- 28/01/2016
-- Is Gaytán
-- Agregar la columna venta libre para identificar los dulces y las verduras
ALTER TABLE productos ADD COLUMN venta_libre INT DEFAULT 0;

-- 04/04/2016
-- Is Gaytán
-- La tabla ventas_vales para llevar el control de las ventas que se cobran con tarjeta de vales
CREATE TABLE ventas_vales(
	id_vta_vales INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    id_venta INT(11) NOT NULL,
    tarjetahabiente VARCHAR(30) NOT NULL,
    digitos_tarjeta INT(11)
);

ALTER TABLE ventas_vales 
ADD CONSTRAINT id_venta_fk_ventas_vales
FOREIGN KEY(id_venta)
REFERENCES ventas(id_venta)
ON DELETE NO ACTION ON UPDATE NO ACTION;

-- 04/04/2016
-- Is Gaytán
-- Agregar campo compra_rapida para identificar cuando la compra se realizó en el momento en la caja
ALTER TABLE compras ADD COLUMN compra_rapida VARCHAR (2) DEFAULT 'SI';

-- 13/04/2016
-- Is Gaytán
-- Cosas por arreglar en la base de datos de Andrés
Eliminar la tabla compras
Agregar el nombre y el apellido a los usuarios
Crear la tabla compras con os campos correctamente
Agregar el campo compra_rapida

