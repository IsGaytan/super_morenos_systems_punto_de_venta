<?php
class conectar_bd{

    static $server = 'localhost';
    static $user = 'root';
    static $password = '';
    static $db_name = 'super_morenos';
    
    public function realizar_conexion()
    {
        $conect = new mysqli();
        $conect->connect(conectar_bd::$server, conectar_bd::$user, conectar_bd::$password, conectar_bd::$db_name);
        $conect->query("SET NAMES 'utf8'");
        
        if ($conect->connect_error) {
            error_log("Error en la conexión a la bd..." . print_r($conect->connect_error, true));
            $resultado['tipo'] = 3;
            throw new Exception($conect->connect_error);
        }
        
        return $conect;
    }
}
